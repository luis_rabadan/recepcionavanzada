CREATE TABLE IF NOT EXISTS `llx_recepcionavanzada_costo` (
 `rowid` int(11) NOT NULL AUTO_INCREMENT,
 `fk_reception` int(11) NOT NULL,
 `fk_facture_fourn` int(11) NOT NULL,
 `categoria` varchar(100) DEFAULT NULL,
 `xml` varchar(255) DEFAULT NULL,
 `pdf` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`rowid`)
);
ALTER TABLE `llx_recepcionavanzada_costo` add `type_fac` int(11) DEFAULT 0;

CREATE TABLE IF NOT EXISTS `llx_cfdimx_facturefourn` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `fk_facture` int(11) NOT NULL DEFAULT '0',
  `xml` varchar(255) NOT NULL DEFAULT '',
  `pdf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rowid`)
);
