<?php
if($form2==''){
$xml = simplexml_load_file('xml/'.$filename); 
}else{
	$xml = simplexml_load_file(''.$filename);
}
$ns = $xml->getNamespaces(true);
$xml->registerXPathNamespace('c', $ns['cfdi']);
$xml->registerXPathNamespace('t', $ns['tfd']);
 
 
foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){ 
      $compVersion=$cfdiComprobante['version']; 
      $compFecha=$cfdiComprobante['fecha']; 
      $compSello=$cfdiComprobante['sello']; 
      $compImpTot=$cfdiComprobante['total']; 
      $compIppSubTot=$cfdiComprobante['subTotal']; 
      $compCertifi=$cfdiComprobante['certificado']; 
      $compFormPag=$cfdiComprobante['formaDePago']; 
      $compMetPag=$cfdiComprobante['metodoDePago'];
      $compNoCertificado=$cfdiComprobante['noCertificado']; 
      $compTipoCompr=$cfdiComprobante['tipoDeComprobante']; 
      $compSerie=$cfdiComprobante['serie'];
      $compFolio=$cfdiComprobante['folio'];
      $compMoneda=$cfdiComprobante['Moneda'];
      
      //echo "<br />"; 
} 
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){ 
   $emisorRfc=$Emisor['rfc']; 
   $emisorNomc=$Emisor['nombre']; 
   //echo "<br />"; 
} 
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal') as $DomicilioFiscal){ 
   $emisorDomPais=$DomicilioFiscal['pais']; 
   $emisorDomCalle=$DomicilioFiscal['calle']; 
   $emisorDomEstado=$DomicilioFiscal['estado']; 
   $emisorDomCol=$DomicilioFiscal['colonia']; 
   $emisorDomMunic=$DomicilioFiscal['municipio']; 
   $emisorDomNoExt=$DomicilioFiscal['noExterior']; 
   $emisorDomNoInt=$DomicilioFiscal['noInterior'];
   $emisorDomCP=$DomicilioFiscal['codigoPostal']; 
   //echo "<br />"; 
} 
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn') as $ExpedidoEn){ 
   $emisorExpedPais=$ExpedidoEn['pais']; 
   $emisorExpedCalle=$ExpedidoEn['calle']; 
   $emisorExpedEstado=$ExpedidoEn['estado']; 
   $emisorExpedColo=$ExpedidoEn['colonia']; 
   $emisorExpedNoExt=$ExpedidoEn['noExterior']; 
   $emisorExpedCP=$ExpedidoEn['codigoPostal']; 
   //echo "<br />"; 
} 
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){ 
   $receptorRFC=$Receptor['rfc']; 
   $receptorNombre=$Receptor['nombre']; 
   //echo "<br />"; 
} 
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio') as $ReceptorDomicilio){ 
   $receptorDomPais=$ReceptorDomicilio['pais']; 
   $receptorDomCalle=$ReceptorDomicilio['calle']; 
   $receptorDomEstad=$ReceptorDomicilio['estado']; 
   $receptorDomCol=$ReceptorDomicilio['colonia']; 
   $receptorDomMun=$ReceptorDomicilio['municipio']; 
   $receptorDomNoExt=$ReceptorDomicilio['noExterior']; 
   $receptorDomNoInt=$ReceptorDomicilio['noInterior']; 
   $receptorDomCP=$ReceptorDomicilio['codigoPostal']; 
   //echo "<br />"; 
} 
$i=0;
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){ 
   $arrConcp[$i]['unidad']=$Concepto['unidad']; 
   $arrConcp[$i]['importe']=$Concepto['importe']; 
   $arrConcp[$i]['cantidad']=$Concepto['cantidad']; 
   $arrConcp[$i]['descripcion']=$Concepto['descripcion']; 
   $arrConcp[$i]['valorUnitario']=$Concepto['valorUnitario'];
   $arrConcp[$i]['noIdentificacion']=$Concepto['noIdentificacion'];
   $i++;
   //echo "<br />";   
   //echo "<br />"; 
} 
$maxConc=$i;
/* for($i=0;$i<$maxConc;$i++){
	print "Desc: ".$arrConcp[$i]['descripcion']." - Importe:".$arrConcp[$i]['importe']." - valorUnitario: ".$arrConcp[$i]['valorUnitario']."
			 - cantidad: ".$arrConcp[$i]['cantidad']."<br>";
} */

$i=0;
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){ 
   $arrTras[$i]['tasa']=$Traslado['tasa']; 
   $arrTras[$i]['importe']=$Traslado['importe']; 
   $arrTras[$i]['impuesto']=$Traslado['impuesto']; 
   $i++;
   //echo "<br />";   
   //echo "<br />"; 
} 
$maxTras=$i;
 
foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
  $timbreFDSelloCfd=$tfd['selloCFD']; 
  $timbreFDFechTimbra=$tfd['FechaTimbrado']; 
  $timbreFDUuid=$tfd['UUID']; 
  $timbreFDNoCertif=$tfd['noCertificadoSAT']; 
  $timbreFDVersion=$tfd['version']; 
  $timbreFDSelloSat=$tfd['selloSAT']; 
} 
?>