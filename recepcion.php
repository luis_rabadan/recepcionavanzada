<?php
require '../main.inc.php';
require_once DOL_DOCUMENT_ROOT.'/core/modules/supplier_order/modules_commandefournisseur.php';
require_once DOL_DOCUMENT_ROOT.'/product/stock/class/entrepot.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/fourn.lib.php';
require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.class.php';
require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.dispatch.class.php';
require_once DOL_DOCUMENT_ROOT.'/product/class/html.formproduct.class.php';
dol_include_once('/recepcionavanzada/lib/recepcion.lib.php');
$id=GETPOST('id');
$ref=GETPOST('ref');
$act=GETPOST('act');
$object = new CommandeFournisseur($db);
$object->fetch($id,$ref);

$id=$object->id;

$langs->load('orders');
$langs->load('sendings');
$langs->load('companies');
$langs->load('bills');
$langs->load('deliveries');
$langs->load('products');
$langs->load('stocks');
$langs->load("recepcionavanzada@recepcionavanzada");



//llxHeader('',"Recepcion de Stock avanzado","");
llxHeader('',$langs->trans("Order"),"Recepcion de Stock avanzado",'',0,0,array('/recepcionavanzada/js/lib_dispatch.js'));

$res=$object->fetch_optionals($object->id,$extralabels);

$head = ordersupplier_prepare_head($object);
$title=$langs->trans("SupplierOrder");
dol_fiche_head($head, 'taborderprov', $title, 0, 'order');

$soc = new Societe($db);
$form =	new Form($db);
$formproduct = new FormProduct($db);
$warehouse_static = new Entrepot($db);
$supplierorderdispatch = new CommandeFournisseurDispatch($db);
$commande = new CommandeFournisseur($db);
$author = new User($db);

$result=$commande->fetch($id,$ref);
$soc->fetch($commande->socid);
$author->fetch($commande->user_author_id);

/*
 *	Commande
*/
print '<table class="border" width="100%">';
// Ref
print '<tr><td class="titlefield">'.$langs->trans("Ref").'</td>';
print '<td colspan="3">';
print $form->showrefnav($commande,'ref','',1,'ref','ref');
print '</td>';
print '</tr>';

// Fournisseur
print '<tr><td>'.$langs->trans("Supplier")."</td>";
print '<td colspan="3">'.$soc->getNomUrl(1,'supplier').'</td>';
print '</tr>';

// Statut
print '<tr>';
print '<td>'.$langs->trans("Status").'</td>';
print '<td colspan="3">';
print $commande->getLibStatut(4);
print "</td></tr>";

print '<tr>';
print '<td>'.$langs->trans("Date").'</td>';
print '<td colspan="1">';
print dol_print_date($commande->date_commande,"dayhourtext")."\n";
print '</td>';
print '<td colspan="1">Total de art&iacute;culos de esta O.C.</td>';
$sqm="SELECT ROUND(IFNULL(sum(qty),0),2) as tot FROM ".MAIN_DB_PREFIX."commande_fournisseurdet WHERE product_type=0 and fk_product>0 and fk_commande= ".$id;
$rm=$db->query($sqm);
$rsm=$db->fetch_object($rm);
$roc=$rsm->tot;
print '<td colspan="1">'.$roc.'</td>';
print "</tr>";


print '<tr>';
print '<td>Divisa</td>';
print '<td colspan="1">';
$divisadocumento=$conf->currency;
if($conf->global->MAIN_MODULE_MULTIDIVISA){
	$sqld="SELECT divisa FROM ".MAIN_DB_PREFIX."multidivisa_commande_fournisseur WHERE fk_object=".$id;
	//print $sqld;
	$rqd=$db->query($sqld);
	$nrd=$db->num_rows($rqd);
	if($nrd>0){
		$rsld=$db->fetch_object($rqd);
		$divisadocumento=$rsld->divisa;
	}
}
print $divisadocumento;
print '</td>';
print '<td colspan="1">Art&iacute;culos recibidos</td>';
$sqm="SELECT ROUND(IFNULL(sum(qty),0),2) as tot FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch WHERE fk_commande= ".$id;
$rm=$db->query($sqm);
$rsm=$db->fetch_object($rm);
$ror=$rsm->tot;
print '<td colspan="1">'.$ror.'</td>';
print "</tr>";

// Auteur
print '<tr><td>'.$langs->trans("AuthorRequest").'</td>';
print '<td colspan="1">'.$author->getNomUrl(1).'</td>';
print '<td colspan="1">Art&iacute;culos sin recepci&oacute;n</td>';
$rest=$roc-$ror;
print '<td colspan="1">'.$rest.'</td>';
print '</tr>';

print "</table>";
print '<div class="tabsAction">'."\n";
	
if ($result >= 0)
{
	if ($commande->statut <= 2 || $commande->statut >= 6){	
		print $langs->trans("OrderStatusNotReadyToDispatch");		
	}else{
		$sqc="SELECT count(rowid) as ext FROM ".MAIN_DB_PREFIX."commande_fournisseurdet WHERE product_type=0 AND fk_product>0 AND fk_commande=".$id;
		$rqc=$db->query($sqc);
		$rslc=$db->fetch_object($rqc);
		if($rslc->ext==0){
			print "No hay productos predefinidos en este objeto. Por lo tanto no se puede realizar una orden de recepcion.";
		}else{
			if ($user->rights->recepcionavanzada->edit_recep && $commande->statut !=5 ){
				print '<div class="inline-block divButAction"><a class="butAction" href="recepcion.php?id='.$id.'&act=card&action=new">Nueva Recepcion</a></div>';
				print "</div>";
			}
		}
	}
}



if($act=='card'){
	include 'card.php';
}

if($act==''){
	$sql="SELECT a.rowid,a.ref, a.date_orderreception, a.import_declaration, a.status, a.time_orderreception, b.firstname,b.lastname, c.lieu, a.fk_order_supplier
FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion a, ".MAIN_DB_PREFIX."user b, ".MAIN_DB_PREFIX."entrepot c
WHERE a.fk_order_supplier=".$id." AND a.entity=".$conf->entity." AND a.fk_user_reception=b.rowid 
		AND a.fk_user_warehouse=c.rowid ORDER BY a.rowid DESC";
	$rq=$db->query($sql);
	//print $sql;
	$nr=$db->num_rows($rq);
	if($nr>0){
	print "<table class='noborder' witdh='100%'>";
	print "<tr class='liste_titre' >";
		print "<td>Ref.</td>";
		print "<td>Fecha</td>";
		print "<td>Almacen destino</td>";
		print "<td>Pedimento de importacion</td>";
		print "<td>Recibi&oacute;</td>";
		print "<td>Articulos recibidos</td>";
		print "<td>Estatus</td>";
	print "</tr>";
	while($rs=$db->fetch_object($rq)){
	print "<tr>";
	$sqb="SELECT bandConfgCostos FROM ".MAIN_DB_PREFIX."recepcionavanzada_conf WHERE entity=".$conf->entity;
	$rbs=$db->query($sqb);
	$rbb=$db->fetch_object($rbs);
	if($rbb->bandConfgCostos==1){
		print "<td align='left'><a href='recepcion.php?id=".$id."&act=card&type=1&coste=".$rs->rowid."'>".$rs->ref."</a></td>";
	}else{
		print "<td align='left'><a href='recepcion.php?id=".$id."&act=card&type=1&idrec=".$rs->rowid."'>".$rs->ref."</a></td>";
	}
	print "<td align='left'>".dol_print_date($db->jdate($rs->date_orderreception), 'day')."</td>";
	print "<td align='left'>".$rs->lieu."</td>";
	print "<td align='left'>".$rs->import_declaration."</td>";
	print "<td align='left'>".$rs->firstname." ".$rs->lastname."</td>";
	$sqm="SELECT ROUND(IFNULL(sum(qty),0),2) as tot FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch WHERE fk_commande= ".$id.' and fk_order_reception='.$rs->rowid;	
	$rm=$db->query($sqm);
	$rsm=$db->fetch_object($rm);
	$ror=$rsm->tot;
	print "<td align='left'>".$ror."</td>";
	if($rs->status==0){
		$a='<span class="hideonsmartphone">'."Borrador".' </span>'.img_picto($langs->trans('StatusOrderDraftShort'),'statut0');
	}
	if($rs->status==1){
		$a='<span class="hideonsmartphone">'."Validado".' </span>'.img_picto($langs->trans('StatusOrderValidatedShort'),'statut1');
	}
	if($rs->status==2){
		$a='<span class="hideonsmartphone">'."Cerrado".' </span>'.img_picto($langs->trans('Cerrado'),'statut4');
	}
	print "<td align='left'>".$a."</td>";
	print "</tr>";
	}
	print "</table>";
	}
}


// End of page
llxFooter();

$db->close();

