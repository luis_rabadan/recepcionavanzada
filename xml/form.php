<?php
$sql="SELECT a.*, b.firstname,b.lastname, c.lieu
FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion a, ".MAIN_DB_PREFIX."user b, ".MAIN_DB_PREFIX."entrepot c
WHERE a.fk_order_supplier=".$idp." AND a.rowid=".$idrec." AND a.entity=".$conf->entity." AND a.fk_user_reception=b.rowid
		AND a.fk_user_warehouse=c.rowid";
//echo $sql;
$rq=$db->query($sql);
$nr=$db->num_rows($rq);
if($nr>0){
	$rs=$db->fetch_object($rq);
	print "<table  class='border' width='100%' style='text-align:left'>";
	print "<tr>";
	print "<td width='25%'>Ref.</td>";
	print "<td width='25%'>".$rs->ref."</td>";
	print "<td width='25%'>Genera pedimento de importacion?</td>";
	if($rs->status_import_declaration==0){$generap='NO';}
	if($rs->status_import_declaration==1){$generap='SI';}
	print "<td width='25%'>".$generap."</td>";
	print "</tr>";
	print "<tr>";
	print "<td><strong>Fecha de recepcion</strong></td>";
	//print "<td>".$rs->date_orderreception." ".$rs->time_orderreception."</td>";
	print "<td>".date('d/m/Y',strtotime($rs->date_orderreception))." ".$rs->time_orderreception."</td>";
	print "<td>Pedimento de importacion</td>";
	print "<td>".$rs->import_declaration."</td>";
	print "</tr>";
	print "<tr>";
	print "<td><strong>Recibe</strong></td>";
	print "<td>".$rs->firstname." ".$rs->lastname."</td>";
	print "<td>Fecha de pedimento</td>";
	print "<td>".$rs->date_import_declaration."</td>";
	print "</tr>";
	print "<tr>";
	print "<td><strong>Almacen destino</strong></td>";
	print "<td>".$rs->lieu."</td>";
	print "<td>Aduana</td>";
	print "<td>".$rs->aduana."</td>";
	print "</tr>";
	print "<tr>";
	print "<td>Tipo de cambio</td>";
	print "<td>".$rs->exchange_rate."</td>";
	print "<td>Estatus</td>";
	if($rs->status==0){$status='Borrador';}
	if($rs->status==1){$status='Validado';}
	if($rs->status==2){$status='Cerrada';}
	$statusdocto=$rs->status;
	//if($rs->status==3){$status='Cerrada';}
	print "<td>".$status."</td>";
	print "</tr>";
	print "</table><br><br>";

	$sql="SELECT a.rowid,a.xml,a.pdf,a.type_fac,d.label,a.fk_facture_fourn, b.ref,b.ref_supplier,b.fk_soc,
				b.datef,b.total_ht,b.total_tva, b.total_ttc,c.nom
		FROM ".MAIN_DB_PREFIX."recepcionavanzada_costo a LEFT JOIN ".MAIN_DB_PREFIX."c_recepcionavanzada_categoria_gasto d ON a.categoria=d.rowid, ".MAIN_DB_PREFIX."facture_fourn b, 
				".MAIN_DB_PREFIX."societe c
		WHERE a.fk_reception=".$idrec." AND a.fk_facture_fourn=b.rowid AND b.fk_soc=c.rowid";
	$rqs=$db->query($sql);
	$nrw=$db->num_rows($sql);
	if($nrw>0){
		print "<table class='noborder' width='100%' style='text-align:left'>";
		print "<tr class='liste_titre'>";
		print "<td>Ref.<td>";
		print "<td>Ref. proveedor<td>";
		print "<td>Fecha de Factura<td>";
		print "<td>Proveedor<td>";
		print "<td>Categoria de gasto<td>";
		print "<td>Subtotal<td>";
		print "<td>Impuestos<td>";
		print "<td>Total<td>";
		print "<td>Moneda<td>";
		print "<td>XML<td>";
		print "<td>PDF<td>";
		print "<td>&nbsp;<td>";
		print "<td>&nbsp;<td>"; //type_fac
		print "</tr>";
		while($rsl=$db->fetch_object($rqs)){	
			require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.facture.class.php';
			$facpr= new FactureFournisseur($db);
			$facpr->fetch($rsl->fk_facture_fourn);		
			print "<tr>";
			print "<td><a href='".DOL_MAIN_URL_ROOT."/fourn/facture/card.php?facid=".$rsl->fk_facture_fourn."'>".$rsl->ref."</a><td>";
			print "<td>".$rsl->ref_supplier."<td>";
			print "<td>".$rsl->datef."<td>";
			print "<td><a href='".DOL_MAIN_URL_ROOT."/fourn/card.php?socid=".$rsl->fk_soc."'>".$rsl->nom."</a><td>";
			print "<td>".$rsl->label."<td>";
			print "<td>".number_format($rsl->total_ht,2)."<td>";
			print "<td>".number_format($rsl->total_tva,2)."<td>";
			print "<td>".number_format($rsl->total_ttc,2)."<td>";
			$divisadocumento=$conf->currency;
			if($conf->global->MAIN_MODULE_MULTIDIVISA){
				$sqld="SELECT divisa FROM ".MAIN_DB_PREFIX."multidivisa_facture_fourn WHERE fk_object=".$rsl->fk_facture_fourn;
				//print $sqld;
				$rqd=$db->query($sqld);
				$nrd=$db->num_rows($rqd);
				if($nrd>0){
					$rsld=$db->fetch_object($rqd);
					if($rsld->divisa!='MN'){
						$divisadocumento=$rsld->divisa;
					}
				}
			}
			print "<td>".$divisadocumento."<td>";
			$subdir = get_exdir($facpr->id,2,0,0,$facpr,'invoice_supplier');
			if($rsl->xml!='' && $rsl->xml!=null){
				//print $subdir."::";
				$relativepath=$subdir.$rsl->ref."/".$rsl->xml;
				print "<td><a href='".DOL_URL_ROOT."/document.php?modulepart=facture_fournisseur&file=".urlencode($relativepath)."'> ".@img_mime()."</a><td>";
			}else{
				print "<td>&nbsp;<td>";
			}
			if($rsl->pdf!='' && $rsl->pdf!=null){
				$relativepath=$subdir.$rsl->ref."/".$rsl->pdf;
				//print $relativepath;
				print "<td><a href='".DOL_URL_ROOT."/document.php?modulepart=facture_fournisseur&file=".urlencode($relativepath)."'>".img_pdf()."</a><td>";
			}else{
				print "<td>&nbsp;<td>";
			}
			if($rs->status==0){
				print "<td><a href='recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec."&oper=delet&cid=".$rsl->rowid."'>".img_delete()."</a><td>";
			}else{
				print "<td>&nbsp;<td>";
			}
			if($rsl->type_fac == 0)
				print "<td>Ninguna<td>";
			elseif($rsl->type_fac == 1)
				print "<td>Factura Aduana<td>";
			else
				print "<td>Gasto Pedimento<td>";
			print "</tr>";
		}
		print "</table><br><br>";
	}
	
	if($_REQUEST['oper']=='delet'){
		$form = new Form($db);
		print $form->formconfirm("recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec."&cid=".$_REQUEST['cid'],"Comfirmar","Esta seguro de eliminar la relacion con la Factura?","delet2");
		//$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $id, "Esta seguro de eliminar la relacion con la factura?", '', 'delet2', '', 0, 1, 220);
		//print $formconfirm;
	}
	if($_REQUEST['confirm']=='yes' && $_REQUEST['action']=='delet2'){
		$sql="DELETE FROM ".MAIN_DB_PREFIX."recepcionavanzada_costo WHERE rowid=".$_REQUEST['cid'];
		$rqq=$db->query($sql);
		$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);
		print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec."'</script>";
	}
	if($_REQUEST['oper']=='enviarxml'){
		$filename = $_FILES["xmlfac"]["name"];
		$source = $_FILES["xmlfac"]["tmp_name"];
		$tipofile= $_FILES["xmlfac"]["type"];
		$tipofile = explode("/", $tipofile);
		$tipofile=$tipofile[1];
		$filepdf='';
		if($_FILES["pdffac"]["name"]){
			$filepdf = $_FILES["pdffac"]["name"];
			$source2 = $_FILES["pdffac"]["tmp_name"];
			$dir_subida2 = "xml/".basename($filepdf);
		}
		if($tipofile=='xml'){
			$dir_subida = "xml/".basename($filename);
			//$fichero_subido = $dir_subida . basename($_FILES['xmlfac']['name']);
				
			if (@move_uploaded_file($source, $dir_subida)) {
				if($_FILES["pdffac"]["name"]){
					@move_uploaded_file($source2, $dir_subida2);
				}
				echo "El fichero es valido y se subio con exito. <br><br>";
			} else {
				echo "Error al cargar archivo<br><br>";
			}
			require 'xml/index.php';
			print "<form method='POST' action='xml/facturaxml.php?file=".$filename."&filepdf=".$filepdf."&idp=".$idp."&idrec=".$idrec."&catefac=".GETPOST('catefac')."&catefac2=".GETPOST('catefac2')."' ><table width='100%' class='border'><tr class='liste_titre'>";
			print "<td colspan='4' align='center' >Factura: ".$compSerie."-".$compFolio."</td></tr>";
			print "<tr class='liste_titre'>";
			print "<td colspan='4' align='center'>Emisor</td>";
			print "</tr>";
			$sql2="SELECT count(*) as exist,fournisseur FROM ".MAIN_DB_PREFIX."societe WHERE siren='".$emisorRfc."' AND entity='".$conf->entity."'";
			$dd=$db->query($sql2);
			$rdd=$db->fetch_object($dd);
			if($rdd->exist==0){
				print "<tr>";
				print "<td colspan='4' align='center'><strong style='color:#F00;'>El Proveedor no existe, se creara al generar la factura.</strong></td>";
				print "</tr>";
			}else{
				if($rdd->fournisseur==0){
					print "<tr>";
					print "<td colspan='4' align='center'><strong style='color:#F00;'>El tercero no es un proveedor, se convertira en proveedor al generar la factura.</strong></td>";
					print "</tr>";
				}
			}
			print "<tr>";
			print "<td><strong>RFC</strong></td>";
			print "<td width='40%'>".$emisorRfc."</td>";
			print "<td><strong>Nombre</strong></td>";
			print "<td width='40%'>".$emisorNomc."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>Pais</strong></td>";
			print "<td>".$emisorDomPais."</td>";
			print "<td><strong>Estado</strong></td>";
			print "<td>".$emisorDomEstado."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>Municipio</strong></td>";
			print "<td>".$emisorDomMunic."</td>";
			print "<td><strong>Colonia</strong></td>";
			print "<td>".$emisorDomCol."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>CP</strong></td>";
			print "<td>".$emisorDomCP."</td>";
			print "<td><strong>Calle</strong></td>";
			print "<td>".$emisorDomCalle."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>No. Ext</strong></td>";
			print "<td>".$emisorDomNoExt."</td>";
			print "<td><strong>No. Int</strong></td>";
			print "<td>".$emisorDomNoInt."</td>";
			print "</tr>";
			print "<tr class='liste_titre'>";
			print "<td colspan='4' align='center'>Receptor</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>RFC</strong></td>";
			print "<td>".$receptorRFC."</td>";
			print "<td><strong>Nombre</strong></td>";
			print "<td>".$receptorNombre."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>Pais</strong></td>";
			print "<td>".$receptorDomPais."</td>";
			print "<td><strong>Estado</strong></td>";
			print "<td>".$receptorDomEstad."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>Municipio</strong></td>";
			print "<td>".$receptorDomMun."</td>";
			print "<td><strong>Colonia</strong></td>";
			print "<td>".$receptorDomCol."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>CP</strong></td>";
			print "<td>".$receptorDomCP."</td>";
			print "<td><strong>Calle</strong></td>";
			print "<td>".$receptorDomCalle."</td>";
			print "</tr>";
			print "<tr>";
			print "<td><strong>No. Ext</strong></td>";
			print "<td>".$receptorDomNoExt."</td>";
			print "<td><strong>No. Int</strong></td>";
			print "<td>".$receptorDomNoInt."</td>";
			print "</tr>";
			print "<tr class='liste_titre'>";
			print "<td colspan='4' align='center'>Comprobante</td>";
			print "</tr>";
			print "<tr>";
			print '<td class="nowrap"><strong>'.$langs->trans('PaymentMode').'</strong></td><td>&nbsp;&nbsp;';
			print $compMetPag;
			print "</td><td colspan='2'>";
			print '<strong>Seleccionar el correspondiente en Dolibarr: &nbsp;&nbsp;&nbsp;&nbsp;';
			$form->select_types_paiements(isset($_POST['mode_reglement_id'])?$_POST['mode_reglement_id']:$mode_reglement_id, 'mode_reglement_id', 'DBIT');
			print '</td>';
			print "</tr>";
			print "<tr>";
			print "<tr>";
			print '<td class="nowrap"><strong>'.$langs->trans('PaymentConditionsShort').'</strong></td><td>&nbsp;&nbsp;';
			print $compFormPag;
			print "</td><td colspan='2'>";
			print '<strong>Seleccionar el correspondiente en Dolibarr: &nbsp;&nbsp;&nbsp;&nbsp;';
			$form->select_conditions_paiements(isset($_POST['cond_reglement_id'])?$_POST['cond_reglement_id']:$cond_reglement_id, 'cond_reglement_id');
			print '</td>';
			print "</tr>";
			$sql="SELECT count(*) as exist FROM ".MAIN_DB_PREFIX."const WHERE name='MAIN_MODULE_MULTIDIVISA' AND entity=".$conf->entity;
			$rf=$db->query($sql);
			$rff=$db->fetch_object($rf);
			if($rff->exist>0){
				print "<tr>";
				print '<td><strong>Divisa</strong></td><td>';
				print $compMoneda;
				print '</td>';
				print "<td colspan='2'>";
				$sql2="SELECT code,label,unicode FROM ".MAIN_DB_PREFIX."multidivisa_divisas
								WHERE code!='".$res->divisa."' AND entity=".$conf->entity;
				$resulset2=$db->query($sql2);
				print '<strong>Seleccionar el correspondiente en Dolibarr: &nbsp;&nbsp;&nbsp;&nbsp;';
				print "<select name='cadivisa'>";
				while($res2=$db->fetch_object($resulset2)){
					print "<option value='".$res2->code."'>".$res2->code."</option>";
				}
				print "</select>";
				print '</td>';
				print "</tr>";
			}
			print "<tr class='liste_titre'>";
			print "<td colspan='4' align='center'>Conceptos</td>";
			print "</tr>";
			print "<tr >";
			print "<td colspan='4'>";
			print "<table width='100%' class='notopnoleftnoright'>";
			print "<tr class='liste_titre'>";
			print "<td>No. Identificacion</td>";
			print "<td>Descripcion</td>";
			print "<td>Unidad</td>";
			print "<td>Cantidad</td>";
			print "<td>Valor Unitario</td>";
			print "<td>Importe</td>";
			print "<td>Impuesto</td>";
			print "<td>Tasa</td>";
			print "<td>Importe</td>";
			print "<td>ProductoDolibarr</td>";
			print "<td>Tipo</td>";
			print "</tr>";
			for($i=0;$i<$maxConc;$i++){
				print "<tr>";
				print "<td>".$arrConcp[$i]['noIdentificacion']."</td>";
				print "<td>".$arrConcp[$i]['descripcion']."</td>";
				print "<td>".$arrConcp[$i]['unidad']."</td>";
				print "<td>".$arrConcp[$i]['cantidad']."</td>";
				print "<td>".$arrConcp[$i]['valorUnitario']."</td>";
				print "<td>".$arrConcp[$i]['importe']."</td>";
				print "<td>".$arrTras[$i]['impuesto']."</td>";
				$las=count($arrTras);
				print "<td>".$arrTras[$i]['tasa']." <br> Tasa :: <input type='text' name='prodtasa".$i."' id='prodtasa".$i."' value='0'>%</td>";
				if($las>1){
					if($arrTras[$i]['importe']>0 && $arrTras[$i]['importe']!=''){
						$auxpuni=((float)$arrConcp[$i]['valorUnitario'])+((float)$arrTras[$i]['importe']);
					}else{
						$auxpuni=$arrConcp[$i]['importe'];
					}
				}else{
					$auxpuni=(float)$arrConcp[$i]['valorUnitario']+((float)$arrConcp[$i]['valorUnitario'])*((float)$arrTras[0]['tasa']/100);
				}
				print "<td>".number_format($auxpuni,2)."</td>";
				print "<td>";
				$ref=$arrConcp[$i]['noIdentificacion'];
				$object = new Product($db);
				$extrafields = new ExtraFields($db);
				if ($idp > 0 || ! empty($ref))
				{
					$object = new Product($db);
					$object->fetch($idp, $ref);
				}
				//print_r($object);
				/* if($object->id!=NULL){
					print $object->ref."-".$object->label."<br>";
					print "Ref :: <input type='text' name='prod".$i."' id='prod".$i."' >";
				}else{

					print "Ref :: <input type='text' name='prod".$i."' id='prod".$i."' required>";
						
				} */
				$sql="SELECT rowid, ref, label
				FROM ".MAIN_DB_PREFIX."product
				WHERE entity=".$conf->entity."";// AND fk_soc=";
				//print $sql;
				$rqs=$db->query($sql);
				include_once DOL_DOCUMENT_ROOT . '/core/lib/ajax.lib.php';
				$formul2=new Form($db);
				$comboenhancement =ajax_combobox('prod'.$i, $events, $conf->global->COMPANY_USE_SEARCH_TO_SELECT);
				$text.= $comboenhancement;
				$text.= "<select class='flat' name='prod".$i."' id='prod".$i."' style='width:100%'>";
				$text.= "<option value=''>--</option>";
				while($rsl=$db->fetch_object($rqs)){
					$text.= "<option value='".$rsl->ref."'>".$rsl->ref." - ".$rsl->label."</option>";
				}
				$text.= "</select>";
				$desc = $formul2->textwithpicto($text, '', 1, '', '', 0, 3);
				print $desc;
				unset($desc);
				unset($text);
				
				print "</td>";
				print "<td>";
				if($object->type==0){
					$a=" Selected ";
					$b="";
				}else{
					if($object->type==1){
						$b=" Selected ";
						$a="";
					}
				}
				print "<select name='tipoprod".$i."'>";
				print "<option value='1' ".$a.">Producto</option>";
				print "<option value='2' ".$b.">Servicio</option>";
				print "</select>";
				print "</td>";
				print "</tr>";
			}
			print "</table>";
			print "</td>";
			print "</tr>";
			if($receptorRFC==$conf->global->MAIN_INFO_SIREN){
				$sql="SELECT count(*) as exist FROM ".MAIN_DB_PREFIX."cfdimx_facturefourn WHERE xml='".$filename."'";
				$rsd=$db->query($sql);
				$rsf=$db->fetch_object($rsd);
				if($rsf->exist>0){
					print "<tr><td colspan='4' align='center' ><strong style='color:#F00;'>El XML ya esta relacionado a una Factura</strong></td></tr>";
				}else{
					print "<tr><td colspan='4' align='center' ><input type='submit' value='Generar Factura'></td></tr>";
				}
			}else{
				print "<tr><td colspan='4' align='center' ><strong style='color:#F00;'>El RFC del Receptor es diferente al Registrado en Dolibarr</strong></td></tr>";
			}
			print "</table></form>";
		}else{
			print "<strong style='color:#F00;'>Error: El archivo no es un XML</strong>";
		}
	}else{
		if($_REQUEST['oper']=='direcfacture'){
			$sql="INSERT INTO ".MAIN_DB_PREFIX."recepcionavanzada_costo
	    		(fk_reception,fk_facture_fourn,categoria,xml,pdf,type_fac)
	    		VALUES('".GETPOST('coste')."','".GETPOST('selecfac')."','".GETPOST('catefac')."'
    				,'','',".GETPOST('catefac2').")";
			$rf=$db->query($sql);
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.GETPOST('id').' and entity='.$conf->entity . ' and rowid='.GETPOST('coste');
				$res=$db->query($string);
			print "<script>window.location='".DOL_MAIN_URL_ROOT."/recepcionavanzada/recepcion.php?id=".GETPOST('id')."&act=card&type=1&coste=".GETPOST('coste')."';</script>";
		}else{
			if($statusdocto==0){
			print "<div>";//div padre
			print "<div style='display: inline-block;width: 45%;height: 250px;'>";
		 print "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec."&oper=enviarxml' enctype='multipart/form-data' onsubmit='return validar(this)'>";
			print "<table class='noborder' width='100%' style='text-align:left'>";
			print "<tr class='liste_titre'>";
			print "<td>Carga de XML y PDF de proveedor</td>";
			print "</tr>";
			print "<tr><td>XML: <input type='file' name='xmlfac' id='xmlfac' accept='.xml,.XML' required onchange=\'alert('ALGO')\'><td></tr>";
			print "<tr><td>PDF: <input type='file' name='pdffac' id='pdffac' accept='.pdf,.PDF'></td></tr>";
			$sqm="SELECT f.rowid as rowid, f.label 
					FROM ".MAIN_DB_PREFIX."c_recepcionavanzada_categoria_gasto  f
					WHERE f.active=1";
			$rsm=$db->query($sqm);
			print "<tr><td>";
			print "Categoria del gasto: <select name='catefac' id='catefac'>";
			while($rssm=$db->fetch_object($rsm)){
				print "<option value='".$rssm->rowid."'>".$rssm->label."</option>";
			}
			print "</select>";
			print "</td></tr>";
			//Asignar si es tipo Factura Aduana o Gasto pedimento
			print "<tr><td>";
			print "Factura Aduana/Gasto pedimento: <select name='catefac2' id='catefac2'>";
				print "<option value='0' default>Ninguna</option>";
				print "<option value='1'>Factua Aduana</option>";
				print "<option value='2'>Gasto Pedimento</option>";
			print "</select>";
			print "</td></tr>";
			//print "<tr><td>Categoria del gasto: <input type='text' name='catefac' id='catefac' required><td></tr>";
			print "<tr><td align='center'><input type='submit' value='Cargar Factura Prov.'></td></tr>";
			print "</table>";
		 print "</form>";
		 print '    <script language="JavaScript">
		    function validar(f){
		            //alert(f.xmlfac.value);
		 		value=f.xmlfac.value;
		 		if(value.match(/\.(xml)|(XML)$/)){
		 		 	return true;
				}else{
		 			window.location.href="recepcion.php?id='.$idp.'&act=card&type=1&coste='.$idrec.'&oper=errorarchivo";
		 			return false;
		 		}
		        
		    }
		    </script>';
		 if($_REQUEST['oper']=='errorarchivo'){
		 	dol_htmloutput_errors('El archivo que intenta cargar no es un XML');
		 	//print "AAA:";
		 }
		 print "</div>";
		 

		 print "<div style='display: inline-block;width: 10%;'>&nbsp;</div>";

		 print "<div style='display: inline-block;width: 45%;height: 250px;' >";
		 print '<script src="//www.appelsiini.net/projects/chained/jquery.chained.min.js"></script>';
		 
		 print "<form name='selmetod' id='selmetod' method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec."&oper=direcfacture'>";
			print "<table class='noborder' width='100%' style='text-align:left'>";
			print "<tr class='liste_titre'>";
			print "<td>Vincular factura de proveedor existente</td>";
			print "</tr>";
			print "<tr>";
			print "<td>Proveedor: ";
			$formul=new Form($db);
			//print $formul->select_company('', 'selecprov', 's.fournisseur = 1', 'SelectThirdParty');
			print $formul->select_company('', 'selecprov', 's.fournisseur = 1', 'SelectThirdParty',0, 0,'',0,'','style="width: 100%"');
			/* $sql="SELECT rowid, nom FROM ".MAIN_DB_PREFIX."societe WHERE fournisseur=1 AND entity=".$conf->entity;
			//print $sql;
			$rqs=$db->query($sql);
			print "<select name='selecprov' id='selecprov'>";
			print "<option value=''>--</option>";
			while($rsl=$db->fetch_object($rqs)){
				print "<option value='".$rsl->rowid."'>".$rsl->nom."</option>";
			}
			print "</select>"; */
			print "</td></tr>";
			print "<tr>";
			
			print "<td>Referencia/Folio de proveedor: ";
			$sql="SELECT rowid, ref, ref_supplier, fk_soc
				FROM ".MAIN_DB_PREFIX."facture_fourn
				WHERE entity=".$conf->entity."
						AND rowid NOT IN (SELECT fk_facture_fourn
						FROM ".MAIN_DB_PREFIX."recepcionavanzada_costo)";// AND fk_soc=";
			//print $sql;
			$rqs=$db->query($sql);
			include_once DOL_DOCUMENT_ROOT . '/core/lib/ajax.lib.php';
			$comboenhancement =ajax_combobox('selecfac', $events, $conf->global->COMPANY_USE_SEARCH_TO_SELECT);
			$text.= $comboenhancement;
			$text.= "<select class='flat' name='selecfac' id='selecfac'>";
			$text.= "<option value=''>--</option>";
			while($rsl=$db->fetch_object($rqs)){
				$text.= "<option value='".$rsl->rowid."' class='".$rsl->fk_soc."'>".$rsl->ref." / ".$rsl->ref_supplier."</option>";
			}
			$text.= "</select>";
			$desc = $formul->textwithpicto($text, '', 1, '', '', 0, 3);
			print $desc;
			print "</td></tr>";
			$sqm="SELECT f.rowid as rowid, f.label
					FROM ".MAIN_DB_PREFIX."c_recepcionavanzada_categoria_gasto  f
					WHERE f.active=1";
			$rsm=$db->query($sqm);
			print "<tr><td>";
			print "Categoria del gasto: <select name='catefac' id='catefac'>";
			while($rssm=$db->fetch_object($rsm)){
				print "<option value='".$rssm->rowid."'>".$rssm->label."</option>";
			}
			print "</select>";
			print "</td></tr>";
			//Asignar si es tipo Factura Aduana o Gasto pedimento
			print "<tr><td>";
			print "Factura Aduana/Gasto pedimento: <select name='catefac2' id='catefac2'>";
				print "<option value='0' default>Ninguna</option>";
				print "<option value='1'>Factua Aduana</option>";
				print "<option value='2'>Gasto Pedimento</option>";
			print "</select>";
			print "</td></tr>";
			//print "<tr><td>Categoria del gasto: <input type='text' name='catefac' id='catefac' required><td></tr>";
			print "<tr><td align='center'><input type='submit' value='Relacionar Factura Prov.'></td></tr>";
			print "</table></form>";
			print "<div>";
			print '<script>
			  $(function(){
			      $("#selecfac").chained("#selecprov");
					
			  });
			  </script>';
			
			print "</div>";//div padre
			}else{
				if($statusdocto!=0){
				print "El stock ya fue afectado, no es posible relacionar m&aacute;s Facturas/Gastos";
				}
			}
		}
	}
}



