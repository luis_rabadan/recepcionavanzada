CREATE TABLE IF NOT EXISTS `llx_recepcionavanzada_conf` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `entity` int(11) DEFAULT NULL,
  `bandConfNumSerie` int(11) DEFAULT NULL,
  `bandConfOrdCompra` int(11) DEFAULT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

ALTER TABLE `llx_recepcionavanzada_conf` add `bandConfgCostos` int(11) DEFAULT 1;
ALTER TABLE `llx_recepcionavanzada_conf` add `bandConfgCalcCst_Prc` int(11) DEFAULT 0; -- Realizar calculo de costo/precio de entrada Nuevo
