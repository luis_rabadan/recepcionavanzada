<?php
/* Copyright (C) 2003      Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2012 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis@dolibarr.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   mymodule     Module MyModule
 *  \brief      Example of a module descriptor.
 *				Such a file must be copied into htdocs/mymodule/core/modules directory.
 *  \file       htdocs/mymodule/core/modules/modMyModule.class.php
 *  \ingroup    mymodule
 *  \brief      Description and activation file for module MyModule
 */
include_once(DOL_DOCUMENT_ROOT ."/core/modules/DolibarrModules.class.php");


/**
 *  Description and activation class for module MyModule
 */
class modRecepcionavanzada extends DolibarrModules
{
	/**
	 *   Constructor. Define names, constants, directories, boxes, permissions
	 *
	 *   @param      DoliDB		$db      Database handler
	 */
	function modRecepcionavanzada($db)
	{
        global $langs,$conf,$langs;

        $this->db = $db;

		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 50800;
		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'recepcionavanzada';

		// Family can be 'crm','financial','hr','projects','products','ecm','technic','other'
		// It is used to group modules in module setup page
		$this->family = "products";
		// Module label (no space allowed), used if translation string 'ModuleXXXName' not found (where XXX is value of numeric property 'numero' of module)
		$this->name = preg_replace('/^mod/i','',get_class($this));
		// Module description, used if translation string 'ModuleXXXDesc' not found (where XXX is value of numeric property 'numero' of module)
		$this->description = "Modulo para la recepcion de stock avanzado";
		// Possible values for version are: 'development', 'experimental', 'dolibarr' or version
		$this->version = '1.0';
		// Key used in llx_const table to save module status enabled/disabled (where MYMODULE is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);
		// Where to store the module in setup page (0=common,1=interface,2=others,3=very specific)
		$this->special = 0;
		// Name of image file used for this module.
		// If file is in theme/yourtheme/img directory under name object_pictovalue.png, use this->picto='pictovalue'
		// If file is in module/img directory under name object_pictovalue.png, use this->picto='pictovalue@module'
		$this->picto='generic';

		// Defined all module parts (triggers, login, substitutions, menus, css, etc...)
		// for default path (eg: /mymodule/core/xxxxx) (0=disable, 1=enable)
		// for specific path of parts (eg: /mymodule/core/modules/barcode)
		// for specific css file (eg: /mymodule/css/mymodule.css.php)
		//$this->module_parts = array(
		//                        	'triggers' => 0,                                 // Set this to 1 if module has its own trigger directory
		//							'login' => 0,                                    // Set this to 1 if module has its own login method directory
		//							'substitutions' => 0,                            // Set this to 1 if module has its own substitution function file
		//							'menus' => 0,                                    // Set this to 1 if module has its own menus handler directory
		//							'barcode' => 0,                                  // Set this to 1 if module has its own barcode directory
		//							'models' => 0,                                   // Set this to 1 if module has its own models directory
		//							'css' => '/mymodule/css/mymodule.css.php',       // Set this to relative path of css if module has its own css file
		//							'hooks' => array('hookcontext1','hookcontext2')  // Set here all hooks context managed by module
		//							'workflow' => array('order' => array('WORKFLOW_ORDER_AUTOCREATE_INVOICE')) // Set here all workflow context managed by module
		//                        );
		$this->module_parts = array(
			'hooks' => array('ordersuppliercard')
			);
		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/mymodule/temp");
		$this->dirs = array();

		// Config pages. Put here list of php page, stored into mymodule/admin directory, to use to setup module.
		$this->config_page_url = array("admin.php@recepcionavanzada");
		//$this->config_page_url = array("recepAdvSetuppage.php@recepcionavanzada");

		// Dependencies
		$this->depends = array();		// List of modules id that must be enabled if this module is enabled
		$this->requiredby = array();	// List of modules id to disable if this one is disabled
		$this->phpmin = array(5,0);					// Minimum version of PHP required by module
		$this->need_dolibarr_version = array(4,0);	// Minimum version of Dolibarr required by module
		$this->langfiles = array("products","recepcionavanzada@recepcionavanzada");
		//$this->langfiles = array('stockavanzado@stockavanzado');

		// Constants
		// List of particular constants to add when module is enabled (key, 'chaine', value, desc, visible, 'current' or 'allentities', deleteonunactive)
		// Example: $this->const=array(0=>array('MYMODULE_MYNEWCONST1','chaine','myvalue','This is a constant to add',1),
		//                             1=>array('MYMODULE_MYNEWCONST2','chaine','myvalue','This is another constant to add',0)
		// );
		$this->const = array();

		$this->tabs = array('supplier_order:+taborderprov:Recepcion de stock avanzado:@hwtitle:true:/recepcionavanzada/recepcion.php?id=__ID__',
				'supplier_order:-dispatch');
		// Array to add new pages in new tabs
		// Example: $this->tabs = array('objecttype:+tabname1:Title1:langfile@mymodule:$user->rights->mymodule->read:/mymodule/mynewtab1.php?id=__ID__',  // To add a new tab identified by code tabname1
        //                              'objecttype:+tabname2:Title2:langfile@mymodule:$user->rights->othermodule->read:/mymodule/mynewtab2.php?id=__ID__',  // To add another new tab identified by code tabname2
        //                              'objecttype:-tabname');                                                     // To remove an existing tab identified by code tabname
		// where objecttype can be
		// 'thirdparty'       to add a tab in third party view
		// 'intervention'     to add a tab in intervention view
		// 'order_supplier'   to add a tab in supplier order view
		// 'invoice_supplier' to add a tab in supplier invoice view
		// 'invoice'          to add a tab in customer invoice view
		// 'order'            to add a tab in customer order view
		// 'product'          to add a tab in product view
		// 'stock'            to add a tab in stock view
		// 'propal'           to add a tab in propal view
		// 'member'           to add a tab in fundation member view
		// 'contract'         to add a tab in contract view
		// 'user'             to add a tab in user view
		// 'group'            to add a tab in group view
		// 'contact'          to add a tab in contact view
		// 'categories_x'	  to add a tab in category view (replace 'x' by type of category (0=product, 1=supplier, 2=customer, 3=member)
        
        
        // Dictionnaries
        //if (! isset($conf->mymodule->enabled)) $conf->mymodule->enabled=0;
		$this->dictionnaries=array(
'tabname'=>array("llx_c_recepcionavanzada_categoria_gasto"),        // List of tables we want to see into dictonnary editor
				'tablib'=>array("Categoria de Gastos"),                                                    // Label of tables
				'tabsql'=>array('SELECT f.rowid as rowid, f.label, f.active FROM '.MAIN_DB_PREFIX.'c_recepcionavanzada_categoria_gasto as f'),    // Request to select fields
				'tabsqlsort'=>array("label"),                                                                                    // Sort order
				'tabfield'=>array("label"),                                                                                    // List of fields (result of select to show dictionary)
				'tabfieldvalue'=>array("label"),                                                                                // List of fields (list of fields to edit a record)
				'tabfieldinsert'=>array("label"),                                                                            // List of fields (list of fields for insert)
				'tabrowid'=>array("rowid"),                                                                                                    // Name of columns with primary key (try to always name it 'rowid')
				'tabcond'=>array(1)   
);
        /* Example:
        if (! isset($conf->mymodule->enabled)) $conf->mymodule->enabled=0;	// This is to avoid warnings
        $this->dictionnaries=array(
            'langs'=>'mymodule@mymodule',
            'tabname'=>array(MAIN_DB_PREFIX."table1",MAIN_DB_PREFIX."table2",MAIN_DB_PREFIX."table3"),		// List of tables we want to see into dictonnary editor
            'tablib'=>array("Table1","Table2","Table3"),													// Label of tables
            'tabsql'=>array('SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table1 as f','SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table2 as f','SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table3 as f'),	// Request to select fields
            'tabsqlsort'=>array("label ASC","label ASC","label ASC"),																					// Sort order
            'tabfield'=>array("code,label","code,label","code,label"),																					// List of fields (result of select to show dictionnary)
            'tabfieldvalue'=>array("code,label","code,label","code,label"),																				// List of fields (list of fields to edit a record)
            'tabfieldinsert'=>array("code,label","code,label","code,label"),																			// List of fields (list of fields for insert)
            'tabrowid'=>array("rowid","rowid","rowid"),																									// Name of columns with primary key (try to always name it 'rowid')
            'tabcond'=>array($conf->mymodule->enabled,$conf->mymodule->enabled,$conf->mymodule->enabled)												// Condition to show each dictionnary
        );
        */

        // Boxes
		// Add here list of php file(s) stored in core/boxes that contains class to show a box.
        $this->boxes = array();			// List of boxes
		$r=0;
		// Example:
		/*
		$this->boxes[$r][1] = "myboxa.php";
		$r++;
		$this->boxes[$r][1] = "myboxb.php";
		$r++;
		*/

		// Permissions
		$this->rights = array();		// Permission array used by this module
		$this->rights_class = 'recepcionavanzada';
		$r=0;
		// Add here list of permission defined by an id, a label, a boolean and two constant strings.
		// Example:
		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Eliminar orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'delete_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;

		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Crear/Modificar Orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'edit_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;

		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Validar/Recibir Orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'valid_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;
		
		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Cerrar Orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'close_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;

		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Reabrir Orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'reopen_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;

		$this->rights[$r][0] = $this->numero+$r; 				// Permission id (must not be already used)
		$this->rights[$r][1] = 'Exportar Orden de recepci&oacute;n';	// Permission label
		$this->rights[$r][3] = 0; 					// Permission by default for new user (0/1)
		$this->rights[$r][4] = 'export_recep';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$this->rights[$r][5] = '';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		$r++;
		


		// Add here list of permission defined by an id, a label, a boolean and two constant strings.
		// Example:
		// $this->rights[$r][0] = 2000; 				// Permission id (must not be already used)
		// $this->rights[$r][1] = 'Permision label';	// Permission label
		// $this->rights[$r][3] = 1; 					// Permission by default for new user (0/1)
		// $this->rights[$r][4] = 'level1';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		// $this->rights[$r][5] = 'level2';				// In php code, permission will be checked by test if ($user->rights->permkey->level1->level2)
		


		// Main menu entries
		$this->menus = array();			// List of menus to add
		$r=0;

		/* $this->menu[$r]=array(    'fk_menu'=>'fk_mainmenu=commercial',            // Use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode of parent menu
                 'type'=>'left',                    // This is a Left menu entry
                 'titre'=>'Recepci&oacute;n avanzada ',
                 //'mainmenu'=>'stockadv',
                 'leftmenu'=>'recepcionavan',
                  'url'=>'/recepcionavanzada/recepcionavanzadarecepcion_list.php?mainmenu=commercial&leftmenu=#',
                 'langs'=>'main',    // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
                 'position'=>100,
                 'enabled'=>'1',            // Define condition to show or hide menu entry. Use '$conf->mymodule->enabled' if entry must be visible if module is enabled.
                 'perms'=>'1',            // Use 'perms'=>'$user->rights->mymodule->level1->level2' if you want your menu with a permission rules
                 'target'=>'',
                 'user'=>2);
         $r++; */

		$this->menu[$r]=array(    'fk_menu'=>'fk_mainmenu=products,fk_leftmenu=stock',            // Use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode of parent menu
                 'type'=>'left',                    // This is a Left menu entry
                 'titre'=>'Lista ordenes de recepci&oacute;n',
                 'mainmenu'=>'recepcionAvanz',
                 'url'=>'/recepcionavanzada/recepcionavanzadarecepcion_list.php?mainmenu=commercial&leftmenu=',
                 'langs'=>'main',    // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
                 'position'=>101,
                 'enabled'=>'1',            // Define condition to show or hide menu entry. Use '$conf->mymodule->enabled' if entry must be visible if module is enabled.
                 'perms'=>'1',            // Use 'perms'=>'$user->rights->mymodule->level1->level2' if you want your menu with a permission rules
                 'target'=>'',
                 'user'=>2);
         $r++;
        

		
		
	$this->export_code[$r]=$this->rights_class.'_'.$r;
		$this->export_label[$r]='Modulo para la recepcion de stock avanzado';	// Translation key (used only if key ExportDataset_xxx_z not found)
        $this->export_enabled[$r]='1';                               // Condition to show export in list (ie: '$user->id==3'). Set to 1 to always show when module is enabled.
		$this->export_permission[$r]=array(array( "recepcionavanzada","export_recep"));
		$this->export_fields_array[$r]=array(
			'a.rowid' => 'ID de empresa',
			'a.nom' => 'Razón social',
			'a.address' => 'Address',
			'b.ref' => 'Ref.',
			'b.ref_supplier' => 'Ref. Proveedor',
			'b.date_commande' => 'Fecha de pedido',
			'b.total_ht' => 'Importe',
			'b.tva' => 'Total IVA',
			'b.total_ttc' => 'Total',
			'd.label2' => 'Estatus',
			'b.date_approve' => 'Fecha de aprobación',
			'c.login' => 'O.C. Aprobada por',
			'e.rowid' => 'rowid',
			'e.ref' => 'Orden Ref',
			'e.date_orderreception' => 'date_orderreception',
			'e.time_orderreception' => 'time_orderreception',
			'f.login' => 'Recepcion usuario',
			'g.label' => 'Almacen',
			'e.exchange_rate' => 'exchange_rate',
			'e.status_import_declaration' => 'status_import_declaration',
			'e.import_declaration' => 'import_declaration',
			'e.date_import_declaration' => 'date_import_declaration',
			'e.aduana' => 'aduana',
			'e.note_public' => 'note_public',
			'e.note_private' => 'note_private',
			'e.fk_user_creation' => 'fk_user_creation',
			'e.date_creation' => 'date_creation',
			'e.date_validation' => 'date_validation',
			'h.login' => 'User validation',
			'i.login' => 'User Cierre',
			'e.date_close' => 'date_close',
			'm.label' => 'Estatus Orden',
			'k.ref' => 'Codigo de producto',
			'j.qty' => 'Cantidad recibida',
			'j.eatby' => 'Fecha caducidad',
			'j.sellby' => 'Fecha límite de venta',
			'j.batch' => 'Número de serie/Lote',
			'l.label' => 'Almacén a ingresar',
			'k.label' => 'Descripción de producto'
		);

		//fields
		$this->export_TypeFields_array[$r]=array(
			'a.rowid' => 'Text',
			'a.nom' => 'Text',
			'a.address' => 'Text',
			'b.ref' => 'Text',
			'b.ref_supplier' => 'Text',
			'b.date_livraison' => 'Date',
			'b.total_ht' => 'Numeric',
			'b.tva' => 'Numeric',
			'b.total_ttc' => 'Numeric',
			'd.label2' => 'Text',
			'b.date_approve' => 'Date',
			'c.login' => 'Text',
			'e.rowid' => 'List:recepcionavanzada_recepcion:ref',
			'e.ref' => 'Text',
			'e.date_orderreception' => 'Date',
			'f.login' => 'Text',
			'g.label' => 'Text',
			'e.exchange_rate' => 'Text',
			'e.status_import_declaration' => 'Text',
			'e.import_declaration' => 'Numeric',
			'e.date_import_declaration' => 'Date',
			'e.aduana' => 'Text',
			'e.note_public' => 'Text',
			'e.note_private' => 'Text',
			'e.fk_user_creation' => 'List:user:login',
			'e.date_creation' => 'Date',
			'e.date_validation' => 'Date',
			'h.login' => 'Text',
			'i.login' => 'Text',
			'e.date_close' => 'Date',
			'm.label' => 'Text',
			'k.ref' => 'Text',
			'j.qty' => 'Numeric',
			'j.eatby' => 'Date',
			'j.sellby' => 'Date',
			'j.batch' => 'Numeric',
			'l.label' => 'Text',
			'k.label' => 'Text'
		);







		$this->export_entities_array[$r]=array(
			'a.rowid' => 'company',
			'a.nom' => 'company',
			'a.address' => 'company',
			'b.ref' => 'Pedido a proveedor/Orden de compra.',
			'b.ref_supplier' => 'Pedido a proveedor/Orden de compra',
			'b.date_livraison' => 'Pedido a proveedor/Orden de compra',
			'b.total_ht' => 'Pedido a proveedor/Orden de compra',
			'b.tva' => 'Pedido a proveedor/Orden de compra',
			'b.total_ttc' => 'Pedido a proveedor/Orden de compra',
			'd.label2' => 'Pedido a proveedor/Orden de compra',
			'b.date_approve' => 'Pedido a proveedor/Orden de compra',
			'c.login' => 'User',
			'e.rowid' => 'Orden de recepción de stock ',
			'e.ref' => 'Orden de recepción de stock ',
			'e.date_orderreception' => 'Orden de recepción de stock ',
			'e.time_orderreception' => 'Orden de recepción de stock',
			'f.login' => 'Orden de recepción de stock',
			'g.label' => 'Orden de recepción de stock',
			'e.exchange_rate' => 'Orden de recepción de stock',
			'e.status_import_declaration' => 'Orden de recepción de stock',
			'e.import_declaration' => 'Orden de recepción de stock',
			'e.date_import_declaration' => 'Orden de recepción de stock',
			'e.aduana' => 'Orden de recepción de stock',
			'e.note_public' => 'Orden de recepción de stock',
			'e.note_private' => 'Orden de recepción de stock',
			'e.fk_user_creation' => 'Orden de recepción de stock',
			'e.date_creation' => 'Orden de recepción de stock',
			'e.date_validation' => 'Orden de recepción de stock',
			'h.login' => 'Orden de recepción de stock',
			'i.login' => 'Orden de recepción de stock',
			'e.date_close' => 'Orden de recepción de stock',
			'm.label' => 'Orden de recepción de stock',
			'k.ref' => 'Línea de Orden de recepción de stock',
			'j.qty' => 'Línea de Orden de recepción de stock',
			'j.eatby' => 'Línea de Orden de recepción de stock',
			'j.sellby' => 'Línea de Orden de recepción de stock',
			'j.batch' => 'Línea de Orden de recepción de stock',
			'l.label' => 'Línea de Orden de recepción de stock',
			'k.label' => 'product'
		);
		$this->export_sql_start[$r]='SELECT DISTINCT ';
		$this->export_sql_end[$r]  .=' FROM llx_societe as a';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_commande_fournisseur as b on b.fk_soc=a.rowid';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_user as c on c.rowid=b.fk_user_approve';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_status as d on d.statut=b.fk_statut';
		$this->export_sql_end[$r]  .=' INNER JOIN llx_recepcionavanzada_recepcion as e on e.fk_order_supplier=b.rowid';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_user as f on f.rowid=e.fk_user_reception';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_entrepot as g on g.rowid=e.fk_user_warehouse';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_user as h on h.rowid=e.fk_user_validation';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_user as i on i.rowid=e.fk_user_close';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_commande_fournisseur_dispatch as j on j.fk_commande=b.rowid and j.fk_order_reception=e.rowid';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_product AS k ON j.fk_product = k.rowid';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_entrepot as l on l.rowid=j.fk_entrepot';
		$this->export_sql_end[$r]  .=' LEFT JOIN llx_status as m on m.statut=e.status ';
		$this->export_sql_end[$r]  .=' WHERE c.statut=1 ';
		$r++;
	}

	/**
	 *		Function called when module is enabled.
	 *		The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *		It also creates data directories
	 *
     *      @param      string	$options    Options when enabling module ('', 'noboxes')
	 *      @return     int             	1 if OK, 0 if KO
	 */
	function init($options='')
	{
		$sql = array();

		$result=$this->load_tables();

		return $this->_init($sql, $options);
	}

	/**
	 *		Function called when module is disabled.
	 *      Remove from database constants, boxes and permissions from Dolibarr database.
	 *		Data directories are not deleted
	 *
     *      @param      string	$options    Options when enabling module ('', 'noboxes')
	 *      @return     int             	1 if OK, 0 if KO
	 */
	function remove($options='')
	{
		$sql = array();

		return $this->_remove($sql, $options);
	}


	/**
	 *		Create tables, keys and data required by module
	 * 		Files llx_table1.sql, llx_table1.key.sql llx_data.sql with create table, create keys
	 * 		and create data commands must be stored in directory /mymodule/sql/
	 *		This function is called by this->init
	 *
	 * 		@return		int		<=0 if KO, >0 if OK
	 */
	function load_tables()
	{
		return $this->_load_tables('/recepcionavanzada/sql/');
	}
}

?>
