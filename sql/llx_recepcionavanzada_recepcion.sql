CREATE TABLE IF NOT EXISTS `llx_recepcionavanzada_recepcion` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `fk_order_supplier` int(11) NOT NULL,
  `ref` varchar(50) NOT NULL,
  `date_orderreception` date DEFAULT NULL,
  `time_orderreception` time DEFAULT NULL,
  `fk_user_reception` int(11) DEFAULT NULL,
  `fk_user_warehouse` int(11) DEFAULT NULL,
  `exchange_rate` double DEFAULT NULL,
  `status_import_declaration` int(11) DEFAULT NULL,
  `import_declaration` varchar(30) DEFAULT NULL,
  `date_import_declaration` date DEFAULT NULL,
  `aduana` varchar(30) DEFAULT NULL,
  `note_public` text,
  `note_private` text,
  `fk_user_creation` int(11) DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_validation` datetime DEFAULT NULL,
  `fk_user_validation` int(11) DEFAULT NULL,
  `fk_user_close` int(11) DEFAULT NULL,
  `date_close` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `attach_files` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`rowid`)
);

ALTER TABLE `llx_recepcionavanzada_recepcion` ADD COLUMN `entity` int(11) DEFAULT 1;

