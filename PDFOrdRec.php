<?php
	//error_reporting(1);
	ini_set("memory_limit", "128M");
	require('../main.inc.php');
	require_once("lib/dom/dompdf_config.inc.php");
	require_once("lib/dom/numero_a_letra.php");
	
	$id = $_GET['id'];
	$idrec = $_GET['idrec'];
	

	//Datos Emisor
	$e_nom = $conf->global->MAIN_INFO_SOCIETE_NOM;
	$e_address = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_ADDRESS));
	$e_zip =ucwords(strtolower( $conf->global->MAIN_INFO_SOCIETE_ZIP));
	$e_town = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_TOWN)); 
	$e_tel =ucwords(strtolower( $conf->global->MAIN_INFO_SOCIETE_TEL));
	$e_fax = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_FAX));
	$e_mail = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_MAIL));
	$e_web = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_WEB));
	$e_rfc = ucwords(strtolower($conf->global->MAIN_INFO_SIREN));
	$e_country = ucwords(strtolower($conf->global->MAIN_INFO_SOCIETE_COUNTRY));
	$pais= explode(":", $e_country);

	

	//Datos del cliente
	

	if (! empty($mysoc->logo_mini) && is_readable($conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_mini))
	{
		$urllogo=DOL_DOCUMENT_ROOT.'/viewimage.php?cache=1&amp;modulepart=companylogo&amp;file='.urlencode('thumbs/'.$mysoc->logo_mini);
	}
	else
	{
		$urllogo=DOL_DOCUMENT_ROOT.'/theme/dolibarr_logo.png';
	}
	$url_logo = $conf->mycompany->dir_output.'/logos/'.$conf->global->MAIN_INFO_SOCIETE_LOGO;

	$stringProv='SELECT
				a.ref AS refPedido,				
				b.nom as nomProv,
				b.siren,
				b.address,
				b.town,
				b.zip,
				b.phone,
				c.ref,
				c.exchange_rate,
				c.fk_user_warehouse,
				c.status_import_declaration,
				c.import_declaration,
				c.date_import_declaration,
				c.aduana,
				date(c.date_orderreception) as date_close,
				c.fk_user_reception
				FROM
					'.MAIN_DB_PREFIX.'commande_fournisseur AS a
				INNER JOIN 
					'.MAIN_DB_PREFIX.'societe as b on a.fk_soc=b.rowid
				INNER JOIN
					'.MAIN_DB_PREFIX.'recepcionavanzada_recepcion as c on c.fk_order_supplier=a.rowid
				WHERE
					a.rowid ='.$id.'
					and c.rowid='.$idrec;
				//	echo $stringProv;

					
	$query=$db->query($stringProv);
	$nrd=$db->num_rows($query);
		if($nrd>0){
			$dataProv=$db->fetch_object($query);
			$proveedor=$dataProv->nomProv;
			$rfc=$dataProv->siren;
			$direc=$dataProv->address;
			$ciudad=$dataProv->town;
			$cp=$dataProv->zip;
			$telefono=$dataProv->phone;
			$folio=$dataProv->refPedido;
			$refOrder=$dataProv->ref;			
			$tipo=$dataProv->exchange_rate;
			$userReception=$dataProv->fk_user_reception;

			$sqlAlm='SELECT lieu from '.MAIN_DB_PREFIX.'entrepot where rowid='.$dataProv->fk_user_warehouse;
			$queryAlm=$db->query($sqlAlm);
			$dataALm=$db->fetch_object($queryAlm);
			$ubica=$dataALm->lieu;

			$statusImport=$dataProv->status_import_declaration;
			$impor=$dataProv->import_declaration;
			$dateImpor=$dataProv->date_import_declaration;
			$aduana=$dataProv->aduana;
			$close=$dataProv->date_close;


		}

	$date = date_create($close);

	$div_cabecera = '<div class="cabecera2">
						<table  width="100%">
							<tr>
								<td >
									
										<img width="240px" src="'.$url_logo.'" />
									
								</td>
								<td width="60%">
									<div align="center"><b>'.$e_nom.'</b><br/></div>
									<div align="center"><font size="10px">'.$e_address.' '.$e_town.'</font><br/></div>
									<div align="center"><font size="10px"> '.ucwords($pais[2]).'	 C.P. '.$e_zip.' R.F.C. '.$e_rfc.'</font><br/></div>
									<div align="center"><font size="10px">	Tel. '.$e_tel.' '.$e_web.'</font><br/>	</div>	
								</td>	
								<td >
									<div align="center"><fieldset id="circ"><font size="10px"><b>CODIGO</b></font></fieldset></div>
									<div align="center">RE-ALM-01</div>
									<div align="center"><fieldset id="circ"><font size="10px"><b>Fecha recibo</b></font></fieldset></div>
									<div align="center">'.date_format($date, 'd/m/Y').'</div>
								</td>	
							</tr>
						</table>
					</div>
					<div class="cabecera2" align="center"><font size="14px"><b>Recibo de artículos</font></b></div>';

	/**************datos proveedor*******************/

	$divisadocumento=$conf->currency;
		
	if($conf->global->MAIN_MODULE_MULTIDIVISA){
		$sqld="SELECT divisa FROM ".MAIN_DB_PREFIX."multidivisa_commande_fournisseur WHERE fk_object=".$id;
		//print $sqld;
		$rqd=$db->query($sqld);
		$nrd=$db->num_rows($rqd);
		if($nrd>0){
			$rsld=$db->fetch_object($rqd);
			$divisadocumento=$rsld->divisa;
		}
	}

	

	$div_cabecera2 = '<div class="cabecera2">
					 <fieldset>
						<table  width="100%" align="center" >
							<tr>
								<td>
									<div align="left"><b>Proveedor: </b>'.$proveedor.'</br></div>
									<div align="left"><b>RFC/ID Fiscal: </b>'.$rfc.'</br></div>
									<div align="left"><b>Dirección: </b>'.$direc.'</br></div>
									<div  align="left"><b>Ciudad: </b>'.$ciudad.'<b> C.P.: </b>'.$cp.'</br></div>	
									<div  align="left"><b>Teléfono: </b>'.$telefono.'</br></div>	
								</td>
								<td>
									<div align="left"><b>Folio del recibo: </b>'.$refOrder.'</br></div>
									<div align="left"><b>Referente a Orden de compra: </b>'.$folio.'</br></div>
									<div align="left"><b>Moneda de la O. de C.: </b>'.$divisadocumento.'</br></div>
									<div align="left"><b>Tipo de cambio de recepción: </b>'.$tipo.'</br>	</div>	
									<div align="left"><b>Ubicación de destino (Almacén): </b>'.$ubica.'</br></div>									
								</td>									
							</tr>
						</table>
					 </fieldset>
					</div>
					';
	$div_medio = '';

	if($statusImport==1){
		$imp=date('d/m/Y',strtotime($dateImpor));
		$div_medio = '<div class="medio">
					 <fieldset>
						<table  width="100%" align="center" >
							<tr>
								<td>
									<div align="left"><b>Pedimento de importación: </b>'.$impor.'</br></div>									
								</td>
								<td>
									<div align="left"><b>Fecha de pedimento: </b>'.$imp.'</br></div>									
								</td>
								<td>
									<div align="left"><b>Aduana:</b>'.$aduana.'</br></div>									
								</td>									
							</tr>
						</table>
					 </fieldset>
					</div>
					';	
	}
	

	

	$div_contenido = '<div class="contenido">
					 <fieldset>
						<table width="100%" align="center" >
							<tr class="cabeceraTabla">
								<td width="5%">Cant.</td>
								<td width="15%">Código</td>
								<td width="35%">Descripción</td>
								<td width="20%">Número de serie</td>	
								<td align="right" width="15%">Precio de compra</td>
								<td align="right" width="10%">Subtotal</td>								
							</tr>';

	$stringProd='SELECT
					a.qty,
					a.fk_product,
					a.batch,
					b.ref,
					b.label,
					c.subprice as price,
					c.tva_tx as iva
				FROM
					'.MAIN_DB_PREFIX.'commande_fournisseur_dispatch AS a
				INNER JOIN '.MAIN_DB_PREFIX.'product as b on b.rowid= a.fk_product
				INNER JOIN '.MAIN_DB_PREFIX.'commande_fournisseurdet as c on a.fk_commande=c.fk_commande 
						AND a.fk_product=c.fk_product
				WHERE
					a.fk_commande ='.$id.'
					and a.fk_order_reception='.$idrec;
			

	$queryProd=$db->query($stringProd);
	$numProd=$db->num_rows($queryProd);

	$suma=0;
	$iva=0;

	if($numProd>0){

		while ($datProd=$db->fetch_object($queryProd)) {
			$sub=$datProd->qty*$datProd->price;
			$subtt=round($sub,2);
			
			$div_contenido .= '
							<tr>
								<td width="5%">'.$datProd->qty.'</td>								
								<td width="15%">'. wordwrap($datProd->ref,15,"<br>", true).'</td>
								<td width="35%">'.$datProd->label.'</td>
								<td width="20%">'.$datProd->batch.'</td>	
								<td width="15%" align="right">'.number_format($datProd->price ,2).'</td>
								<td width="10%" align="right">'.number_format($subtt,2).'</td>								
							</tr>
						
					';
			$suma+=$subtt;
			$iva+=(($datProd->price*$datProd->iva)/100)*$datProd->qty;
		}
	}

	//$iva=round((($suma*16)/100),2);
	$total=$suma+$iva;

	$div_contenido .= '	
						</table>
					 </fieldset>
					
					 	<table width="100%">
					 		<tr>
								<td width="65%" colspan="3"><td>
								<td class="cabeceraTabla">Subtotal</td>
								<td align="right">$'.number_format($suma,2).'</td>	
							</tr>
							<tr>
								<td colspan="3"><td>
								<td class="cabeceraTabla">I.V.A.</td>
								<td align="right">$'.number_format($iva,2).'</td>	
							</tr>
							<tr>
								<td colspan="3"><td>
								<td class="cabeceraTabla">Total</td>
								<td align="right">$'.number_format($total,2).'</td>							
							</tr>
						</table>
					
					</div>';

	$sqlNot='SELECT note_public from '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion where fk_order_supplier='.$id.' and rowid='.$idrec;
	$queryNot=$db->query($sqlNot);
	$numNote=$db->num_rows($queryNot);
	$notasPublic='';
	if($numNote>0){

		while ($dataNot=$db->fetch_object($queryNot)){
			$notasPublic.=' '.$dataNot->note_public;
		}
	}
	

	$div_nota='<div class="nota">
					 <fieldset>
						<div>Notas: '.$notasPublic.'
						</div>
					 </fieldset>
					</div>
					';
	if($conf->global->MAIN_MODULE_MULTIDIVISA){
		$stringCost='SELECT
					e.divisa as multicurrency_code,
					a.rowid,
					a.categoria,
					d.label,
					a.fk_facture_fourn,
					b.ref,
					b.ref_supplier,
					b.fk_soc,
					b.datef,
					b.total_ht,
					b.total_tva,
					b.total_ttc,
					c.nom
				FROM
					'.MAIN_DB_PREFIX.'recepcionavanzada_costo a
				LEFT JOIN '.MAIN_DB_PREFIX.'c_recepcionavanzada_categoria_gasto d ON a.categoria = d.rowid,
				 '.MAIN_DB_PREFIX.'facture_fourn b,
				 '.MAIN_DB_PREFIX.'societe c,
				 	'.MAIN_DB_PREFIX.'multidivisa_facture_fourn e
				WHERE
					a.fk_reception = '.$idrec.'
				AND a.fk_facture_fourn = b.rowid
				AND b.fk_soc = c.rowid
				AND b.rowid=e.fk_object';
	}else{
	$stringCost='SELECT
					b.multicurrency_code,
					a.rowid,
					a.categoria,
					d.label,
					a.fk_facture_fourn,
					b.ref,
					b.ref_supplier,
					b.fk_soc,
					b.datef,
					b.total_ht,
					b.total_tva,
					b.total_ttc,
					c.nom
				FROM
					'.MAIN_DB_PREFIX.'recepcionavanzada_costo a
				LEFT JOIN '.MAIN_DB_PREFIX.'c_recepcionavanzada_categoria_gasto d ON a.categoria = d.rowid,
				 '.MAIN_DB_PREFIX.'facture_fourn b,
				 '.MAIN_DB_PREFIX.'societe c
				WHERE
					a.fk_reception = '.$idrec.'
				AND a.fk_facture_fourn = b.rowid
				AND b.fk_soc = c.rowid';
	}
	$queryCos=$db->query($stringCost);
	$numCost=$db->num_rows($queryCos);
	$div_costos='';
	if($numCost>0){
		$div_costos .= '<div class="contenido">
					 <fieldset>
					 	Gastos de Entrega/ Facturas
						<table  width="100%" align="center" >
							<tr >
								<td width="12%">Folio Fac..</td>
								<td width="12%">Fecha Fac.</td>
								<td width="25%">Proveedor</td>
								<td width="15%" align="center">Categoria</td>									
								<td width="13%" align="right">Subtotal</td>	
								<td width="13%" align="right">Impuestos</td>	
								<td width="13%" align="right">Total</td>						
								<td width="13%" align="right">Moneda</td>						
							</tr>';
		$ban=0;
		$mon=$divisadocumento;
		$sumSubt=0;
		$sumIva=0;
		$sumTotal=0;
		$i=0;

		$cost=array();
		while ($dataCos=$db->fetch_object($queryCos)){
			$cost[]=$dataCos;
		}
		//while ($dataCos=$db->fetch_object($queryCos)){
		foreach ($cost as $dataCos){

			$sql='select label from '.MAIN_DB_PREFIX.'c_recepcionavanzada_categoria_gasto where rowid='.$dataCos->categoria;
			$query=$db->query($sql);
			$num=$db->num_rows($query);
			
			if($num>0){

				$data=$db->fetch_object($query);
				$cat=$data->label;
			}

			$iva=$dataCos->total_tva;
			$total=$dataCos->total_ttc;	

			$sumSubt+=$dataCos->total_ht;
			$sumIva+=$iva;
			$sumTotal+=$total;	

			if(!is_null($cost[$i+1])){
				if(strcmp($cost[$i]->multicurrency_code, $cost[$i+1]->multicurrency_code)!=0){					
					$ban=1;	
				}
			}
			
			// if(strcmp($mon, $dataCos->multicurrency_code)!=0){
			// //	echo $mon.' x '.$dataCos->multicurrency_code;
			// 	$ban=1;
			// }
			$mon=$dataCos->multicurrency_code;
			$dat=date('d/m/Y',strtotime($dataCos->datef));
			$div_costos .= '
							<tr >
								<td>'.$dataCos->ref.'</td>
								<td>'.$dat.'</td>
								<td>'.$dataCos->nom.'</td>
								<td align="center">'.$cat.'</td>					
								<td align="right">'.number_format($dataCos->total_ht,2).'</td>
								<td align="right">'.number_format($iva,2).'</td>
								<td align="right">'.number_format($total,2).'</td>		
								<td align="right">'.$dataCos->multicurrency_code.'</td>					
							</tr>';
			$i++;
		}

		if($ban==0){
			$div_costos .= '	
						</table>
					 </fieldset>
					
					 	<table width="100%" align="left">
					 		<tr>
								<td width="12%" ></td>	
								<td width="12%" ></td>	
								<td width="25%" ></td>									
								<td width="15%" class="cabeceraTabla">Total</td>
								<td width="13%" align="right" >$'.number_format($sumSubt,2).'</td>	
								<td width="13%" align="right" >$'.number_format($sumIva,2).'</td>	
								<td width="13%" align="right">$'.number_format($sumTotal,2).'</td>
								<td width="13%" ></td>	
							</tr>							
						</table>
					
					</div>';
		}else{
			$div_costos .= '	
						</table>
					 </fieldset>				
					</div>';
		}
		
	}

	$sql='select CONCAT(firstname, " ", lastname ) as nom from '.MAIN_DB_PREFIX.'user where rowid='.$userReception;
	$query=$db->query($sql);
	$num=$db->num_rows($query);
	
	if($num>0){

		$data=$db->fetch_object($query);
		$user=$data->nom;
	}
	$div_pie='</br></br>
			<div class="pie" align="left" border="1px"  >
				<div><HR width=100% align="left"></div>
				<div align="center">Recibio:</div>					 
				<div align="center"> '.$user.'</div>					 
			</div>
					';


	$html ='<html>
			<head><link type="text/css" href="style.css" rel="stylesheet" /></head>
			<body>
				'.$div_cabecera.'
				'.$div_cabecera2.'
				'.$div_medio.'
				'.$div_contenido.'	
				'.$div_nota.'		
				'.$div_costos.'	
				'.$div_pie.'			
			</body>
			</html>';


	
			//echo $html;

	header('Content-type: application/pdf');

			
	$trigger = "
		<script type='text/javascript'>
			//this.print();
			//this.closeDoc(true);
		</script>
	";
	
				
	$dompdf = new DOMPDF();
	$dompdf->set_paper("A4", "portrait");
	$dompdf->load_html($html."{$trigger}</body></html>");
	
	$dompdf->render();
	$canvas = $dompdf->get_canvas();
	$font = Font_Metrics::get_font("helvetica", "bold");
	//$canvas->page_text(535, 825, "Pág. {PAGE_NUM} de {PAGE_COUNT}", $font, 7, array(0,0,0));
	$canvas->page_text(795, 570, "Pág. {PAGE_NUM} de {PAGE_COUNT}", $font, 7, array(0,0,0));
	$pdf = $dompdf->output();
	if(file_exists(DOL_DATA_ROOT.'/fournisseur/commande/reception')){/*print "EXISTE<br>";*/}else{
		mkdir(DOL_DATA_ROOT.'/fournisseur/commande/reception',0755);/*print "NO EXISTE<br>";*/}
	if(file_exists(DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec)){print "EXISTE<br>";}else{
		mkdir(DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec,0755);/*rint "NO EXISTE<br>";*/}

	$file_location=DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec.'/'.$refOrder.'.pdf';
	
	file_put_contents($file_location,$pdf); 

	

	header("Location: ../recepcionavanzada/recepcion.php?id=".$id."&act=card&type=1&idrec=".$idrec);
	//echo $dompdf->output();
?>
