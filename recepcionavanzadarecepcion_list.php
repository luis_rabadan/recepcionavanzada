<?php
/* Copyright (C) 2007-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *   	\file       recepcionavanzada/recepcionavanzadarecepcion_list.php
 *		\ingroup    recepcionavanzada
 *		\brief      This file is an example of a php page
 *					Initialy built by build_class_from_table on 2017-01-19 17:41
 */

//if (! defined('NOREQUIREUSER'))  define('NOREQUIREUSER','1');
//if (! defined('NOREQUIREDB'))    define('NOREQUIREDB','1');
//if (! defined('NOREQUIRESOC'))   define('NOREQUIRESOC','1');
//if (! defined('NOREQUIRETRAN'))  define('NOREQUIRETRAN','1');
//if (! defined('NOCSRFCHECK'))    define('NOCSRFCHECK','1');			// Do not check anti CSRF attack test
//if (! defined('NOSTYLECHECK'))   define('NOSTYLECHECK','1');			// Do not check style html tag into posted data
//if (! defined('NOTOKENRENEWAL')) define('NOTOKENRENEWAL','1');		// Do not check anti POST attack test
//if (! defined('NOREQUIREMENU'))  define('NOREQUIREMENU','1');			// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))  define('NOREQUIREHTML','1');			// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))  define('NOREQUIREAJAX','1');
//if (! defined("NOLOGIN"))        define("NOLOGIN",'1');				// If this page is public (can be called outside logged session)

// Change this following line to use the correct relative path (../, ../../, etc)
$res=0;
if (! $res && file_exists("../main.inc.php")) $res=@include '../main.inc.php';					// to work if your module directory is into dolibarr root htdocs directory
if (! $res && file_exists("../../main.inc.php")) $res=@include '../../main.inc.php';			// to work if your module directory is into a subdir of root htdocs directory
if (! $res && file_exists("../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../dolibarr/htdocs/main.inc.php';     // Used on dev env only
if (! $res && file_exists("../../../../dolibarr/htdocs/main.inc.php")) $res=@include '../../../../dolibarr/htdocs/main.inc.php';   // Used on dev env only
if (! $res) die("Include of main fails");
// Change this following line to use the correct relative path from htdocs
require_once(DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php');
require_once DOL_DOCUMENT_ROOT.'/core/lib/date.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
dol_include_once('/recepcionavanzada/class/recepcionavanzadarecepcion.class.php');

// Load traductions files requiredby by page
$langs->load("recepcionavanzada");
$langs->load("other");

// Get parameters
$id			= GETPOST('id','int');
$action		= GETPOST('action','alpha');
$backtopage = GETPOST('backtopage');
$myparam	= GETPOST('myparam','alpha');


$search_fk_order_supplier=GETPOST('search_fk_order_supplier','int');
$search_ref=GETPOST('search_ref','alpha');
$search_date_orderreception=GETPOST('search_date_orderreception','alpha');
$search_time_orderreception=GETPOST('search_time_orderreception','alpha');
$search_fk_user_reception=GETPOST('search_fk_user_reception','int');
$search_fk_user_warehouse=GETPOST('search_fk_user_warehouse','int');
$search_exchange_rate=GETPOST('search_exchange_rate','alpha');
$search_status_import_declaration=GETPOST('search_status_import_declaration','int');
$search_import_declaration=GETPOST('search_import_declaration','alpha');
$search_aduana=GETPOST('search_aduana','alpha');
$search_note_public=GETPOST('search_note_public','alpha');
$search_note_private=GETPOST('search_note_private','alpha');
$search_fk_user_creation=GETPOST('search_fk_user_creation','int');
$search_fk_user_validation=GETPOST('search_fk_user_validation','int');
$search_fk_user_close=GETPOST('search_fk_user_close','int');
$search_status=GETPOST('search_status','int');
$search_attach_files=GETPOST('search_attach_files','alpha');
$search_entity=GETPOST('search_entity','int');
$search_proveedor=GETPOST('search_proveedor','alpha');
$search_refpedido=GETPOST('search_refpedido','alpha');

$search_myfield=GETPOST('search_myfield');
$optioncss = GETPOST('optioncss','alpha');

// Load variable for pagination
$limit = GETPOST("limit")?GETPOST("limit","int"):$conf->liste_limit;
$sortfield = GETPOST('sortfield','alpha');
$sortorder = GETPOST('sortorder','alpha');
$page = GETPOST('page','int');
if ($page == -1) { $page = 0; }
$offset = $limit * $page;
$pageprev = $page - 1;
$pagenext = $page + 1;
if (! $sortfield) $sortfield="t.rowid"; // Set here default search field
if (! $sortorder) $sortorder="ASC";

// Protection if external user
$socid=0;
if ($user->societe_id > 0)
{
    $socid = $user->societe_id;
	//accessforbidden();
}

// Initialize technical object to manage hooks. Note that conf->hooks_modules contains array
$hookmanager->initHooks(array('recepcionavanzadarecepcionlist'));
$extrafields = new ExtraFields($db);

// fetch optionals attributes and labels
$extralabels = $extrafields->fetch_name_optionals_label('recepcionavanzada');
$search_array_options=$extrafields->getOptionalsFromPost($extralabels,'','search_');

// Load object if id or ref is provided as parameter
$object=new Recepcionavanzadarecepcion($db);
if (($id > 0 || ! empty($ref)) && $action != 'add')
{
	$result=$object->fetch($id,$ref);
	if ($result < 0) dol_print_error($db);
}

// Definition of fields for list
$arrayfields=array(
    
't.ref'=>array('label'=>"Ref", 'checked'=>1),
't.date_orderreception'=>array('label'=>"Fecha de recepcion.", 'checked'=>1),
't.time_orderreception'=>array('label'=>"Hora de recepcion.", 'checked'=>1),
't.fk_user_reception'=>array('label'=>"Recibe", 'checked'=>1),
't.fk_user_warehouse'=>array('label'=>"Almacen destino", 'checked'=>1),
't.import_declaration'=>array('label'=>"Pedimento", 'checked'=>1),
's.nom'=>array('label'=>"Proveedor", 'checked'=>1),
'cf.ref'=>array('label'=>"Orden de compra", 'checked'=>1),
'recibido'=>array('label'=>"Art&iacute;culos recibidos", 'checked'=>1),
't.status'=>array('label'=>"Estatus", 'checked'=>1)
/*
't.time_orderreception'=>array('label'=>$langs->trans("Fieldtime_orderreception"), 'checked'=>1),


't.exchange_rate'=>array('label'=>$langs->trans("Fieldexchange_rate"), 'checked'=>1),
't.status_import_declaration'=>array('label'=>$langs->trans("Fieldstatus_import_declaration"), 'checked'=>1),
't.import_declaration'=>array('label'=>$langs->trans("Fieldimport_declaration"), 'checked'=>1),
't.aduana'=>array('label'=>$langs->trans("Fieldaduana"), 'checked'=>1),
't.note_public'=>array('label'=>$langs->trans("Fieldnote_public"), 'checked'=>1),
't.note_private'=>array('label'=>$langs->trans("Fieldnote_private"), 'checked'=>1),
't.fk_user_creation'=>array('label'=>$langs->trans("Fieldfk_user_creation"), 'checked'=>1),
't.fk_user_validation'=>array('label'=>$langs->trans("Fieldfk_user_validation"), 'checked'=>1),
't.fk_user_close'=>array('label'=>$langs->trans("Fieldfk_user_close"), 'checked'=>1),
't.status'=>array('label'=>$langs->trans("Fieldstatus"), 'checked'=>1),
't.attach_files'=>array('label'=>$langs->trans("Fieldattach_files"), 'checked'=>1),
't.entity'=>array('label'=>$langs->trans("Fieldentity"), 'checked'=>1),


    //'t.entity'=>array('label'=>$langs->trans("Entity"), 'checked'=>1, 'enabled'=>(! empty($conf->multicompany->enabled) && empty($conf->multicompany->transverse_mode))),
    't.datec'=>array('label'=>$langs->trans("DateCreationShort"), 'checked'=>0, 'position'=>500),
    't.tms'=>array('label'=>$langs->trans("DateModificationShort"), 'checked'=>0, 'position'=>500),
    //'t.statut'=>array('label'=>$langs->trans("Status"), 'checked'=>1, 'position'=>1000),
   */
);
// Extra fields
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
{
   foreach($extrafields->attribute_label as $key => $val) 
   {
       $arrayfields["ef.".$key]=array('label'=>$extrafields->attribute_label[$key], 'checked'=>$extrafields->attribute_list[$key], 'position'=>$extrafields->attribute_pos[$key], 'enabled'=>$extrafields->attribute_perms[$key]);
   }
}




/*******************************************************************
* ACTIONS
*
* Put here all code to do according to value of "action" parameter
********************************************************************/

if (GETPOST('cancel')) { $action='list'; $massaction=''; }
if (! GETPOST('confirmmassaction')) { $massaction=''; }

$parameters=array();
$reshook=$hookmanager->executeHooks('doActions',$parameters,$object,$action);    // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

include DOL_DOCUMENT_ROOT.'/core/actions_changeselectedfields.inc.php';

if (GETPOST("button_removefilter_x") || GETPOST("button_removefilter.x") ||GETPOST("button_removefilter")) // All test are required to be compatible with all browsers
{
	
$search_fk_order_supplier='';
$search_ref='';
$search_date_orderreception='';
$search_time_orderreception='';
$search_fk_user_reception='';
$search_fk_user_warehouse='';
$search_exchange_rate='';
$search_status_import_declaration='';
$search_import_declaration='';
$search_aduana='';
$search_note_public='';
$search_note_private='';
$search_fk_user_creation='';
$search_fk_user_validation='';
$search_fk_user_close='';
$search_status='';
$search_attach_files='';
$search_entity='';
$search_proveedor='';
$search_refpedido='';
	
	$search_date_creation='';
	$search_date_update='';
	$search_array_options=array();
}


if (empty($reshook))
{
    // Mass actions. Controls on number of lines checked
    $maxformassaction=1000;
    if (! empty($massaction) && count($toselect) < 1)
    {
        $error++;
        setEventMessages($langs->trans("NoLineChecked"), null, "warnings");
    }
    if (! $error && count($toselect) > $maxformassaction)
    {
        setEventMessages($langs->trans('TooManyRecordForMassAction',$maxformassaction), null, 'errors');
        $error++;
    }
    
	// Action to delete
	if ($action == 'confirm_delete')
	{
		$result=$object->delete($user);
		if ($result > 0)
		{
			// Delete OK
			setEventMessages("RecordDeleted", null, 'mesgs');
			header("Location: ".dol_buildpath('/recepcionavanzada/list.php',1));
			exit;
		}
		else
		{
			if (! empty($object->errors)) setEventMessages(null,$object->errors,'errors');
			else setEventMessages($object->error,null,'errors');
		}
	}
}




/***************************************************
* VIEW
*
* Put here all code to build page
****************************************************/

$now=dol_now();

$form=new Form($db);

//$help_url="EN:Module_Customers_Orders|FR:Module_Commandes_Clients|ES:Módulo_Pedidos_de_clientes";
$help_url='';
$title = $langs->trans('Recepción avanzada');
llxHeader('',"Recepcion de Stock avanzado","/recepcionavanzada/recepcion.php");

// Put here content of your page

// Example : Adding jquery code
print '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	function init_myfunc()
	{
		jQuery("#myid").removeAttr(\'disabled\');
		jQuery("#myid").attr(\'disabled\',\'disabled\');
	}
	init_myfunc();
	jQuery("#mybutton").click(function() {
		init_myfunc();
	});
});
</script>';


$sql = "SELECT";
$sql.= " t.rowid,";
		$sql .= " t.ref,";
		$sql .= " t.date_orderreception,";
		$sql .= " t.time_orderreception,";
		$sql .= " t.fk_user_reception,";
		$sql .= " t.fk_user_warehouse,";
		$sql .= " t.exchange_rate,";
		$sql .= " t.status_import_declaration,";
		$sql .= " t.import_declaration,";
		$sql .= " t.date_import_declaration,";
		$sql .= " t.aduana,";
		$sql .= " t.note_public,";
		$sql .= " t.note_private,";
		$sql .= " t.fk_user_creation,";
		$sql .= " t.date_creation,";
		$sql .= " t.date_validation,";
		$sql .= " t.fk_user_validation,";
		$sql .= " t.fk_user_close,";
		$sql .= " t.date_close,";
		$sql .= " t.status,";
		$sql .= " t.attach_files,";
		$sql .= " t.entity,";
		$sql .= " s.nom,";
		$sql .= " s.rowid as provid,";
		$sql .= " cf.ref as refpedido,";
		$sql .= " cf.rowid as pedid,";
		$sql .="recibido";

// Add fields for extrafields
foreach ($extrafields->attribute_label as $key => $val) $sql.=($extrafields->attribute_type[$key] != 'separate' ? ",ef.".$key.' as options_'.$key : '');
// Add fields from hooks
$parameters=array();
$reshook=$hookmanager->executeHooks('printFieldListSelect',$parameters);    // Note that $action and $object may have been modified by hook
$sql.=$hookmanager->resPrint;
$sql.= " FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion as t";
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label)) $sql.= " LEFT JOIN ".MAIN_DB_PREFIX."recepcionavanzada_recepcion_extrafields as ef on (u.rowid = ef.fk_object)";
$sql.=", ".MAIN_DB_PREFIX."societe s, ".MAIN_DB_PREFIX."commande_fournisseur cf ";
$sql.=" LEFT JOIN (SELECT ROUND(IFNULL(sum(qty),0),2) as recibido,fk_commande,fk_order_reception  FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch GROUP BY fk_commande,fk_order_reception) as cfd ON cf.rowid=cfd.fk_commande";
$sql.= " WHERE t.entity = ".$conf->entity." ";
//$sql.= " WHERE t.entity = ".$conf->entity." AND t.rowid=cfd.fk_order_reception ";
$sql.=" AND t.fk_order_supplier=cf.rowid AND cf.fk_soc=s.rowid ";
//$sql.= " WHERE u.entity IN (".getEntity('mytable',1).")";

if ($search_ref) $sql.= natural_search("t.ref",$search_ref);
if ($search_date_orderreception) $sql.= natural_search("date_orderreception",$search_date_orderreception);
if ($search_time_orderreception) $sql.= natural_search("time_orderreception",$search_time_orderreception);
if ($search_fk_user_reception) $sql.= natural_search("fk_user_reception",$search_fk_user_reception);
if ($search_fk_user_warehouse) $sql.= natural_search("fk_user_warehouse",$search_fk_user_warehouse);
if ($search_import_declaration) $sql.= natural_search("import_declaration",$search_import_declaration);
if ($search_proveedor) $sql.= natural_search("s.nom",$search_proveedor);
if ($search_refpedido) $sql.= natural_search("cf.ref",$search_refpedido);
$sql.="GROUP BY t.ref";
/*
if ($search_time_orderreception) $sql.= natural_search("time_orderreception",$search_time_orderreception);


if ($search_exchange_rate) $sql.= natural_search("exchange_rate",$search_exchange_rate);
if ($search_status_import_declaration) $sql.= natural_search("status_import_declaration",$search_status_import_declaration);
if ($search_import_declaration) $sql.= natural_search("import_declaration",$search_import_declaration);
if ($search_aduana) $sql.= natural_search("aduana",$search_aduana);
if ($search_note_public) $sql.= natural_search("note_public",$search_note_public);
if ($search_note_private) $sql.= natural_search("note_private",$search_note_private);
if ($search_fk_user_creation) $sql.= natural_search("fk_user_creation",$search_fk_user_creation);
if ($search_fk_user_validation) $sql.= natural_search("fk_user_validation",$search_fk_user_validation);
if ($search_fk_user_close) $sql.= natural_search("fk_user_close",$search_fk_user_close);
if ($search_status) $sql.= natural_search("status",$search_status);
if ($search_attach_files) $sql.= natural_search("attach_files",$search_attach_files);
if ($search_entity) $sql.= natural_search("entity",$search_entity);*/


if ($sall)          $sql.= natural_search(array_keys($fieldstosearchall), $sall);
// Add where from extra fields
foreach ($search_array_options as $key => $val)
{
    $crit=$val;
    $tmpkey=preg_replace('/search_options_/','',$key);
    $typ=$extrafields->attribute_type[$tmpkey];
    $mode=0;
    if (in_array($typ, array('int','double'))) $mode=1;    // Search on a numeric
    if ($val && ( ($crit != '' && ! in_array($typ, array('select'))) || ! empty($crit))) 
    {
        $sql .= natural_search('ef.'.$tmpkey, $crit, $mode);
    }
}
// Add where from hooks
$parameters=array();
$reshook=$hookmanager->executeHooks('printFieldListWhere',$parameters);    // Note that $action and $object may have been modified by hook
$sql.=$hookmanager->resPrint;
$sql.=$db->order($sortfield,$sortorder);
//$sql.= $db->plimit($conf->liste_limit+1, $offset);

// Count total nb of records
$nbtotalofrecords = 0;
if (empty($conf->global->MAIN_DISABLE_FULL_SCANLIST))
{
	$result = $db->query($sql);
	$nbtotalofrecords = $db->num_rows($result);
}	

$sql.= $db->plimit($limit+1, $offset);


dol_syslog($script_file, LOG_DEBUG);
$resql=$db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    
    $params='';
    if ($limit > 0 && $limit != $conf->liste_limit) $param.='&limit='.$limit;
    
if ($search_ref != '') $params.= '&amp;search_ref='.urlencode($search_ref);
if ($search_date_orderreception != '') $params.= '&amp;search_date_orderreception='.urlencode($search_date_orderreception);
if ($search_time_orderreception != '') $params.= '&amp;search_time_orderreception='.urlencode($search_time_orderreception);
if ($search_fk_user_reception != '') $params.= '&amp;search_fk_user_reception='.urlencode($search_fk_user_reception);
if ($search_fk_user_warehouse != '') $params.= '&amp;search_fk_user_warehouse='.urlencode($search_fk_user_warehouse);
if ($search_proveedor != '') $params.= '&amp;search_proveedor='.urlencode($search_proveedor);
if ($search_refpedido != '') $params.= '&amp;search_refpedido='.urlencode($search_refpedido);
/*


if ($search_exchange_rate != '') $params.= '&amp;search_exchange_rate='.urlencode($search_exchange_rate);
if ($search_status_import_declaration != '') $params.= '&amp;search_status_import_declaration='.urlencode($search_status_import_declaration);
if ($search_import_declaration != '') $params.= '&amp;search_import_declaration='.urlencode($search_import_declaration);
if ($search_aduana != '') $params.= '&amp;search_aduana='.urlencode($search_aduana);
if ($search_note_public != '') $params.= '&amp;search_note_public='.urlencode($search_note_public);
if ($search_note_private != '') $params.= '&amp;search_note_private='.urlencode($search_note_private);
if ($search_fk_user_creation != '') $params.= '&amp;search_fk_user_creation='.urlencode($search_fk_user_creation);
if ($search_fk_user_validation != '') $params.= '&amp;search_fk_user_validation='.urlencode($search_fk_user_validation);
if ($search_fk_user_close != '') $params.= '&amp;search_fk_user_close='.urlencode($search_fk_user_close);
if ($search_status != '') $params.= '&amp;search_status='.urlencode($search_status);
if ($search_attach_files != '') $params.= '&amp;search_attach_files='.urlencode($search_attach_files);
if ($search_entity != '') $params.= '&amp;search_entity='.urlencode($search_entity);

	*/
    if ($optioncss != '') $param.='&optioncss='.$optioncss;
    // Add $param from extra fields
    foreach ($search_array_options as $key => $val)
    {
        $crit=$val;
        $tmpkey=preg_replace('/search_options_/','',$key);
        if ($val != '') $param.='&search_options_'.$tmpkey.'='.urlencode($val);
    } 

    print_barre_liste($title, $page, $_SERVER["PHP_SELF"], $params, $sortfield, $sortorder, '', $num, $nbtotalofrecords, 'title_companies', 0, '', '', $limit);


	print '<form method="POST" id="searchFormList" action="'.$_SERVER["PHP_SELF"].'">';
    if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
    print '<input type="hidden" name="action" value="list">';
	print '<input type="hidden" name="sortfield" value="'.$sortfield.'">';
	print '<input type="hidden" name="sortorder" value="'.$sortorder.'">';
	
    if ($sall)
    {
        foreach($fieldstosearchall as $key => $val) $fieldstosearchall[$key]=$langs->trans($val);
        print $langs->trans("FilterOnInto", $all) . join(', ',$fieldstosearchall);
    }
    
    $moreforfilter = '';
    /*$moreforfilter.='<div class="divsearchfield">';
    $moreforfilter.= $langs->trans('MyFilter') . ': <input type="text" name="search_myfield" value="'.dol_escpae_htmltag($search_myfield).'">';
    $moreforfilter.= '</div>';*/
    
	if (! empty($moreforfilter))
	{
		print '<div class="liste_titre liste_titre_bydiv centpercent">';
		print $moreforfilter;
    	$parameters=array();
    	$reshook=$hookmanager->executeHooks('printFieldPreListTitle',$parameters);    // Note that $action and $object may have been modified by hook
	    print $hookmanager->resPrint;
	    print '</div>';
	}

    $varpage=empty($contextpage)?$_SERVER["PHP_SELF"]:$contextpage;
    $selectedfields=$form->multiSelectArrayWithCheckbox('selectedfields', $arrayfields, $varpage);	// This also change content of $arrayfields
	
	print '<table class="liste '.($moreforfilter?"listwithfilterbefore":"").'">';

    // Fields title
    print '<tr class="liste_titre">';
    // 


if (! empty($arrayfields['t.ref']['checked'])) print_liste_field_titre("Ref.",$_SERVER['PHP_SELF'],'t.ref','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.date_orderreception']['checked'])) print_liste_field_titre("Fecha de recepcion.",$_SERVER['PHP_SELF'],'t.date_orderreception','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.time_orderreception']['checked'])) print_liste_field_titre("Hora de recepcion.",$_SERVER['PHP_SELF'],'t.time_orderreception','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.fk_user_reception']['checked'])) print_liste_field_titre("Recibe",$_SERVER['PHP_SELF'],'t.fk_user_reception','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.fk_user_warehouse']['checked'])) print_liste_field_titre("Almacen destino",$_SERVER['PHP_SELF'],'t.fk_user_warehouse','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.import_declaration']['checked'])) print_liste_field_titre("Pedimento",$_SERVER['PHP_SELF'],'t.import_declaration','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['s.nom']['checked'])) print_liste_field_titre("Proveedor",$_SERVER['PHP_SELF'],'s.nom','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['cf.ref']['checked'])) print_liste_field_titre("Orden de compra",$_SERVER['PHP_SELF'],'cf.ref','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['recibido']['checked'])) print_liste_field_titre("Art&iacute;culos recibidos",$_SERVER['PHP_SELF'],'recibido','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.status']['checked'])) print_liste_field_titre("Estatus",$_SERVER['PHP_SELF'],'t.status','',$params,'',$sortfield,$sortorder);
/*
if (! empty($arrayfields['t.date_orderreception']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_date_orderreception" value="'.$search_date_orderreception.'" size="10"></td>';


if (! empty($arrayfields['t.time_orderreception']['checked'])) print_liste_field_titre($arrayfields['t.time_orderreception']['label'],$_SERVER['PHP_SELF'],'t.time_orderreception','',$params,'',$sortfield,$sortorder);


if (! empty($arrayfields['t.exchange_rate']['checked'])) print_liste_field_titre($arrayfields['t.exchange_rate']['label'],$_SERVER['PHP_SELF'],'t.exchange_rate','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.status_import_declaration']['checked'])) print_liste_field_titre($arrayfields['t.status_import_declaration']['label'],$_SERVER['PHP_SELF'],'t.status_import_declaration','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.import_declaration']['checked'])) print_liste_field_titre($arrayfields['t.import_declaration']['label'],$_SERVER['PHP_SELF'],'t.import_declaration','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.aduana']['checked'])) print_liste_field_titre($arrayfields['t.aduana']['label'],$_SERVER['PHP_SELF'],'t.aduana','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.note_public']['checked'])) print_liste_field_titre($arrayfields['t.note_public']['label'],$_SERVER['PHP_SELF'],'t.note_public','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.note_private']['checked'])) print_liste_field_titre($arrayfields['t.note_private']['label'],$_SERVER['PHP_SELF'],'t.note_private','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.fk_user_creation']['checked'])) print_liste_field_titre($arrayfields['t.fk_user_creation']['label'],$_SERVER['PHP_SELF'],'t.fk_user_creation','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.fk_user_validation']['checked'])) print_liste_field_titre($arrayfields['t.fk_user_validation']['label'],$_SERVER['PHP_SELF'],'t.fk_user_validation','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.fk_user_close']['checked'])) print_liste_field_titre($arrayfields['t.fk_user_close']['label'],$_SERVER['PHP_SELF'],'t.fk_user_close','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.status']['checked'])) print_liste_field_titre($arrayfields['t.status']['label'],$_SERVER['PHP_SELF'],'t.status','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.attach_files']['checked'])) print_liste_field_titre($arrayfields['t.attach_files']['label'],$_SERVER['PHP_SELF'],'t.attach_files','',$params,'',$sortfield,$sortorder);
if (! empty($arrayfields['t.entity']['checked'])) print_liste_field_titre($arrayfields['t.entity']['label'],$_SERVER['PHP_SELF'],'t.entity','',$params,'',$sortfield,$sortorder);
*/

    //if (! empty($arrayfields['t.field1']['checked'])) print_liste_field_titre($arrayfields['t.field1']['label'],$_SERVER['PHP_SELF'],'t.field1','',$params,'',$sortfield,$sortorder);
    //if (! empty($arrayfields['t.field2']['checked'])) print_liste_field_titre($arrayfields['t.field2']['label'],$_SERVER['PHP_SELF'],'t.field2','',$params,'',$sortfield,$sortorder);
	// Extra fields
	if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
	{
	   foreach($extrafields->attribute_label as $key => $val) 
	   {
           if (! empty($arrayfields["ef.".$key]['checked'])) 
           {
				$align=$extrafields->getAlignFlag($key);
				print_liste_field_titre($extralabels[$key],$_SERVER["PHP_SELF"],"ef.".$key,"",$param,($align?'align="'.$align.'"':''),$sortfield,$sortorder);
           }
	   }
	}
    // Hook fields
	$parameters=array('arrayfields'=>$arrayfields);
    $reshook=$hookmanager->executeHooks('printFieldListTitle',$parameters);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
	if (! empty($arrayfields['t.datec']['checked']))  print_liste_field_titre($arrayfields['t.datec']['label'],$_SERVER["PHP_SELF"],"t.datec","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
	if (! empty($arrayfields['t.tms']['checked']))    print_liste_field_titre($arrayfields['t.tms']['label'],$_SERVER["PHP_SELF"],"t.tms","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
	//if (! empty($arrayfields['t.status']['checked'])) print_liste_field_titre($langs->trans("Status"),$_SERVER["PHP_SELF"],"t.status","",$param,'align="center"',$sortfield,$sortorder);
	print_liste_field_titre($selectedfields, $_SERVER["PHP_SELF"],"",'','','align="right"',$sortfield,$sortorder,'maxwidthsearch ');
    print '</tr>'."\n";

    // Fields title search
	print '<tr class="liste_titre">';
	// 

if (! empty($arrayfields['t.ref']['checked'])) 
	print '
	<td class="liste_titre">
		<input type="text" class="flat" name="search_ref" value="'.$search_ref.'" size="10">
	</td>';
	if (! empty($arrayfields['t.date_orderreception']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_date_orderreception" value="'.$search_date_orderreception.'" size="10"></td>';
	if (! empty($arrayfields['t.time_orderreception']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_time_orderreception" value="'.$search_time_orderreception.'" size="10"></td>';

	if (! empty($arrayfields['t.fk_user_reception']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_fk_user_reception" value="'.$search_fk_user_reception.'" size="10"></td>';

	if (! empty($arrayfields['t.fk_user_warehouse']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_fk_user_warehouse" value="'.$search_fk_user_warehouse.'" size="10"></td>';
	if (! empty($arrayfields['t.import_declaration']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_import_declaration" value="'.$search_import_declaration.'" size="10"></td>';
	if (! empty($arrayfields['s.nom']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_proveedor" value="'.$search_proveedor.'" size="10"></td>';
	if (! empty($arrayfields['cf.ref']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_refpedido" value="'.$search_refpedido.'" size="10"></td>';
	if (! empty($arrayfields['recibido']['checked'])) print '<td class="liste_titre"></td>';
	if (! empty($arrayfields['t.status']['checked'])) print '<td class="liste_titre"></td>';
/*
if (! empty($arrayfields['t.time_orderreception']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_time_orderreception" value="'.$search_time_orderreception.'" size="10"></td>';


if (! empty($arrayfields['t.exchange_rate']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_exchange_rate" value="'.$search_exchange_rate.'" size="10"></td>';
if (! empty($arrayfields['t.status_import_declaration']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_status_import_declaration" value="'.$search_status_import_declaration.'" size="10"></td>';
if (! empty($arrayfields['t.import_declaration']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_import_declaration" value="'.$search_import_declaration.'" size="10"></td>';
if (! empty($arrayfields['t.aduana']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_aduana" value="'.$search_aduana.'" size="10"></td>';
if (! empty($arrayfields['t.note_public']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_note_public" value="'.$search_note_public.'" size="10"></td>';
if (! empty($arrayfields['t.note_private']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_note_private" value="'.$search_note_private.'" size="10"></td>';
if (! empty($arrayfields['t.fk_user_creation']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_fk_user_creation" value="'.$search_fk_user_creation.'" size="10"></td>';
if (! empty($arrayfields['t.fk_user_validation']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_fk_user_validation" value="'.$search_fk_user_validation.'" size="10"></td>';
if (! empty($arrayfields['t.fk_user_close']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_fk_user_close" value="'.$search_fk_user_close.'" size="10"></td>';
if (! empty($arrayfields['t.status']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_status" value="'.$search_status.'" size="10"></td>';
if (! empty($arrayfields['t.attach_files']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_attach_files" value="'.$search_attach_files.'" size="10"></td>';
if (! empty($arrayfields['t.entity']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_entity" value="'.$search_entity.'" size="10"></td>';*/

	//if (! empty($arrayfields['t.field1']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_field1" value="'.$search_field1.'" size="10"></td>';
	//if (! empty($arrayfields['t.field2']['checked'])) print '<td class="liste_titre"><input type="text" class="flat" name="search_field2" value="'.$search_field2.'" size="10"></td>';
	// Extra fields
	if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
	{
        foreach($extrafields->attribute_label as $key => $val) 
        {
            if (! empty($arrayfields["ef.".$key]['checked']))
            {
                $align=$extrafields->getAlignFlag($key);
                $typeofextrafield=$extrafields->attribute_type[$key];
                print '<td class="liste_titre'.($align?' '.$align:'').'">';
            	if (in_array($typeofextrafield, array('varchar', 'int', 'double', 'select')))
				{
				    $crit=$val;
    				$tmpkey=preg_replace('/search_options_/','',$key);
    				$searchclass='';
    				if (in_array($typeofextrafield, array('varchar', 'select'))) $searchclass='searchstring';
    				if (in_array($typeofextrafield, array('int', 'double'))) $searchclass='searchnum';
    				print '<input class="flat'.($searchclass?' '.$searchclass:'').'" size="4" type="text" name="search_options_'.$tmpkey.'" value="'.dol_escape_htmltag($search_array_options['search_options_'.$tmpkey]).'">';
				}
                print '</td>';
            }
        }
	}
    // Fields from hook
	$parameters=array('arrayfields'=>$arrayfields);
    $reshook=$hookmanager->executeHooks('printFieldListOption',$parameters);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
    if (! empty($arrayfields['t.datec']['checked']))
    {
        // Date creation
        print '<td class="liste_titre">';
        print '</td>';
    }
    if (! empty($arrayfields['t.tms']['checked']))
    {
        // Date modification
        print '<td class="liste_titre">';
        print '</td>';
    }
    /*if (! empty($arrayfields['u.statut']['checked']))
    {
        // Status
        print '<td class="liste_titre" align="center">';
        print $form->selectarray('search_statut', array('-1'=>'','0'=>$langs->trans('Disabled'),'1'=>$langs->trans('Enabled')),$search_statut);
        print '</td>';
    }*/
    // Action column
	print '<td class="liste_titre" align="right">';
    $searchpitco=$form->showFilterAndCheckAddButtons(0);
    print $searchpitco;
    print '</td>';
	print '</tr>'."\n";
        
    
	$i=0;
	$var=true;
	$totalarray=array();
    while ($i < min($num, $limit))
    {
        $obj = $db->fetch_object($resql);
        if ($obj)
        {
            $var = !$var;
            
            // Show here line of result
            print '<tr '.$bc[$var].'>';
            // LIST_OF_TD_FIELDS_LIST
            /*
            if (! empty($arrayfields['t.field1']['checked'])) 
            {
                print '<td>'.$obj->field1.'</td>';
    		    if (! $i) $totalarray['nbfield']++;
            }
            if (! empty($arrayfields['t.field2']['checked'])) 
            {
                print '<td>'.$obj->field2.'</td>';
    		    if (! $i) $totalarray['nbfield']++;
            }*/
        	// Extra fields
    		if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
    		{
    		   foreach($extrafields->attribute_label as $key => $val) 
    		   {
    				if (! empty($arrayfields["ef.".$key]['checked'])) 
    				{
    					print '<td';
    					$align=$extrafields->getAlignFlag($key);
    					if ($align) print ' align="'.$align.'"';
    					print '>';
    					$tmpkey='options_'.$key;
    					print $extrafields->showOutputField($key, $obj->$tmpkey, '', 1);
    					print '</td>';
    		            if (! $i) $totalarray['nbfield']++;
    				}
    		   }
    		}
            // Fields from hook
    	    $parameters=array('arrayfields'=>$arrayfields, 'obj'=>$obj);
    		$reshook=$hookmanager->executeHooks('printFieldListValue',$parameters);    // Note that $action and $object may have been modified by hook
            print $hookmanager->resPrint;
        	// Date creation
            if (! empty($arrayfields['t.datec']['checked']))
            {
                print '<td align="center">';
                print dol_print_date($db->jdate($obj->date_creation), 'dayhour');
                print '</td>';
    		    if (! $i) $totalarray['nbfield']++;
            }
            // Date modification
            if (! empty($arrayfields['t.tms']['checked']))
            {
                print '<td align="center">';
                print dol_print_date($db->jdate($obj->date_update), 'dayhour');
                print '</td>';
    		    if (! $i) $totalarray['nbfield']++;
            }
            // Status
            /*
            if (! empty($arrayfields['u.statut']['checked']))
            {
    		  $userstatic->statut=$obj->statut;
              print '<td align="center">'.$userstatic->getLibStatut(3).'</td>';
            }*/

            // Action column
            if (! empty($arrayfields['t.ref']['checked']))
            {
            	$rec=new Recepcionavanzadarecepcion($db);
            	$rec->fetch($obj->rowid);
            	print '<td>'.$rec->getNomUrl(1).'</td>';
        	}
        	if (! empty($arrayfields['t.date_orderreception']['checked']))
            {
            	print '<td>'.dol_print_date($db->jdate($obj->date_orderreception), 'day').'</td>';
            }
            
            if (! empty($arrayfields['t.time_orderreception']['checked']))
            {
            	print '<td>'.$obj->time_orderreception.'</td>';
        	}

        	if (! empty($arrayfields['t.fk_user_reception']['checked']))
            {
	            $user_new=new User($db);
	            $user_new->fetch($obj->fk_user_reception);
	            print '<td>'.$user_new->getNomUrl(1).'</td>';
	        }

	        if (! empty($arrayfields['t.fk_user_reception']['checked']))
            {
				require_once DOL_DOCUMENT_ROOT.'/product/stock/class/entrepot.class.php';
	            $almacen = new Entrepot($db);
	            $almacen->fetch($obj->fk_user_warehouse);
	            print '<td>'.$almacen->getNomUrl(1).'</td>';
            }
            
            if (! empty($arrayfields['t.import_declaration']['checked']))
            {
            	print '<td>'.$obj->import_declaration.'</td>';
            }
            if (! empty($arrayfields['s.nom']['checked']))
            {
            	$prov=new Societe($db);
            	$prov->fetch($obj->provid);
            	print '<td>'.$prov->getNomUrl(1).'</td>';
            }
            if (! empty($arrayfields['cf.ref']['checked']))
            {
            	require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.class.php';
            	$order=new CommandeFournisseur($db);
            	$order->fetch($obj->pedid);
            	print '<td>'.$order->getNomUrl(1).'</td>';
            }
            if (! empty($arrayfields['recibido']['checked']))
            {
            	print '<td>'.$obj->recibido.'</td>';
            }
            if (! empty($arrayfields['t.status']['checked']))
            {
            	if($obj->status==0){
            		$a='<span class="hideonsmartphone">'."Borrador".' </span>'.img_picto($langs->trans('StatusOrderDraftShort'),'statut0');
            	}
            	if($obj->status==1){
            		$a='<span class="hideonsmartphone">'."Validado".' </span>'.img_picto($langs->trans('StatusOrderValidatedShort'),'statut1');
            	}
            	if($obj->status==2){
            		$a='<span class="hideonsmartphone">'."Cerrado".' </span>'.img_picto($langs->trans('Cerrado'),'statut4');
            	}
            	print '<td>'.$a.'</td>';
            }

            

            print '<td></td>';

            if (! $i) $totalarray['nbfield']++;

            print '</tr>';
        }
        $i++;
    }
    
    $db->free($resql);

	$parameters=array('sql' => $sql);
	$reshook=$hookmanager->executeHooks('printFieldListFooter',$parameters);    // Note that $action and $object may have been modified by hook
	print $hookmanager->resPrint;

	print "</table>\n";
	print "</form>\n";
	
	$db->free($result);
}
else
{
    $error++;
    dol_print_error($db);
}


// End of page
llxFooter();
$db->close();

