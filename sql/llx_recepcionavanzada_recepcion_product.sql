CREATE TABLE IF NOT EXISTS `llx_recepcionavanzada_recepcion_product` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `fk_recepcion` int(11) NOT NULL,
  `fk_product` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`rowid`)
);

