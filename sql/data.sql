ALTER TABLE `llx_commande_fournisseur_dispatch` ADD COLUMN `fk_order_reception` int(11);
ALTER TABLE `llx_stock_mouvement` ADD COLUMN `fk_order_reception` int(11) null;
ALTER TABLE `llx_recepcionavanzada_recepcion` ADD COLUMN `fk_user_mod` int(11) null;
ALTER TABLE `llx_recepcionavanzada_recepcion` ADD COLUMN `date_mod` date null;