<?php 
require '../main.inc.php';
date_default_timezone_set('America/Mexico_City');

if(isset($_POST['funcion'])){
		$funcion= addslashes($_POST['funcion']);
		switch ($funcion) {
			
			case 'validarRecepcion':			
				validarRecepcion($db);
				break;
			default:
				echo "Parametros incorrectos";
				break;
		}

	}

function validarRecepcion($db){
		
		$series=$_POST['series'];
		$productos=$_POST['productosBatch'];
		$productosAll=$_POST['productosAll'];
		$cantidades=$_POST['cantidades'];
		$ventas=$_POST['venta'];
		$caducidad=$_POST['caducidad'];
		$idP=$_POST['idp'];
		$idRec=$_POST['idrec'];
		$almacenes=$_POST['almacenes'];
		$comment=$_POST['comment'];
		$user=$_POST['user'];

		if(is_null($cantidades)){
			echo "Introduzca una cantidad mayor a cero (0)";
			$err=1;
	    	exit();
		}

		// var_dump($series);
		// var_dump($productos);
		// var_dump($productosAll);
		// var_dump($cantidades);
		// var_dump($ventas);
		// var_dump($caducidad);
		// var_dump($idP);
		// var_dump($idRec);
		// var_dump($almacenes);
		// var_dump($comment);
		// var_dump($user);

		$sqlconf = "select bandConfNumSerie as band from  ".MAIN_DB_PREFIX."recepcionavanzada_conf "; 
		$resconf=$db->query($sqlconf);
		if (! $resconf){
			dol_print_error($db);	
		}else{
			$band = $db->fetch_object($resconf);
		} 
		
		$err=0;		

		if($band->band==1){
			for ($i=0; $i<sizeof($productos); $i++){	 
    		   	if($cantidades[$i]>1 || $cantidades[$i]<0){
    		   		$err=1;
    		   		echo "ERROR: Número de lote/Serie debe ser único";
    		   		exit();
    		   	}
    		}
			if(sizeof($series)>0){
				$res = array_diff($series, array_diff(array_unique($series), array_diff_assoc($series, array_unique($series))));
				
				foreach(array_unique($res) as $v) {
					$num= implode( array_keys($res, $v)) ;
					$ant= $productos[$num[0]];
					for ($i=1; $i<=sizeof($num); $i++){
						if($cantidades[$i-1]>1 || $cantidades[$i-1]<0){
							$err=1;
							echo "ERROR: Número de lote/Serie debe ser único";
							exit();
						}
						if($ant==$productos[$num[$i]]){
							$err=1;
							echo "ERROR: Número de lote/Serie repetidas en el formulario";
							exit();
						}else{
							$ant= $productos[$num[$i]];
						}
				
					}
					 
				}
			}
			

			if($err==0){

				for ($i=0; $i<sizeof($series); $i++){			
					$string='SELECT
							COUNT(*) as cant
						FROM
							'.MAIN_DB_PREFIX.'stock_mouvement
						WHERE
							fk_product='.$productos[$i].'
						and batch="'.$series[$i].'" AND rowid NOT IN (
							SELECT fk_stock_mouvement
							FROM '.MAIN_DB_PREFIX.'recepcion_avanzada_devolucion
							) AND value>0';
					$res=$db->query($string);
					$data = $db->fetch_object($res);

					if($data->cant>0){
						echo "ERROR: Número de lote/Serie ya existe en el stock";
						$err=1;
	    				exit();
					}
				}				
			}
			
		}else{

			$ban=0;
			for ($i=0; $i<sizeof($ventas); $i++){			
				$string='SELECT
						rowid, eatby, sellby, fk_product, batch, value
					FROM
						'.MAIN_DB_PREFIX.'stock_mouvement
					WHERE
						fk_product='.$productos[$i].'
					and batch="'.$series[$i].'";';

					
				$res=$db->query($string);
				$num=$db->num_rows($res);
				$data = $db->fetch_object($res);

				if($num>0){	
				//echo 	$data->value;			
					if($data->value>0){
						$string2='SELECT
								fk_stock_mouvement
							FROM
								'.MAIN_DB_PREFIX.'recepcion_avanzada_devolucion
							WHERE
								fk_stock_mouvement='.$data->rowid;
							
						$res2=$db->query($string2);
						$num2=$db->num_rows($res2);
						if($num2==0){	
							for ($i=0; $i<sizeof($ventas); $i++){			
								if($productos[$i]==$data->fk_product && $series[$i]==$data->batch && $ventas[$i]==$data->sellby && $caducidad[$i]==$data->eatby){

								}else{
									echo "ERROR: El lote/serie ya existe con una fecha de caducidad diferente";
									exit();
									$ban=1;
									$err=1;
								}
							}
						}
						
					}
					
    				
				}
			}	

			if($ban==0){
				if(sizeof($series)>0){
					$res = array_diff($series, array_diff(array_unique($series), array_diff_assoc($series, array_unique($series))));
	 
					foreach(array_unique($res) as $v) {
			    		$num= implode( array_keys($res, $v)) ;     		
			    		$ant= $productos[$num[0]];  
			    		$eat= $caducidad[$num[0]];
			    		$sell=$ventas[$num[0]];
			    		for ($i=1; $i<=sizeof($num); $i++){	    			
			    			if($ant==$productos[$num[$i]] && $eat!=$caducidad[$num[$i]] && $sell!=$ventas[$num[$i]]){ 		
			    				echo "ERROR: El lote/serie ya existe en el formulario con una fecha de caducidad diferente (".$eat."-".$sell.")(".$caducidad[$num[$i]]."-".$ventas[$num[$i]].")";
			    				 exit();
			    				 $err=1;
			    			}else{   		    			
			    				$ant= $productos[$num[$i]]; 
			    				$eat= $caducidad[$num[$i]];
			    				$sell=$ventas[$num[$i]];
			    			}
	
			    		}
			    		  
					}
				}
			}
			
		}

		$sqlxx = "select bandConfOrdCompra as band  from  ".MAIN_DB_PREFIX."recepcionavanzada_conf "; 
		$resqlx=$db->query($sqlxx);	
	
		if (! $resqlx){
			dol_print_error($db);	
		}else{
			$bandx = $db->fetch_object($resqlx);
			
			if($bandx->band==0 || is_null($bandx->band)){

				$ant=$productosAll[0];
				$sum=$cantidades[0];				
				
				for ($i=1; $i<=sizeof($productosAll); $i++){						
				
					if($ant==$productosAll[$i]){
						$sum=$sum+$cantidades[$i];						
					}else{

						$s='select  SUM(qty) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$idP.'  and fk_product='.$ant;
						$r=$db->query($s);
						$d=$db->fetch_object($r);

						$new=$sum+$d->cant;										

						$string='SELECT qty from '.MAIN_DB_PREFIX.'commande_fournisseurdet where fk_product='.$ant.' and fk_commande='.$idP.' ;';	
						
						$quer=$db->query($string);
						$res=$db->fetch_object($quer);	
						$solicitada=$res->qty;

					//	if($solicitada<$sum){
						if($solicitada<$new){
							echo "La cantidad de productos introducidos es mayor a la solicitada";
							exit();
							$err=1;
						}else{
							$sum=$cantidades[$i];
						}
					}

					$ant=$productosAll[$i];
					
				}	
			}
		}


		if($err==0){
			$string='delete from '.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch where	fk_commande='.$idP.' and fk_order_reception='.$idRec.';';

			$res=$db->query($string);

			
			for ($i=0; $i<sizeof($productosAll); $i++){				

				$complement='';
				$values='';

				
					$string='INSERT INTO '.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch (
						fk_commande,
						fk_product,
						fk_commandefourndet,
						qty,
						fk_entrepot,
						fk_user,
						COMMENT,
						batch,						
						STATUS,
						datec,						
						fk_order_reception	';



					$eat2=null;				
					if(strlen( $caducidad[$i])>0 ){					
						$eat=$caducidad[$i];
						$aux=str_replace('/','-',$eat);
						$eat2=date('Y-m-d',strtotime($aux));

						$complement.=' ,eatby ';
						$values.=' ,"'.$eat2.'"  ';
					}

					$sell2=null;				
					if(strlen( $ventas[$i])>0 ){					
						$sell=$ventas[$i];
						$aux=str_replace('/','-',$sell);
						$sell2=date('Y-m-d',strtotime($aux));
						$complement.=' ,sellby ';
						$values.=' ,"'.$sell2.'"  ';
					}
				

					$ser='';
					if(!is_null($series[$i])){
						$ser=$series[$i];
					}		

					$cant='';
					if(!is_null($cantidades[$i])){
						$cant=$cantidades[$i];
					}

					$alm='';
					if(!is_null($almacenes[$i])){
						$alm=$almacenes[$i];
					}

					$id='';
					if(!is_null($productosAll[$i])){
						$id=$productosAll[$i];
					}

					$string.=$complement.' )
							values('.$idP.','.$id.',1,'.$cant.','.$alm.','.$user.',"'.$comment.'","'.$ser.'",1,now(),'.$idRec.$values.' );';
					
					$res=$db->query($string);	
				

								
				
			}

			
		}	

}

?>
