
CREATE TABLE IF NOT EXISTS `llx_status` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `statut` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rowid`)
);

INSERT INTO `llx_status` VALUES ('1', '0', 'Borrador', 'Borrador');
INSERT INTO `llx_status` VALUES ('2', '1', 'Validado', null);
INSERT INTO `llx_status` VALUES ('3', '2', 'Cerrada', 'Aprobado');
INSERT INTO `llx_status` VALUES ('8', '3', null, 'Pedido');
INSERT INTO `llx_status` VALUES ('9', '4', null, 'Recibido Parcialmente');
INSERT INTO `llx_status` VALUES ('10', '5', null, 'Recibido');
INSERT INTO `llx_status` VALUES ('11', '7', null, 'Cancelado');