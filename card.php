<?php
//require '../main.inc.php';
//require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.class.php';

//llxHeader('',"Recepcion de Stock avanzado","");
global $user;
$action=GETPOST('action');
$idrec=GETPOST('idrec');
$id=GETPOST('id');
date_default_timezone_set('America/Mexico_City');

print "<script type='text/javascript'>		
			
			$(document).ready(function() {
			  
			    $(document).on('click','#validateProd',function() {


			       	var serieBatch = [];
			       	var productosBatch = [];
			       	var dateCaducidad=[];
			       	var dateVenta=[];
			       	var cantidades=[];
			       	var productosAll=[];
					var band=0;
					var almacenes=[];
					var cantidadesAux=[];
					var almacenesAux=[];
					var i=0;
					var c=0;

					i=0;
					$('.sufixCant').each(function () {	
							
						var cad=$(this).val();																					
			       		cantidadesAux[i] = $('#qty'+cad).val() ;	
			       		//alert(cad+' '+cantidadesAux[i]+' '+i)												
			       		almacenesAux[i]=$('#entrepot'+cad).val() ;																       		
						i++;
					});



					i=0;
					c=0;
					$('.serieBatch').each(function () {	
							
						var cad=$(this).val();						
						if(parseInt(cantidadesAux[c])>0){	
							if(cad.length>0){
								serieBatch[i] = cad.trim();		
							 }else{
							 	
						 		alert('".$langs->trans('msgBatchNull')."');											
						 		band=1;
							 	return false;		 	
							 					
							 }						 						 		
						 	
						 	i++;
					 	}
					 	if(cad.length>0 && (parseInt(cantidadesAux[c])<=0 || cantidadesAux[c]==null )){
					 		alert('".$langs->trans('msgCantNull')."');		
					 		band=1;
							 return false;											
					 	}
					 	c++;

														
						
					});


					i=0;
					c=0;
					$('.sufixEat').each(function () {	
							
						var cad=$(this).val();
						if(parseInt(cantidadesAux[c])>0){						 						 		
							dateCaducidad[i]=$('#dlc'+cad).val();
			       			dateVenta[i]=$('#dluo'+cad).val() ;	
			       			i++;
						}																					       								
						c++;
					});

					i=0;
					c=0;
					$('.producID').each(function () {
						
						if(parseInt($(this).val())>0){
							if(parseInt(cantidadesAux[c])>0){						 						 		
								productosBatch[i] = $(this).val();	
								i++;
							}							
						}else{													
							band=1;
							return false;					
						}			
						
						c++;
					});

					i=0;
					c=0;
					$('.products').each(function () {
						
						if(c!=0){							
							if(parseInt(cantidadesAux[c-1])>0){									
								productosAll[i] = $(this).val();															
								i++;
							}
						}				
																										
						c++;
					});
					

					i=0;
					c=0;
					$('.dateSell').each(function () {
						if(parseInt(cantidadesAux[i])>0){
							dateVenta[i] = $(this).val();		
							i++;
						}																															
						c++;
					});

					c=0;
					for (i=0; i<=cantidadesAux.length ; i++) { 
						if(parseInt(cantidadesAux[i])>0){
							cantidades[c] = cantidadesAux[i];		
							almacenes[c] = almacenesAux[i];		
							c++;
						}																		
					}


					if(band==0){						

						datos={funcion:'validarRecepcion', series:serieBatch, productosBatch:productosBatch, almacenes:almacenes, productosAll:productosAll, cantidades:cantidades, caducidad:dateCaducidad, venta:dateVenta, idp:$('#idPedido').val(), idrec:$('#ordenRec').val(), comment:$('#comment').val(), user:$('#user').val()};

						$.ajax({
							async:true,
							type: 'POST',
							dataType: 'html',
							contentType: 'application/x-www-form-urlencoded',
							url:'validaciones.php',
							data:datos,
							success:function (data){
								
								var cad=data;
								if(cad.length>0){
									alert(data);
								}else{									
									window.location = '".DOL_URL_ROOT."/recepcionavanzada/recepcion.php?id=".$id."&act=card&type=1&idrec=".$idrec."&action=validates';
								}								
							},
							error:function (data){
								alert('error '+data);
							}
						});	
						
						return false;
					}else{
						return false;
					}	

			    });	    

			   
			});
		        
		     			
		</script>";


if($action=='deleteOrd'){
	$string='DELETE from '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion where fk_order_supplier='.$id.' and entity='.$conf->entity . ' and rowid='.$idrec;
	$res=$db->query($string);

	print "<script type='text/javascript'>				
			window.location = '".DOL_URL_ROOT."/recepcionavanzada/recepcion.php?id=".$id."';
		</script>";
}

if($action=='closeRec'){
	$fk_user_close=$user->id;
	$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion set status=2, date_close=now(), fk_user_close='.$fk_user_close.' where fk_order_supplier='.$id.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);
$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$id.' and entity='.$conf->entity . ' and rowid='.$idrec;
$res=$db->query($string);
}

if($action=='reabrir'){	
	$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion set status=1  where fk_order_supplier='.$id.' and entity='.$conf->entity. ' and rowid='.$idrec;
		$res=$db->query($string);
$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$id.' and entity='.$conf->entity . ' and rowid='.$idrec;
$res=$db->query($string);
}

if($action=='close'){
	//print_r($_POST);
	if(GETPOST('yes')){
		$fk_user_close=$user->id;
		$sqnn2="SELECT count(ref) as ext FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion 
				WHERE rowid=".$idrec." AND ref LIKE '(PROV%'";
		$rnn2=$db->query($sqnn2);
		$rfd2=$db->fetch_object($rnn2);
		if($rfd2->ext=='1'){
			$ref=getNextNumRef();
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion set status=1, date_validation=now(), fk_user_validation='.$fk_user_close.' ,ref="'.$ref.'" where fk_order_supplier='.$id.' and entity='.$conf->entity. ' and rowid='.$idrec;
		}else{
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion set status=1, date_validation=now(), fk_user_validation='.$fk_user_close.'  where fk_order_supplier='.$id.' and entity='.$conf->entity. ' and rowid='.$idrec;
		}
		$res=$db->query($string);

		// $string=' select b.fk_soc, c.subprice as unitprice, a.*
		// 			FROM
		// 				'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
		// 			INNER JOIN '.MAIN_DB_PREFIX.'commande_fournisseur as b on a.fk_commande=b.rowid
		// 			INNER JOIN '.MAIN_DB_PREFIX.'commande_fournisseurdet as c on b.rowid=c.fk_commande
		// 			 where a.fk_commande='.$id.' and a.fk_order_reception='.$idrec;

		// $string=' select b.fk_soc,  a.*	FROM
		// 			'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
		// 			INNER JOIN '.MAIN_DB_PREFIX.'commande_fournisseur as b on a.fk_commande=b.rowid					
		// 			 where a.fk_commande='.$id.' and a.fk_order_reception='.$idrec;

			// $sql_prds = "
			// SELECT
			// a.fk_product,
			// a.total_ht,
			// a.qty,
			// b.multicurrency_code,
			// CASE
			// 	WHEN b.multicurrency_code = 'MXN' THEN a.total_ht
			// 	ELSE a.total_ht * (SELECT
			// 	exchange_rate
			// 	FROM llx_recepcionavanzada_recepcion
			// 	WHERE rowid = ".$idrec.")
			// END AS cambio
			// FROM llx_facture_fourn_det a,
			// 	llx_facture_fourn b
			// WHERE a.fk_facture_fourn IN (SELECT
			// fk_facture_fourn
			// FROM llx_recepcionavanzada_costo
			// WHERE categoria NOT IN (SELECT
			// rowid
			// FROM llx_c_recepcionavanzada_categoria_gasto
			// WHERE take_into_account != 1)
			// AND fk_reception = ".$idrec.")
			// AND a.fk_product NOT IN (SELECT
			// fk_prod
			// FROM llx_recepcionavanzada_rubros
			// WHERE active = 1)
			// AND a.fk_facture_fourn = b.rowid
			// ";
			// $res_prds = $db->query($sql_prds);
			// while ($dat = $db->fetch_object($res_prds)) {

			// }
		//Comprabar como se calculara el costo/precio de entrada (Forma principal o extra)
		$sqlCst_Prc = "SELECT bandConfgCalcCst_Prc FROM llx_recepcionavanzada_conf";
		$rqCst_Prc = $db->query($sqlCst_Prc);
		$rfCst_Prc = $db->fetch_object($rqCst_Prc);
		if($rfCst_Prc->bandConfgCalcCst_Prc == 1){
			$sql_FacAdu = "
			SELECT (SELECT
					exchange_rate
				FROM llx_recepcionavanzada_recepcion
				WHERE rowid = ".$idrec."
				AND fk_order_supplier = ".$id.")
				AS rate,
				SUM((SELECT
					CASE
					WHEN rate != 1 THEN a.total_ht * rate
					ELSE a.total_ht
					END)
				) AS FacAdu
			FROM llx_facture_fourn_det a,
				llx_facture_fourn b
			WHERE a.fk_facture_fourn IN (SELECT
			fk_facture_fourn
			FROM llx_recepcionavanzada_costo
			WHERE categoria NOT IN (SELECT
			rowid
			FROM llx_c_recepcionavanzada_categoria_gasto
			WHERE take_into_account != 1)
			AND fk_reception = ".$idrec."
			AND type_fac = 1)
			AND a.fk_product NOT IN (SELECT
			fk_prod
			FROM llx_recepcionavanzada_rubros
			WHERE active = 1)
			AND a.fk_facture_fourn = b.rowid
			";
			$res_FacAdu = $db->query($sql_FacAdu);
			$FacAdu = $db->fetch_object($res_FacAdu);

			$sql_GasPed = "
			SELECT (SELECT
					exchange_rate
				FROM llx_recepcionavanzada_recepcion
				WHERE rowid = ".$idrec."
				AND fk_order_supplier = ".$id.")
				AS rate,
				SUM((SELECT
					CASE
					WHEN rate != 1 THEN a.total_ht * rate
					ELSE a.total_ht
					END)
				) AS GasPed
			FROM llx_facture_fourn_det a,
				llx_facture_fourn b
			WHERE a.fk_facture_fourn IN (SELECT
			fk_facture_fourn
			FROM llx_recepcionavanzada_costo
			WHERE categoria NOT IN (SELECT
			rowid
			FROM llx_c_recepcionavanzada_categoria_gasto
			WHERE take_into_account != 1)
			AND fk_reception = ".$idrec."
			AND type_fac = 2)
			AND a.fk_product NOT IN (SELECT
			fk_prod
			FROM llx_recepcionavanzada_rubros
			WHERE active = 1)
			AND a.fk_facture_fourn = b.rowid
			";
			$res_GasPed = $db->query($sql_GasPed);
			$GasPed = $db->fetch_object($res_GasPed);

			$sql_Tot = "
			SELECT (SELECT
					exchange_rate
				FROM llx_recepcionavanzada_recepcion
				WHERE rowid = ".$idrec."
				AND fk_order_supplier = ".$id.")
				AS rate,
				SUM((SELECT
					CASE
					WHEN rate != 1 THEN a.total_ht * rate
					ELSE a.total_ht
					END)
				) AS Tot
			FROM llx_commande_fournisseurdet a
			WHERE fk_commande = ".$id."
			";
			$res_Tot = $db->query($sql_Tot);
			$Tot = $db->fetch_object($res_Tot);
		}

		
		$string='select * from '.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch  where fk_commande='.$id.' and fk_order_reception='.$idrec;
		$res=$db->query($string);
		while ($dat=$db->fetch_object($res)) {

			$string2='SELECT
						subprice
					FROM
						'.MAIN_DB_PREFIX.'commande_fournisseurdet
					WHERE
						fk_commande='.$id.'
					AND fk_product ='.$dat->fk_product;
		
			$res2=$db->query($string2);
			$dat2=$db->fetch_object($res2);

			$complement='';
			$valuesCom='';
			$complementUpdate='';

			
			if(strlen($dat->eatby)>0 ){					
				$eat=$dat->eatby;
				$aux=str_replace('/','-',$eat);
				$eat2=date('Y-m-d',strtotime($aux));

				$complement.=' ,eatby ';
				$valuesCom.=' ,"'.$eat2.' "  ';
				$complementUpdate.=' ,eatby= "'.$dat->eatby.'"';
			}

			
			if(strlen( $dat->sellby)>0 ){					
				$sell=$dat->sellby;
				$aux=str_replace('/','-',$sell);
				$sell2=date('Y-m-d',strtotime($aux));
				$complement.=' ,sellby ';
				$valuesCom.=' ,"'.$sell2.'"  ';
				$complementUpdate.=' ,sellby= "'.$dat->sellby.'"';
			}			

			
			$string='INSERT INTO '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch (
						fk_commande,fk_product,
						fk_commandefourndet,qty,
						fk_entrepot,fk_user,
						comment,batch,
						status,datec,
						tms,fk_order_reception '.$complement.'					 
					)
					values('.$dat->fk_commande.','.$dat->fk_product.','.$dat->fk_commandefourndet.','.$dat->qty.','.$dat->fk_entrepot.','.$dat->fk_user.',"'.$dat->comment.'","'.$dat->batch.'",'.$dat->status.',"'.$dat->datec.'",now(),"'.$dat->fk_order_reception.'"'.$valuesCom.');';

			//	echo $string;

			$resp=$db->query($string);	
			
			$product = new Product($db);
			$id_product=$dat->fk_product;
			
			$id_tw=$dat->fk_entrepot;
			$qty=$dat->qty;
			$batch=$dat->batch;
			$stlabel=$dat->comment;       

			$result=$product->fetch($id_product);
		
			$product->load_stock();    // Load array product->stock_warehouse
		
			// Define value of products moved   
			$sqlCst_Prc = "SELECT bandConfgCalcCst_Prc FROM llx_recepcionavanzada_conf";
			$rqCst_Prc = $db->query($sqlCst_Prc);
			$rfCst_Prc = $db->fetch_object($rqCst_Prc);
			if($rfCst_Prc->bandConfgCalcCst_Prc == 1){
				$porcentaje = $dat2->subprice / (is_null($Tot->Tot) ? 0 : $Tot->Tot);
				$facaducalc = $porcentaje / $qty * (is_null($FacAdu->FacAdu) ? 0 : $FacAdu->FacAdu);
				$gaspedcalc = $porcentaje / $qty * (is_null($GasPed->GasPed) ? 0 : $GasPed->GasPed);
				$pricedest = $dat2->subprice + $facaducalc + $gaspedcalc;
			} else {
				// $pricedest = ($product->pmp>0) ?  $product->pmp: $dat->unitprice ;
				//$pricedest = $dat2->subprice ;
				$sqmv="SELECT exchange_rate FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
					WHERE rowid=".$idrec." AND fk_order_supplier=".$id;
				$mreq=$db->query($sqmv);
				$mres=$db->fetch_object($mreq);
				if($mres->exchange_rate!=1){
					$pricedest = $dat2->subprice * $mres->exchange_rate;
				}else{
					$pricedest = $dat2->subprice ;	        
				}
			}
	        
			if (empty($conf->productbatch->enabled) || ! $product->hasbatch())        // If product does not need lot/serial
			{
				// Add stock
				$result2=$product->correct_stock(
						$user,
						$id_tw,
						$qty,
						0,
						$stlabel,
						$pricedest
						
				);
			}
			else
			{                                
			// $dlc=$dat->eatby;
				//$dluo=$dat->sellby;

				$dlc=$eat2;
				$dluo=$sell2;

				// Add stock
				$result2=$product->correct_stock_batch(
						$user,
						$id_tw,
						$qty,
						0,
						$stlabel,
						$pricedest,
						null,
						null,
						$batch
				);                

			

				if(strlen( $complementUpdate)>0 ){	
					$stringStock='SELECT MAX(rowid) as id FROM '.MAIN_DB_PREFIX.'product_lot where fk_product='.$id_product.';';
					$resStock=$db->query($stringStock);
					$datStock=$db->fetch_object($resStock);

					$stringupd='UPDATE '.MAIN_DB_PREFIX.'product_lot set entity=1 '.$complementUpdate.'  where rowid='.$datStock->id.';';
					$resupd=$db->query($stringupd);	

					$stringStock='SELECT MAX(rowid) as id FROM '.MAIN_DB_PREFIX.'stock_mouvement where fk_product='.$id_product.';';
					$resStock=$db->query($stringStock);
					$datStock=$db->fetch_object($resStock);

					$stringupd='UPDATE '.MAIN_DB_PREFIX.'stock_mouvement set origintype="order_supplier" '.$complementUpdate.'  where rowid='.$datStock->id.';';
					$resupd=$db->query($stringupd);	
					//echo $stringupd;
					//die();
				}
				

			}   

			$stringStock='SELECT MAX(rowid) as id FROM '.MAIN_DB_PREFIX.'stock_mouvement where fk_product='.$id_product.';';
			$resStock=$db->query($stringStock);
			$datStock=$db->fetch_object($resStock);

		//	$stringupd='UPDATE '.MAIN_DB_PREFIX.'stock_mouvement set type_mouvement=3 , fk_origin='.$id.', eatby="'.$eat2.'", sellby="'.$sell2.'" ,origintype="order_supplier", fk_order_reception='.$idrec.' where rowid='.$datStock->id.';';
			$stringupd='UPDATE '.MAIN_DB_PREFIX.'stock_mouvement set type_mouvement=3 , fk_origin='.$id.',origintype="order_supplier", fk_order_reception='.$idrec.' where rowid='.$datStock->id.';';
			$resupd=$db->query($stringupd);			
		}

		$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$id.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);

		
		print '<script>location.href="PDFOrdRec.php?id='.$id.'&idrec='.$idrec.'";</script>';
	}else{
		$sqm="SELECT count(rowid) as cantd FROM ".MAIN_DB_PREFIX."recepcionavanzada_costo WHERE fk_reception=".$idrec;
		$rqm=$db->query($sqm);
		$rqs=$db->fetch_object($rqm);
		$sqb="SELECT bandConfgCostos FROM ".MAIN_DB_PREFIX."recepcionavanzada_conf WHERE entity=".$conf->entity;
		$rbs=$db->query($sqb);
		$rbb=$db->fetch_object($rbs);
		if($rqs->cantd>0 || $rbb->bandConfgCostos==0){
	//	recepcion.php?id=21&act=card&type=1&idrec=28
			print "<script type='text/javascript'>				
				window.location = '".DOL_URL_ROOT."/recepcionavanzada/recepcion.php?id=".$id."&act=card&type=1&idrec=".$idrec."';		
			</script>";
		}else{
			print "<script type='text/javascript'>
				window.location = '".DOL_URL_ROOT."/recepcionavanzada/recepcion.php?id=".$id."&act=card&type=1&coste=".$idrec."';
			</script>";
		}
	}
}

if($action=='validates'){	
	$sqm="SELECT count(rowid) as cantd FROM ".MAIN_DB_PREFIX."recepcionavanzada_costo WHERE fk_reception=".$idrec;
	$rqm=$db->query($sqm);
	$rqs=$db->fetch_object($rqm);
	$sqb="SELECT bandConfgCostos FROM ".MAIN_DB_PREFIX."recepcionavanzada_conf WHERE entity=".$conf->entity;
	$rbs=$db->query($sqb);
	$rbb=$db->fetch_object($rbs);
	if($rqs->cantd>0 || $rbb->bandConfgCostos==0){
		$formquestion = array(
                            // 'text' => $langs->trans("ConfirmClone"),
                             array('type' => 'checkbox', 'name' => 'yes', 'id'=>'yes', 'label' =>'S&iacute; deseo Guardar y afectar stock', 'value' =>
                             0 ),
                             array('type' => 'checkbox', 'name' => 'notyes', 'id'=>'notyes', 'label' =>'No, seguir recibiendo productos m&aacute;s adelante', 'value'
                             => 1)
                            );
		print "<script type='text/javascript'>
		
		$(document).ready(function() {
		
		    $(document).on('click','#yes',function() {
		        $('#yes').attr('checked', true);
				$('#notyes').attr('checked', false);
		    });
		
		    $(document).on('click','#notyes',function() {
		        $('#yes').attr('checked', false);
				$('#notyes').attr('checked', true);
		    });
		});
	    
	</script>";
		
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id='.$id.'&act=card&type=1&idrec='.$idrec, ''.$langs->trans('msgCloseReceptionStock').'', '', 'close', $formquestion, 0, 1);
		print $formconfirm;
	}else{
		$formquestion = array(
				// 'text' => $langs->trans("ConfirmClone"),
				array('type' => 'checkbox', 'name' => 'yes', 'id'=>'yes', 'label' =>'S&iacute; deseo Guardar y afectar stock', 'value' =>
						0 ),
				array('type' => 'checkbox', 'name' => 'notyes', 'id'=>'notyes', 'label' =>'No, agregar costo de entrega', 'value'
						=> 1)
		);
		print "<script type='text/javascript'>
		
		$(document).ready(function() {
		
		    $(document).on('click','#yes',function() {
		        $('#yes').attr('checked', true);
				$('#notyes').attr('checked', false);
		    });
		
		    $(document).on('click','#notyes',function() {
		        $('#yes').attr('checked', false);
				$('#notyes').attr('checked', true);
		    });
		});
	    
	</script>";
		
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id='.$id.'&act=card&type=1&idrec='.$idrec, ''.$langs->trans('msgCloseReceptionStockSinGasto').'', '', 'close', $formquestion, 0, 1,200,900);
		print $formconfirm;
	}
	
}


if($action=='updateNote'){
	$ty=GETPOST('ty');
	$idp=GETPOST('id');
	$textNote=GETPOST('textNote');	
	$idrec=GETPOST('note');

	$not = ($ty==1) ? "note_public" : "note_private" ;
	
	$string="Update  ".MAIN_DB_PREFIX."recepcionavanzada_recepcion"." set ".$not." = '".$textNote. "' where rowid=".$idrec;
	$sql= $db->query($string);
	//echo $string;
	$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
$res=$db->query($string);
}

if($action=='create'){
	$idp=GETPOST('id');
	$sql="SELECT AUTO_INCREMENT as next FROM information_schema.TABLES WHERE 
			TABLE_SCHEMA='".$db->database_name."' and TABLE_NAME='".MAIN_DB_PREFIX."recepcionavanzada_recepcion'";
	$rq=$db->query($sql);
	$rs=$db->fetch_object($rq);
	$ref="(PROV".$rs->next.")";
	$er1="SI";
	$partes= explode("/", GETPOST('recepcion'));
	If (@checkdate ($partes[1],$partes[0],$partes[2]))
	{
		$er1="SI";
		@$aux=str_replace('/','-', GETPOST('recepcion'));
		@$date_orderreception=date('Y-m-d',strtotime($aux));
	}else{
		$er1="NO";
	}
	$dateop=strtotime($aux);
	$time_orderreception=GETPOST('recepcionhour').":".GETPOST('recepcionmin').":00";
	$fk_user_reception=GETPOST('userc');
	$fk_user_warehouse=GETPOST('almacend');
	$exchange_rate=GETPOST('tcambio');
	$er2="SI";
	if(!is_numeric($exchange_rate)){
		$er2="NO";
	}
	$tpcambio=$exchange_rate;
	$status_import_declaration=GETPOST('genpedim');
	$import_declaration=null;
	$date_import_declaration=null;
	$er3="SI";
	if($status_import_declaration==1){
		$import_declaration=GETPOST('pedimento');
		$partes= explode("/", GETPOST('fechaped'));
		If (@checkdate ($partes[1],$partes[0],$partes[2]))
		{
			$er3="SI";
			@$aux=str_replace('/','-', GETPOST('fechaped'));
			@$date_import_declaration=date('Y-m-d',strtotime($aux));
		}else{
			$er3="NO";
		}
		$dateped=strtotime($aux);
		$aduana=GETPOST('aduana');
	}
	$ntpublica=GETPOST('ntpublica');
	$ntprivada=GETPOST('ntprivada');
	$fk_user_creation=$user->id;
	
	if($er1=='SI' && $er2=='SI' && $er3=='SI'){
		if($date_import_declaration==null){
			$sql="INSERT INTO ".MAIN_DB_PREFIX."recepcionavanzada_recepcion 
				(fk_order_supplier, ref, date_orderreception, time_orderreception, fk_user_reception, 
				fk_user_warehouse, exchange_rate, status_import_declaration, import_declaration, 
				 aduana, note_public, note_private, fk_user_creation, 
				date_creation,status,entity) VALUES
				('".$id."','".$ref."','".$date_orderreception."','".$time_orderreception."','".$fk_user_reception."',
				 '".$fk_user_warehouse."','".$exchange_rate."','".$status_import_declaration."','".$import_declaration."',
				 '".$aduana."','".$ntpublica."','".$ntprivada."','".$fk_user_creation."',
				 now(),'0','".$conf->entity."')";		
		}else{
		$sql="INSERT INTO ".MAIN_DB_PREFIX."recepcionavanzada_recepcion 
				(fk_order_supplier, ref, date_orderreception, time_orderreception, fk_user_reception, 
				fk_user_warehouse, exchange_rate, status_import_declaration, import_declaration, 
				date_import_declaration, aduana, note_public, note_private, fk_user_creation, 
				date_creation,status,entity) VALUES
				('".$id."','".$ref."','".$date_orderreception."','".$time_orderreception."','".$fk_user_reception."',
				 '".$fk_user_warehouse."','".$exchange_rate."','".$status_import_declaration."','".$import_declaration."',
				 '".$date_import_declaration."','".$aduana."','".$ntpublica."','".$ntprivada."','".$fk_user_creation."',
				 now(),'0','".$conf->entity."')";
		}
		//print "<br><br>".$sql."<br>";
		if($rq=$db->query($sql)){
			$idrec=$db->last_insert_id(MAIN_DB_PREFIX."recepcionavanzada_recepcion");
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
			$res=$db->query($string);
			print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&idrec=".$idrec."'</script>";
		}else{
			dol_htmloutput_errors($langs->trans('msgErrorAddErase'));
			$action='new';
		}
	}else{
		if($er1=='NO'){
			dol_htmloutput_errors($langs->trans('msgErrorDateReception'));
			$action='new';
		}else{
			if($er2=='NO'){
				dol_htmloutput_errors($langs->trans('msgErrorTypeCamb'));
				$action='new';
			}else{
				dol_htmloutput_errors($langs->trans('msgErrorDatePedim'));
				$action='new';
			}
		}
	}
}


if($action=='new'){
	print_fiche_titre($langs->trans('newReception'),'','setup');
	$idp=GETPOST('id');
	$object = new CommandeFournisseur($db);
	$object->fetch($idp);
	//print ">>".$idp."<br>";
	print "<form action='recepcion.php?id=".$idp."&act=card&action=create' method='POST'><table  class='border' width='100%'>";
	/* print "<tr>";
		print "<td width='25%'>Pedido Proveedor</td>";
		print "<td><a href='".DOL_MAIN_URL_ROOT."/fourn/commande/card.php?id=".$idp."'>".$object->ref."</a></td>";
	print "</tr>"; */
	print "<tr>";
		print "<td width='25%'>Ref.</td>";
		print "<td>".$langs->trans('draf')."</td>";
	print "</tr>";
	print "<tr>";
		print "<td><strong>".$langs->trans('dateReception')."</strong></td>";
		print "<td>";
		if($dateop==''){				
			date_default_timezone_set('America/Mexico_City');
			$dateop=strtotime( date('Y-m-d h:i'));
		}
		$form->select_date($dateop,'recepcion',1,1,0,'nrecepcion');
		print "</td>";
	print "</tr>";
	print "<tr>";
		print "<td><strong>".$langs->trans('receive')."</strong></td>";
		$sql="SELECT rowid, firstname, lastname
				FROM ".MAIN_DB_PREFIX."user
				WHERE (entity=0 OR entity=".$conf->entity.") ";
		$rqs=$db->query($sql);
		print "<td><select name='userc'>";
			while($rsl=$db->fetch_object($rqs)){
				$ro='';
				if($fk_user_reception!=''){
					if($rsl->rowid==$fk_user_reception){
						$ro=' SELECTED';
					}
				}else{
					if($rsl->rowid==$user->id){
						$ro=' SELECTED';
					}
				}
				print "<option value='".$rsl->rowid."' ".$ro.">".$rsl->firstname." ".$rsl->lastname."</option>";
			}
		print "</select></td>";
	print "</tr>";
	print "<tr>";
		print "<td><strong>".$langs->trans('warehouseDest')."</strong></td>";
		$sql="SELECT rowid, lieu
				FROM ".MAIN_DB_PREFIX."entrepot
				WHERE entity=".$conf->entity;
		$rqs=$db->query($sql);
		print "<td><select name='almacend'>";
		while($rsl=$db->fetch_object($rqs)){
			$ro='';
			if($fk_user_warehouse!=''){
				if($fk_user_warehouse==$rsl->rowid){
					$ro=' SELECTED';
				}
			}
			print "<option value='".$rsl->rowid."' ".$ro.">".$rsl->lieu."</option>";
		}
		print "</select></td>";
	print "</tr>";
	print "<tr>";
	if($tpcambio==''){
		$divisadoli=$conf->currency;
		$divisadocumento=$conf->currency;
		$tpcambio=1;
		if($conf->global->MAIN_MODULE_MULTIDIVISA){
			$sqld="SELECT divisa FROM ".MAIN_DB_PREFIX."multidivisa_commande_fournisseur WHERE fk_object=".$idp;
			//print $sqld;
			$rqd=$db->query($sqld);
			$nrd=$db->num_rows($rqd);
			if($nrd>0){
				$rsld=$db->fetch_object($rqd);
				$divisadocumento=$rsld->divisa;
			}
		}
		if($divisadoli!=$divisadocumento){
			//$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$divisadoli.$divisadocumento.'=X';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$divisadocumento.$divisadoli.'=X';
			$handle = fopen($url, 'r');
			if ($handle) {
				$result = fgetcsv($handle);
				fclose($handle);
			}
			$tpcambio=$result[0];
		}
	}
		print "<td>".$langs->trans('typeCamb')."</td>";
		print "<td><input type='text' name='tcambio' size='8' value='".$tpcambio."'></td>";
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('generatePedImport')."</td>";
		print "<td><select name='genpedim' onchange='activar(this.value);'>";
			$aa='';$bb='';
			if($status_import_declaration==0){$aa=' SELECTED';}
			if($status_import_declaration==1){$bb=' SELECTED';}
			print "<option value='0' ".$aa.">".$langs->trans('No')."</option>";
			print "<option value='1' ".$bb.">".$langs->trans('Yes')."</option>";
		print "</select></td>";
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('pedImport')."</td>";
		if($status_import_declaration==1){
			print "<td><input type='text' name='pedimento' id='pedimento' size='30' value='".$import_declaration."'></td>";
		}else{
		print "<td><input type='text' name='pedimento' id='pedimento' size='30'  value='".$import_declaration."' disabled></td>";
		}
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('datePed')."</td>";
		print "<td>";
		if($dateped==''){
			$dateped=date('Y-m-d');
		}
		$form->select_date($dateped,'fechaped',0,0,0,'nfechaped');
		print "</td>";
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('customs')."</td>";
		if($status_import_declaration==1){
			print "<td><input type='text' name='aduana' id='aduana' size='30' value='".$aduana."'></td>";
		}else{
			print "<td><input type='text' name='aduana' id='aduana' size='30' value='".$aduana."' disabled></td>";
		}
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('notePublic')."</td>";
		print "<td><textarea rows='4' cols='50' name='ntpublica' id='ntpublica'>".$ntpublica."</textarea></td>";
	print "</tr>";
	print "<tr>";
		print "<td>".$langs->trans('notePrivate')."</td>";
		print "<td><textarea rows='4' cols='50' name='ntprivada' id='ntprivada'>".$ntprivada."</textarea></td>";
	print "</tr>";
	print "<tr>";
		print "<td colspan='2' align='center'><input type='submit' name='create' value='".$langs->trans('createDraft')."'>
				<a href='recepcion.php?id=".$idp."' class='button'>Cancelar</a></td>";
	print "</tr>";
	print "</table></form>";
	if($status_import_declaration==1){
		print "<script>
			function activar(valact){
				//alert('lalalal: '+valact);
				if(valact==1){
					document.getElementById('pedimento').disabled = false;
					document.getElementById('aduana').disabled = false;
					document.getElementById('fechaped').disabled = false;
				}
				if(valact==0){
					document.getElementById('pedimento').disabled = true;
					document.getElementById('aduana').disabled = true;
					document.getElementById('fechaped').disabled = true;
				}
			}
			</script>";
	}else{
	print "<script>
			window.onload = function() { 
				document.getElementById('fechaped').disabled = true;
			}
			function activar(valact){
				//alert('lalalal: '+valact);
				if(valact==1){
					document.getElementById('pedimento').disabled = false;
					document.getElementById('aduana').disabled = false;
					document.getElementById('fechaped').disabled = false;
				}
				if(valact==0){
					document.getElementById('pedimento').disabled = true;
					document.getElementById('aduana').disabled = true;
					document.getElementById('fechaped').disabled = true;
				}
			}
			</script>";
	}
}
if(GETPOST('type')){
	$idp=GETPOST('id');

	if(GETPOST('idrec')){
		$idrec=GETPOST('idrec');
	}elseif (GETPOST('coste')) {
		$idrec=GETPOST('coste');
	}elseif (GETPOST('note')) {
		$idrec=GETPOST('note');
	}elseif (GETPOST('file')) {
		$idrec=GETPOST('file');
	}elseif (GETPOST('log')) {
		$idrec=GETPOST('log');
	}

	$head = array();
	$h=0;
	
	$sqb="SELECT bandConfgCostos FROM ".MAIN_DB_PREFIX."recepcionavanzada_conf WHERE entity=".$conf->entity;
	$rbs=$db->query($sqb);
	$rbb=$db->fetch_object($rbs);
	if($rbb->bandConfgCostos==1){
		$head[$h][0] ="recepcion.php?id=".$idp."&act=card&type=1&coste=".$idrec;
		$head[$h][1] = $langs->trans('costes');
		$head[$h][2] = 'costescard';
		$h++;
	}
	
	$head[$h][0] ="recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec;
	$head[$h][1] = $langs->trans('afectaDeStock');
	$head[$h][2] = 'card';
	$h++;
	
	$total_notes=0;
	$string='select note_public, note_private from '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion where rowid='.$idrec;
	
	$sql= $db->query($string);
	$res=$db->fetch_object($sql);
	if($res->note_public!=null){
		$total_notes=$total_notes+1;
	}
	if( $res->note_private!=null){
		$total_notes=$total_notes+1;
	}

	$head[$h][0] ="recepcion.php?id=".$idp."&act=card&type=1&note=".$idrec;
	$head[$h][1] = ''.$langs->trans('notes').''.' <span class="badge">'.$total_notes.'</span>';;
	$head[$h][2] = 'notascard';
	$h++;

	$upload_dir = DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/";
	$total_docs = count(dol_dir_list($upload_dir,'files',0,'','(\.meta|_preview\.png)$'));
	$head[$h][0] ="recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec;
	$head[$h][1] = ''.$langs->trans('linkedFiles').''.' <span class="badge">'.$total_docs.'</span>';
	$head[$h][2] = 'filecard';
	$h++;

	$head[$h][0] ="recepcion.php?id=".$idp."&act=card&type=1&log=".$idrec;
	$head[$h][1] = $langs->trans('Log');
	$head[$h][2] = 'logcard';
	$h++;
}

if(GETPOST('coste')){
	$title="Costos";
	dol_fiche_head($head, 'costescard', $title, 0, 'order');
	global $langs;

	$langs->load('companies');
	$langs->load('suppliers');
	$langs->load('products');
	$langs->load('bills');
	$langs->load('orders');
	$langs->load('commercial');
	$idp=GETPOST('id');
	$idrec=GETPOST('coste');
	//print "O:".$idp."  R:".$idrec." < <br>";
	include 'xml/form.php';
}

if(GETPOST('file')){

	$sql="SELECT status 
		FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion 
		WHERE rowid=".$idrec."";
		//echo $sql;
	$rq=$db->query($sql);		
	$rs=$db->fetch_object($rq);

	$title=$langs->trans('linkedFiles');
	dol_fiche_head($head, 'filecard', $title, 0, 'order');
	$idp=GETPOST('id');
	$idrec=GETPOST('file');
	if(file_exists(DOL_DATA_ROOT.'/fournisseur/commande/reception')){/*print "EXISTE<br>";*/}else{
		mkdir(DOL_DATA_ROOT.'/fournisseur/commande/reception',0755);/*print "NO EXISTE<br>";*/}
	if(file_exists(DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec)){/*print "EXISTE<br>";*/}else{
		mkdir(DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec,0755);/*rint "NO EXISTE<br>";*/}
		
	if($_REQUEST["carga"]){
		$filename = $_FILES["doctrans"]["name"];
		$tipofile = explode(".", $filename);
		$tipofile=$tipofile[1];
		//echo "El archivo es valido. <br><br>";
		$dir_subida = DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/";
		$fichero_subido = $dir_subida . basename($_FILES['doctrans']['name']);
		if(move_uploaded_file($_FILES['doctrans']['tmp_name'], $fichero_subido)){
			//echo $langs->trans('ElArchCargoExito')."<br>";
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
			$res=$db->query($string);
			print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."';</script>";
		}else{
			//echo $langs->trans('ErrorCargaArch')."<br>";
		}
	}
	if($_REQUEST['deldoc']){
		//print $_REQUEST['deldoc']."<---";
		$dir_eliminar = DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/".$_REQUEST['deldoc'];
		unlink($dir_eliminar);
		$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);
		print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."';</script>";
	}
	if($rs->status!=2){
		print "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."' enctype='multipart/form-data'>";
		print "<table width='100%' class='notopnoleftnoright'>";
		print "<tr class='liste_titre'><td>".$langs->trans('attachNewFile')."</td></tr>";
		print "<tr><td>";
		print $langs->trans('document')." <input type='file' name='doctrans' id='doctrans' required>";	
		print "&nbsp;&nbsp;&nbsp;<input type='submit' name='carga' value='".$langs->trans('load')."'>";	
		print "</td></tr>";
		print "</form>";
		print "</table><br><br>";
	}
	
	$filedir=DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/";
	$titletoshow='Archivos vinculados';
	$modulepart="commande_fournisseur";
	@$conf->$modulepart->dir_output="reception";
	$file_list=dol_dir_list($filedir,'files',0,'','\.meta$','date',SORT_DESC);
	if (! empty($file_list) && ! $headershown)
	{
		$headershown=1;
		$out.= '<div class="liste_titre">';
		$out.= "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."'>";
		$out.= $titletoshow."&nbsp;&nbsp;&nbsp;&nbsp;";
		$out.= "</form>";
		$out.='</div>';
		$out.= '<table class="border" summary="listofdocumentstable" width="100%">';
	}else{
		$out.= '<div class="liste_titre">';
		$out.= "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."'>";
		$out.= $titletoshow."&nbsp;&nbsp;&nbsp;&nbsp;";
		$out.= "</form>";
		$out.='</div>';
	}
	// Loop on each file found
	if (is_array($file_list))
	{
		foreach($file_list as $file)
		{
			$var=!$var;
			// Define relative path for download link (depends on module)
			$relativepath="reception/".$idrec."/".$file["name"];								// Cas general
			//if ($filename) $relativepath=$filename."/".$file["name"];	// Cas propal, facture...
			// Autre cas
			//if ($modulepart == 'donation')            { $relativepath = get_exdir($filename,2).$file["name"]; }
			//if ($modulepart == 'export')              { $relativepath = $file["name"]; }
			$out.= "<tr ".$bc[$var].">";
			// Show file name with link to download
			$out.= '<td nowrap="nowrap">';
			$out.= '<a href="'.DOL_URL_ROOT .'/document.php?modulepart='.$modulepart.'&amp;file='.urlencode($relativepath).'"';
			//$out.= '<a href="'.DOL_DOCUMENT_ROOT."/cfdinomina/".$relativepath.'"';
			$mime=dol_mimetype($relativepath,'',0);
			if (preg_match('/text/',$mime)) $out.= ' target="_blank"';
			$out.= '>';
			$out.= img_mime($file["name"],$langs->trans("File").': '.$file["name"]).' '.dol_trunc($file["name"],$maxfilenamelength);
			$out.= '</a>'."\n";
			$out.= '</td>';
			// Show file size
			$size=(! empty($file['size'])?$file['size']:dol_filesize($filedir."/".$file["name"]));
			$out.= '<td align="right" nowrap="nowrap">'.dol_print_size($size).'</td>';
			// Show file date
			$date=(! empty($file['date'])?$file['date']:dol_filemtime($filedir."/".$file["name"]));
			$out.= '<td align="right" nowrap="nowrap">'.dol_print_date($date, 'dayhour').'</td>';
			
			if($rs->status!=2){
				$out.= '<td>';
				$out.= '<a href="recepcion.php?id='.$idp.'&act=card&type=1&file='.$idrec.'&deldoc='.$file["name"].'">'.img_delete().'</a>';
				$out.= '</td>';
			}
			
			
		}
		$out.= '</tr>';
	}
	$out.= '</table><br><br>';
	print $out;
	
}

if(GETPOST('log')){

	$title=$langs->trans('Log');
	dol_fiche_head($head, 'logcard', $title, 0, 'order');

	$idp=GETPOST('id');
	$idrec=GETPOST('log');
	
	$sql="SELECT a.fk_user_creation, a.date_creation, a.fk_user_validation, a.date_validation, a.fk_user_close, a.date_close, a.date_mod, a.fk_user_mod
		FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion as a WHERE a.fk_order_supplier=".$idp." AND a.rowid=".$idrec." AND a.entity=".$conf->entity.";";
	
	$rq=$db->query($sql);
	$nr=$db->num_rows($rq);
	if($nr>0){			
		$rs=$db->fetch_object($rq);
		print "<table  class='border' width='100%'>";
			print "<tr>";
				print "<td width='25%'>".$langs->trans('createFor')."</td>";
				print "<td>".getNombre($rs->fk_user_creation,$db)."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('dateCreate')."</td>";
				if(strcmp($rs->date_creation,'')==0){
					print "<td></td>";
				}else{
					print "<td>".date('d/m/Y',strtotime($rs->date_creation))."</td>";	
				}				
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('lastModificationFor')."</td>";
				print "<td>".getNombre($rs->fk_user_mod,$db)."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('dateLastModification')."</td>";
				if(strcmp($rs->date_mod,'')==0){
					print "<td></td>";
				}else{
					print "<td>".date('d/m/Y',strtotime($rs->date_mod))."</td>";	
				}	
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('validateFor')."</td>";
				print "<td>".getNombre($rs->fk_user_validation,$db)."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('dateValidate')."</td>";
				if(strcmp($rs->date_validation,'')==0){
					print "<td></td>";
				}else{
					print "<td>".date('d/m/Y',strtotime($rs->date_validation))."</td>";	
				}					
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('closeFor')."</td>";
				print "<td>".getNombre($rs->fk_user_close,$db)."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('dateClose')."</td>";
				if(strcmp($rs->date_close,'')==0){
					print "<td></td>";
				}else{
					print "<td>".date('d/m/Y',strtotime($rs->date_close))."</td>";	
				}				
			print "</tr>";			
		print "</table>";
	}


}

if(GETPOST('note')){

	$title=$langs->trans('notes');
	dol_fiche_head($head, 'notascard', $title, 0, 'order');

	$idp=GETPOST('id');
	$idrec=GETPOST('note');

	//print "O:".$idp."  R:".$idrec." < <br>";
	$sql="SELECT a.*, b.firstname,b.lastname, c.lieu
	FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion a, ".MAIN_DB_PREFIX."user b, ".MAIN_DB_PREFIX."entrepot c
	WHERE a.fk_order_supplier=".$idp." AND a.rowid=".$idrec." AND a.entity=".$conf->entity." AND a.fk_user_reception=b.rowid
		AND a.fk_user_warehouse=c.rowid";
		//echo $sql;
	$rq=$db->query($sql);
	$nr=$db->num_rows($rq);
	if($nr>0){			
		$rs=$db->fetch_object($rq);
		print "<table  class='border' width='100%'>";
			print "<tr>";
				print "<td width='25%'>Ref.</td>";
				print "<td>".$rs->ref."</td>";
			print "</tr>";
			print "<tr>";
				print "<td><strong>".$langs->trans('dateReception')."</strong></td>";
				print "<td>".date('d/m/Y',strtotime($rs->date_orderreception))." ".$rs->time_orderreception."</td>";
			print "</tr>";
			print "<tr>";
				print "<td><strong>".$langs->trans('receive')."</strong></td>";
				print "<td>".$rs->firstname." ".$rs->lastname."</td>";
			print "</tr>";
			print "<tr>";
				print "<td><strong>".$langs->trans('warehouseDest')."</strong></td>";
				print "<td>".$rs->lieu."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('typeCamb')."</td>";
				print "<td>".$rs->exchange_rate."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('generatePedImport')."</td>";
				if($rs->status_import_declaration==0){$generap='NO';}
				if($rs->status_import_declaration==1){$generap='SI';}
				print "<td>".$generap."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('pedImport')."</td>";
				print "<td>".$rs->import_declaration."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('datePed')."</td>";
				if(strcmp($rs->date_import_declaration, '')==0){
					print "<td></td>";
				}else{
					print "<td>".date('d/m/Y',strtotime($rs->date_import_declaration))."</td>";	
				}				
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('customs')."</td>";
				print "<td>".$rs->aduana."</td>";
			print "</tr>";
			print "<tr>";				
				print "<td>".$langs->trans('notePublic')."";
				if($rs->status!=2){
					print "<a href='recepcion.php?id=".$idp."&act=card&action=editNote&ty=1&type=1&note=".$idrec."'>".img_edit($langs->trans('SetConditions'),1)."</a></td>";
				}
				print "<td>".$rs->note_public."</td>";
			print "</tr>";
			print "<tr>";
				print "<td>".$langs->trans('notePrivate')."";
				if($rs->status!=2){
					print"<a href='recepcion.php?id=".$idp."&act=card&action=editNote&ty=2&type=1&note=".$idrec."'>".img_edit($langs->trans('SetConditions'),1)."</a></td>";
				} 
				print "<td>".$rs->note_private."</td>";
			print "</tr>";
		print "</table>";
	}


}


if(GETPOST('idrec')){
	$idp=GETPOST('id');
	$idrec=GETPOST('idrec');
	//print "O:".$idp."  R:".$idrec." < <br>";
	$sql="SELECT a.*, b.firstname,b.lastname, c.lieu
FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion a, ".MAIN_DB_PREFIX."user b, ".MAIN_DB_PREFIX."entrepot c
WHERE a.fk_order_supplier=".$idp." AND a.rowid=".$idrec." AND a.entity=".$conf->entity." AND a.fk_user_reception=b.rowid
		AND a.fk_user_warehouse=c.rowid";
		//echo $sql;
	$rq=$db->query($sql);
	$nr=$db->num_rows($rq);
	if($nr>0){

	$title=$langs->trans('fiche');
	dol_fiche_head($head, 'card', $title, 0, 'order');
		
	$rs=$db->fetch_object($rq);
	$pedimento = "Sin pedimento";
	print "<table  class='border' width='100%' style='text-align:left'>";
	print "<tr>";
		print "<td width='25%'>Ref.</td>";
		print "<td>".$rs->ref."</td>";
		
		$do="";
		if($rs->status==0 && $rs->status_import_declaration==1){
			$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=importdeclaration'>".img_edit($langs->trans('SetConditions'),1)."</a>";
		}
		print "<td>Pedimento de importacion ".$do."</td>";
		if(GETPOST('modfconf')=='importdeclaration'){
			$import_declaration=GETPOST('pedimento');
			$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
					SET import_declaration='".$import_declaration."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
			if($db->query($squ)){
				$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
				$res=$db->query($string);
				print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
			}
		}
		if(GETPOST('modf')=='importdeclaration'){
			print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=importdeclaration'>
					<input type='text' name='pedimento' id='pedimento' size='30' value='".$rs->import_declaration."'>
					<input type='submit' value='".$langs->trans('save')."'></form></td>";
		}else{
			print "<td>".$rs->import_declaration."</td>";
			$pedimento = $rs->import_declaration;
		}	

	print "</tr>";

	print "<tr>";
	$do="";
	if($rs->status==0){
		$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=dateorderreception'>".img_edit($langs->trans('SetConditions'),1)."</a>";
	}
	print "<td><strong>".$langs->trans('dateReception')."</strong> ".$do."</td>";
	if(GETPOST('modfconf')=='dateorderreception'){
		$er1="SI";
		$partes= explode("/", GETPOST('recepcion'));
		If (@checkdate ($partes[1],$partes[0],$partes[2]))
		{
			$er1="SI";
			@$aux=str_replace('/','-', GETPOST('recepcion'));
			@$date_orderreception=date('Y-m-d',strtotime($aux));
		}else{
			$er1="NO";
		}
		$dateop=strtotime($aux);
		$time_orderreception=GETPOST('recepcionhour').":".GETPOST('recepcionmin').":00";
		if($er1=='NO'){
			dol_htmloutput_errors($langs->trans('msgErrorDateReception'));
		}else{
			$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				SET date_orderreception='".$date_orderreception."', time_orderreception='".$time_orderreception."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
			if($db->query($squ)){
				$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
				$res=$db->query($string);
				print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
			}
		}
	}
	if(GETPOST('modf')=='dateorderreception'){
		print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=dateorderreception'>";
		$dateop=strtotime($rs->date_orderreception." ".$rs->time_orderreception);
		$form->select_date($dateop,'recepcion',1,1,0,'nrecepcion');
		print " <input type='submit' value='".$langs->trans('save')."'></form></td>";
	}else{
	print "<td>"; print date('d/m/Y',strtotime($rs->date_orderreception)); print " ".$rs->time_orderreception."</td>";
	}

		
		$do="";
		if($rs->status==0 && $rs->status_import_declaration==1){
			$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=dateimportdeclaration'>".img_edit($langs->trans('SetConditions'),1)."</a>";
		}
		print "<td>".$langs->trans('datePed')."".$do."</td>";
		if(GETPOST('modfconf')=='dateimportdeclaration'){
			$partes= explode("/", GETPOST('fechaped'));
			If (@checkdate ($partes[1],$partes[0],$partes[2]))
			{
				$er3="SI";
				@$aux=str_replace('/','-', GETPOST('fechaped'));
				@$date_import_declaration=date('Y-m-d',strtotime($aux));
			}else{
				$er3="NO";
			}
			if($er3=="SI"){
				$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
						SET date_import_declaration='".$date_import_declaration."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
				if($db->query($squ)){
					$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
					$res=$db->query($string);
					print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
				}
			}else{
				dol_htmloutput_errors($langs->trans('msgErrorDatePedim'));
			}
		}
		if(GETPOST('modf')=='dateimportdeclaration'){
			print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=dateimportdeclaration'>";
			if($rs->date_import_declaration!='0000-00-00'){
				$dateped=strtotime($rs->date_import_declaration);
			}
			$form->select_date($dateped,'fechaped',0,0,0,'nfechaped');
			print " <input type='submit' value='".$langs->trans('save')."'></form></td>";
		}else{
			if(strcmp($rs->date_import_declaration,'')!=0){

				print "<td>"; print date('d/m/Y',strtotime($rs->date_import_declaration));print "</td>";
			}else{
				print "<td></td>";
			}
		}
		

	print "</tr>";
	print "<tr>";
	$do="";
	if($rs->status==0){
		$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=userc'>".img_edit($langs->trans('SetConditions'),1)."</a>";
	}
	print "<td><strong>Recibe</strong> ".$do."</td>";
	if(GETPOST('modfconf')=='userc'){
		$fk_user_reception=GETPOST('userc');
		$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				SET fk_user_reception='".$fk_user_reception."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
		if($db->query($squ)){
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
			$res=$db->query($string);
			print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
		}
	}
	if(GETPOST('modf')=='userc'){
		$sql="SELECT rowid, firstname, lastname
				FROM ".MAIN_DB_PREFIX."user
				WHERE (entity=0 OR entity=".$conf->entity.") ";
		$rqs=$db->query($sql);
		print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=userc'>";
		print "<select name='userc'>";
		while($rsl=$db->fetch_object($rqs)){
			$ro='';
			if($rs->fk_user_reception!=''){
				if($rsl->rowid==$rs->fk_user_reception){
					$ro=' SELECTED';
				}
			}else{
				if($rsl->rowid==$user->id){
					$ro=' SELECTED';
				}
			}
			print "<option value='".$rsl->rowid."' ".$ro.">".$rsl->firstname." ".$rsl->lastname."</option>";
		}
		print "</select> <input type='submit' value='".$langs->trans('save')."'></form></td>";
	}else{
		print "<td>".$rs->firstname." ".$rs->lastname."</td>";
	}

	
		$do="";
		if($rs->status==0 && $rs->status_import_declaration==1){
			$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=aduana'>".img_edit($langs->trans('SetConditions'),1)."</a>";
		}
		print "<td>".$langs->trans('customs')." ".$do."</td>";
		if(GETPOST('modfconf')=='aduana'){
			$aduana=GETPOST('aduana');
			$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
					SET aduana='".$aduana."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
			if($db->query($squ)){
				$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
				$res=$db->query($string);
				print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
			}
		}
		if(GETPOST('modf')=='aduana'){
			print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=aduana'>
					<input type='text' name='aduana' id='aduana' size='30' value='".$rs->aduana."'>
					 <input type='submit' value='".$langs->trans('save')."'></form></td>";
		}else{
			print "<td>".$rs->aduana."</td>";
		}
	

	print "</tr>";
	print "<tr>";
	$do="";
	if($rs->status==0){
		$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=almacend'>".img_edit($langs->trans('SetConditions'),1)."</a>";
	}
	print "<td><strong>".$langs->trans('warehouseDest')."</strong>  ".$do."</td>";
	if(GETPOST('modfconf')=='almacend'){
		$fk_user_warehouse=GETPOST('almacend');
		$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				SET fk_user_warehouse='".$fk_user_warehouse."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
		if($db->query($squ)){
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
			$res=$db->query($string);
			print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
		}
	}
	if(GETPOST('modf')=='almacend'){
		$sql="SELECT rowid, lieu
				FROM ".MAIN_DB_PREFIX."entrepot
				WHERE entity=".$conf->entity;
		$rqs=$db->query($sql);
		print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=almacend'>";
		print "<select name='almacend'>";
		while($rsl=$db->fetch_object($rqs)){
			$ro='';
			if($rs->fk_user_warehouse!=''){
				if($rs->fk_user_warehouse==$rsl->rowid){
					$ro=' SELECTED';
				}
			}
			print "<option value='".$rsl->rowid."' ".$ro.">".$rsl->lieu."</option>";
		}
		print "</select> <input type='submit' value='".$langs->trans('save')."'></form></td>";
	}else{
		print "<td>".$rs->lieu."</td>";
	}
	
	print "<td>".$langs->trans('notePublic')."</td>";
	print "<td>".$rs->note_public."</td>";
	

	print "</tr>";
	print "<tr>";
	$do="";
	if($rs->status==0){
		$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=exchangerate'>".img_edit($langs->trans('SetConditions'),1)."</a>";
	}
	print "<td>".$langs->trans('typeCamb')." ".$do."</td>";
	if(GETPOST('modfconf')=='exchangerate'){
		$exchange_rate=GETPOST('tcambio');
		$er2="SI";
		if(!is_numeric($exchange_rate)){
			$er2="NO";
		}
		if($er2=="SI"){
			$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
					SET exchange_rate='".$exchange_rate."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
			if($db->query($squ)){
				$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
				$res=$db->query($string);
				print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
			}
		}else{
			dol_htmloutput_errors($langs->trans('msgErrorTypeCamb'));
		}
	}
	if(GETPOST('modf')=='exchangerate'){
		print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=exchangerate'>
				<input type='text' name='tcambio' size='8' value='".$rs->exchange_rate."'> 
				<input type='submit' value='".$langs->trans('save')."'></form></td>";
	}else{
		print "<td>".$rs->exchange_rate."</td>";
	}

	
	print "<td>".$langs->trans('notePrivate')."</td>";
	print "<td>".$rs->note_private."</td>";
	

	print "</tr>";
	print "<tr>";
	$do="";
	if($rs->status==0){
		$do="<a href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modf=statusimportdeclaration'>".img_edit($langs->trans('SetConditions'),1)."</a>";
	}
	print "<td>".$langs->trans('generatePedImport')." ".$do."</td>";
	if($rs->status_import_declaration==0){$generap='NO';}
	if($rs->status_import_declaration==1){$generap='SI';}
	if(GETPOST('modfconf')=='statusimportdeclaration'){
		$status_import_declaration=GETPOST('genpedim');
		$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				SET status_import_declaration='".$status_import_declaration."' WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
		if($db->query($squ)){
			$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
			$res=$db->query($string);
			print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
		}
	}
	if(GETPOST('modf')=='statusimportdeclaration'){
		print "<td><form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."&modfconf=statusimportdeclaration'><select name='genpedim'>";
		$aa='';$bb='';
		if($rs->status_import_declaration==0){$aa=' SELECTED';}
		if($rs->status_import_declaration==1){$bb=' SELECTED';}
		print "<option value='0' ".$aa.">NO</option>";
		print "<option value='1' ".$bb.">SI</option>";
		print "</select> <input type='submit' value='".$langs->trans('save')."'></form></td>";
	}else{
		print "<td>".$generap."</td>";
	}


	
	
	
	if($rs->status==0){
		$a='<span class="hideonsmartphone">'."Borrador".' </span>'.img_picto($langs->trans('StatusOrderDraftShort'),'statut0');
	}
	if($rs->status==1){
		$a='<span class="hideonsmartphone">'."Validado".' </span>'.img_picto($langs->trans('StatusOrderValidatedShort'),'statut1');
	}
	if($rs->status==2){
		$a='<span class="hideonsmartphone">'."Cerrado".' </span>'.img_picto($langs->trans('Cerrado'),'statut4');
	}
	print "<td>Estatus</td><td>".$a."</td>";

	print "</tr>";

	print "</table>";
	}
	
	if($rs->status==1){
		print '<br><div align="right">';		
			if ($user->rights->recepcionavanzada->edit_recep  && $commande->statut != 5){
				print '<a class="butAction" href="recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&action=validrborrador">'.$langs->trans('modify').'</a>';
			}
			if ($user->rights->recepcionavanzada->close_recep ){
				print '<a class="butAction" href="recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&action=msgClose">'.$langs->trans('close').'</a>';
			}
		print '</div>';
		print "</div>";
	}

	if($rs->status==2){
		print '<br><div align="right">';
			if ($user->rights->recepcionavanzada->reopen_recep ){
				print '<a class="butAction" href="recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&action=reabrir">'.$langs->trans('reopen').'</a>';
			}			
		print '</div>';
		print "</div>";
	}

	if(GETPOST('action')=='msgClose'){
		$form =	new Form($db);
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id='.$id.'&act=card&type=1&idrec='.$idrec, $langs->trans('msgCloseReception'),$langs->trans('msgCloseReception2'), 'closeRec', '', 0, 1);		
		print $formconfirm;	
		
	}
	
	if(GETPOST('action')=='validrborrador'){
		$form =	new Form($db);
		$formconfirm = $form->formconfirm('recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&action2=confirmborrador', $langs->trans('msgReopenReception'), $langs->trans('msgReopenReception2'), 'confirmborrador', $formquestion, 0, 1);
		//$formconfirm = $form->formconfirm('recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&action2=confirmborrador', 'Desea regresar la orden de recepcion al estatus de borrador?', '', 'confirmborrador', $formquestion, 0, 1);
		print $formconfirm;
		
	}
	if(GETPOST('action2')=='confirmborrador'){
		$squ="SELECT rowid, fk_product, batch, value,fk_entrepot,price
				FROM ".MAIN_DB_PREFIX."stock_mouvement
				WHERE fk_origin=".$idp." AND origintype='order_supplier' AND fk_order_reception=".$idrec." 
				AND value >0 AND rowid NOT IN (SELECT fk_stock_mouvement FROM ".MAIN_DB_PREFIX."recepcion_avanzada_devolucion)";
		$drqs=$db->query($squ);
		$dnrw=$db->num_rows($drqs);
		if($dnrw>0){
			while($drs=$db->fetch_object($drqs)){

				$string2='SELECT
				 		subprice
				 	FROM
				 		'.MAIN_DB_PREFIX.'commande_fournisseurdet
				 	WHERE
				 		fk_commande='.$idp.'
				 	AND fk_product ='.$drs->fk_product;
		
				 $res2=$db->query($string2);
				 $dat2=$db->fetch_object($res2);				
				$productmv= new Product($db);
				$productmv->fetch($drs->fk_product);
				$oldqty=$productmv->stock_reel;
				$oldpmp=$productmv->pmp;
				$oldqtytouse=($oldqty >= 0?$oldqty:0);
				$qtysacar=$drs->value;
				$sqmv="SELECT exchange_rate FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				WHERE rowid=".$idrec." AND fk_order_supplier=".$idp;
				$mreq=$db->query($sqmv);
				$mres=$db->fetch_object($mreq);
				if($mres->exchange_rate!=1){
					$pricesacar = $dat2->subprice * $mres->exchange_rate;
				}else{
					$pricesacar=$dat2->subprice;
				}
				if($oldpmp>$qtysacar){
					$newpmp=price2num((($oldqtytouse * $oldpmp) - ($qtysacar * $pricesacar)) / ($oldqtytouse - $qtysacar), 'MU');
				}else{
					$newpmp=price2num(0);
				}
				//print $oldqtytouse." :: ".$oldpmp." :>".$qtysacar." :: ".$pricesacar." :>".$newpmp;

				$productn =new Product($db);
				$stlabel="".$langs->trans('msgStockDev')." ".$object->ref;
				$pricesrc=$drs->price;
				$result=$productn->fetch($drs->fk_product);
				$dsqm="INSERT INTO ".MAIN_DB_PREFIX."recepcion_avanzada_devolucion (fk_stock_mouvement) VALUE(".$drs->rowid.")";
				$db->query($dsqm);
				//$squ="DELETE FROM ".MAIN_DB_PREFIX."stock_mouvement WHERE rowid=".$drs->rowid;
				//$db->query($squ);
				$productn->load_stock();    // Load array product->stock_warehouse
				
				if (! empty($productn->pmp)) $pricesrc=$productn->pmp;
				$pricedest=$pricesrc;
				$pricedest = $dat2->subprice ;
				if (empty($conf->productbatch->enabled) || ! $productn->hasbatch())        // If product does not need lot/serial
				{
					// Remove stock
					$result1=$productn->correct_stock(
							$user,
							$drs->fk_entrepot,
							$drs->value,
							1,
							$stlabel,
							$pricedest
					);
				}
				else
				{
// 						$dlc=null;
// 						$dluo=null;
					// Remove stock
					$result1=$productn->correct_stock_batch(
							$user,
							$drs->fk_entrepot,
							$drs->value,
							1,
							$stlabel,
							$pricedest,
							NULL,
							NULL,
							$drs->batch
					);
		
				}
				if(1){
				$stringStock2='SELECT MAX(rowid) as id FROM '.MAIN_DB_PREFIX.'stock_mouvement where fk_product='.$drs->fk_product.'';
				$resStock2=$db->query($stringStock2);
				$datStock2=$db->fetch_object($resStock2);
				
				$stringupd2='UPDATE '.MAIN_DB_PREFIX.'stock_mouvement set origintype="order_supplier",fk_origin="'.$idp.'",fk_order_reception="'.$idrec.'" where rowid='.$datStock2->id.';';
				$resupd2=$db->query($stringupd2); 
				
				$sqw = "UPDATE ".MAIN_DB_PREFIX."product as p SET pmp = ".$newpmp."";
				$sqw.= " WHERE rowid = ".$drs->fk_product;
				//print "<br>".$sqw." <br>";
				$resw2=$db->query($sqw);
				}
			}
			
		}
		$squ="DELETE FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch WHERE fk_commande=".$idp." AND fk_order_reception=".$idrec;
		$db->query($squ);
		$squ="UPDATE ".MAIN_DB_PREFIX."recepcionavanzada_recepcion
				SET status=0 WHERE fk_order_supplier=".$idp." AND rowid=".$idrec;
		$db->query($squ);
		$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);
		print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&fiche=1&idrec=".$idrec."'</script>";
	}

	//if($rs->status==0){		
		$action='formRecPro';	
	//}
	
}




if($action=='editNote'){
	$ty=GETPOST('ty');
	$idp=GETPOST('id');
	$idrec=GETPOST('note');

	print "<form action='recepcion.php?id=".$idp."&act=card&type=1&note=".$idrec."&action=updateNote&ty=".$ty."' method='POST'>";
	print "<table>";
		print "<tr>";
			print "<td>";
				print $string = ($ty==1) ? $langs->trans('notePublic') : $langs->trans('notePrivate') ;
			print "</td>";
			print "<td>";
				$string= "select note_public, note_private from ".MAIN_DB_PREFIX."recepcionavanzada_recepcion where rowid=".$idrec;
				$sql=$db->query($string);
				$data=$db->fetch_object($sql);
				if($ty==1){
					print "<textarea COLS=100 ROWS=10 name='textNote'>".$data->note_public."</textarea>";
				}else{
					print "<textarea COLS=100 ROWS=10 name='textNote'>".$data->note_private."</textarea>";
				}
			print "</td>";
			print "<td>";				
				print "<input type='submit' value='".$langs->trans('modify')."'></br>";		
				print "</form>";		
				print "<form action='recepcion.php?id=".$idp."&act=card&type=1&note=".$idrec."' method='POST'>";
					//print "<button>Cancelar</button>";
					print "<input type='submit' value='".$langs->trans('cancel')."'></br>";		
				//print "</form>";
			print "</td>";
		print "</tr>";
	print "</table>";
	print "</form>";
	
}

function getNombre($id,$db){
	$sql="SELECT CONCAT(b.firstname, ' ', b.lastname) as name FROM  ".MAIN_DB_PREFIX."user as b WHERE b.rowid=".$id.";";
		
	$rq=$db->query($sql);		
	$nr=$db->num_rows($rq);
	if($nr>0){			
		$rs=$db->fetch_object($rq);
		return $rs->name;
	}else{
		return "";
	}
	
}


if($action=='formRecPro'){
$form =	new Form($db);
$formproduct = new FormProduct($db);
$warehouse_static = new Entrepot($db);
$supplierorderdispatch = new CommandeFournisseurDispatch($db);


//$help_url="Recepcion de Stock avanzado";
//llxHeader('',$langs->trans("Order"),$help_url,'',0,0,array('/fourn/js/lib_dispatch.js'));

$now=dol_now();

$id = GETPOST('id','int');
$ref= GETPOST('ref');
if ($id > 0 || ! empty($ref))
{
	
	$commande = new CommandeFournisseur($db);

	$result=$commande->fetch($id,$ref);
	if ($result >= 0)
	{
		$soc = new Societe($db);
		$soc->fetch($commande->socid);

		$author = new User($db);
		$author->fetch($commande->user_author_id);

		print '<input type="hidden" id="user" value="'.$user->id.'"/>';
		print '<br>';


		$disabled=1;
		if (! empty($conf->global->STOCK_CALCULATE_ON_SUPPLIER_DISPATCH_ORDER)) $disabled=0;

		/*
		 * Lignes de commandes
		 */
		if ($commande->statut <= 2 || $commande->statut >= 6)
		{
			print $langs->trans("OrderStatusNotReadyToDispatch");
		}

		if ($commande->statut == 3 || $commande->statut == 4 || $commande->statut == 5)
		{
			$entrepot = new Entrepot($db);
			$listwarehouses=$entrepot->list_array(1);


			//print "<form action='recepcion.php?id=".$idp."&act=card&type=1&idrec=".$idrec."&action=validate' method='POST'>";
			print "<form action='#' method='POST'>";
			//print '<form method="POST" action="dispatch.php?id='.$commande->id.'">';
			print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
			print '<input type="hidden" name="action" value="dispatch">';

			print "<input type='hidden' id='idPedido' value='".$id."'/>";
			print "<input type='hidden' id='ordenRec' value='".$idrec."'/>";

			print '<table class="noborder" width="100%" style="text-align:left">';

			// Set $products_dispatched with qty dispatched for each product id
			$products_dispatched = array();
			$sql = "SELECT l.rowid, cfd.fk_product, sum(cfd.qty) as qty";
			$sql.= " FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch as cfd";
			$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."commande_fournisseurdet as l on l.rowid = cfd.fk_commandefourndet";
			$sql.= " WHERE cfd.fk_commande = ".$commande->id;
			$sql.= " GROUP BY l.rowid, cfd.fk_product";


			$resql = $db->query($sql);
			if ($resql)
			{
				$num = $db->num_rows($resql);
				$i = 0;
				
				if ($num)
				{
					while ($i < $num)
					{
						$objd = $db->fetch_object($resql);
						$products_dispatched[$objd->rowid] = price2num($objd->qty, 5);
						$i++;
					}
				}
				$db->free($resql);
			}


			$sql = "SELECT l.rowid, l.fk_product, l.subprice, l.remise_percent, SUM(l.qty) as qty,";
			$sql.= " p.ref, p.label, p.tobatch";
			$sql.= " FROM ".MAIN_DB_PREFIX."commande_fournisseurdet as l";
			$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON l.fk_product=p.rowid";
			$sql.= " WHERE l.fk_commande = ".$commande->id;
			if(empty($conf->global->STOCK_SUPPORTS_SERVICES)) $sql.= " AND l.product_type = 0";
			$sql.= " GROUP BY p.ref, p.label, p.tobatch, l.rowid, l.fk_product, l.subprice, l.remise_percent";	// Calculation of amount dispatched is done per fk_product so we must group by fk_product
			//$sql.= " ORDER BY p.ref, p.label";
			$sql.= " ORDER BY p.tobatch DESC";

			$resql = $db->query($sql);

			$string_warehouse='SELECT
							fk_user_warehouse as alm
						FROM
							'.MAIN_DB_PREFIX.'recepcionavanzada_recepcion
						WHERE
							rowid  ='.$idrec.';';
			$resWare=$db->query($string_warehouse);
			$ware = $db->fetch_object($resWare);


			if ($resql)
			{
				$num = $db->num_rows($resql);				

				if ($num)
				{
					print '<tr class="liste_titre">';

					print '<td>'.$langs->trans("Description").'</td>';
					print '<td></td>';
					print '<td></td>';
					print '<td></td>';
					print '<td align="right">'.$langs->trans("QtyOrdered").'</td>';
					print '<td align="right">'.$langs->trans("QtyDispatchedShort").'</td>';
					print '<td align="right">'.$langs->trans("Cant. a recibir").'</td>';
					print '<td align="right">'.$langs->trans("Warehouse").'</td>';
					print "</tr>\n";

					if (! empty($conf->productbatch->enabled))
					{
						print '<tr class="liste_titre">';
						print '<td></td>';
						print '<td>'.$langs->trans("N&uacute;mero Lote/Serie").'</td>';
						print '<td>'.$langs->trans("Fecha caducidad/ fin garant&iacute;a").'</td>';
						print '<td>'.$langs->trans("Fecha limite de venta").'</td>';
						print '<td colspan="4">&nbsp;</td>';
						print "</tr>\n";
					}
				}
			

				$i = 0;
				while ($objp = $db->fetch_object($resql))
				{
					//$objp = $db->fetch_object($resql);

					// On n'affiche pas les produits personnalises
					if (! $objp->fk_product > 0)
					{
						$nbfreeproduct++;
					}
					else
					{
						$remaintodispatch=price2num($objp->qty - ((float) $products_dispatched[$objp->rowid]), 5);	// Calculation of dispatched
						if ($remaintodispatch < 0) $remaintodispatch=0;

						if ($remaintodispatch || empty($conf->global->SUPPLIER_ORDER_DISABLE_STOCK_DISPATCH_WHEN_TOTAL_REACHED))
						{
							$nbproduct++;

							$var=!$var;

							// To show detail cref and description value, we must make calculation by cref
							//print ($objp->cref?' ('.$objp->cref.')':'');
							//if ($objp->description) print '<br>'.nl2br($objp->description);
							$suffix='_0_'.($i+$bandAux);
							print "\n";
							print '<!-- Line '.$suffix.' -->'."\n";
							print "<tr ".$bc[$var].">";

							$linktoprod='<a href="'.DOL_URL_ROOT.'/product/fournisseurs.php?id='.$objp->fk_product.'">'.img_object($langs->trans("ShowProduct"),'product').' '.$objp->ref.'</a>';
							$linktoprod.=' - '.$objp->label."\n";

							if (! empty($conf->productbatch->enabled))
							{
								if ($objp->tobatch)
								{
									print '<td colspan="4">';
									print $linktoprod;
									print "</td>";
								}
								else
								{
									print '<td>';
									print "<input name='link'".$suffix."' type='hidden' value='".$linktoprod."'/>";
									print $linktoprod;
									print "</td>";
									print '<td colspan="3">';
									print '<input name="txt'.$suffix.'" type="hidden" value="'.$langs->trans("Este producto no usa lotes/series").'">';
									print $langs->trans("Este producto no usa lotes/series");
									print '</td>';
								}
							}
							else
							{
								print '<td colspan="4">';
								print $linktoprod;
								print "</td>";
							}

							$var=!$var;
							$up_ht_disc=$objp->subprice;
							if (! empty($objp->remise_percent) && empty($conf->global->STOCK_EXCLUDE_DISCOUNT_FOR_PMP)) $up_ht_disc=price2num($up_ht_disc * (100 - $objp->remise_percent) / 100, 'MU');

							// Qty ordered
							print '<td align="right">'.$objp->qty.'</td>';

							$sc='select  ROUND(SUM(qty),2) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
										$rc=$db->query($sc);
										$dc=$db->fetch_object($rc);										


							// Already dispatched
							print '<td align="right">';($dc->cant>0) ? print $dc->cant : print 0 ; print '</td>';

							//echo " su ".$suffix;

							//$stringStatus='SELECT STATUS from '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion  where fk_order_supplier='.$id;
							$stringStatus='SELECT STATUS from '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion  where fk_order_supplier='.$id.' and rowid='.$idrec.';';
							$query=$db->query($stringStatus);
							$resStatus=$db->fetch_object($query);

							if($resStatus->STATUS==0){
								if (! empty($conf->productbatch->enabled) && $objp->tobatch==1)
								{
									$sqlconf = "select bandConfNumSerie as band from  ".MAIN_DB_PREFIX."recepcionavanzada_conf "; 
									$resconf=$db->query($sqlconf);
									if (! $resconf){
										dol_print_error($db);	
									}else{
										$band = $db->fetch_object($resconf);
									} 

									if($band->band==1){



										//$numRow=(int)$objp->qty - (int)$products_dispatched[$objp->rowid];
										$s='select  SUM(qty) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
										$r=$db->query($s);
										$d=$db->fetch_object($r);

										$numRow=(int)$objp->qty - $d->cant;

										$type = 'batch';
										print '<td align="right"></td>';	// Dispatch column
										print '<td></td>';																													// Warehouse column
										print '</tr>';
										$bandAux++;
										$cant=0;	
											//		echo " su2 ".$suffix;
										$sqlProd='
												SELECT
													a.fk_product,
													a.qty,
													a.fk_entrepot,
													a.batch,
													a.eatby,
													a.sellby,
													p.ref,
													p.label,
													p.tobatch
												FROM
													'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
												INNER JOIN '.MAIN_DB_PREFIX.'product AS p ON a.fk_product = p.rowid
												WHERE
													a.fk_commande ='.$id.'
													AND a.fk_order_reception ='.$idrec.'
													and a.fk_product='.$objp->fk_product.';';


											$resProd=$db->query($sqlProd);
											$numP=$db->num_rows($resProd);

														
										while ($cant < $numRow) {
											$ob = $db->fetch_object($resProd);	
											//echo " su3 ".$suffix;
											$suffix='_0_'.$i.'_'.($i+$cant);
										//	echo " su4 ".$suffix;
											print '<tr '.$bc[$var].' name="'.$type.$suffix.'">';
											print '<td>';
											print '<input name="fk_commandefourndet'.$suffix.'" type="hidden" value="'.$objp->rowid.'">';
											print '<input name="product_batch'.$suffix.'"  class="producID products" type="hidden" value="'.$objp->fk_product.'">';
											print '<input name="pu'.$suffix.'" type="hidden" value="'.$up_ht_disc.'"><!-- This is a up including discount -->';
											// hidden fields for js function
											print '<input id="qty_ordered'.$suffix.'" type="hidden" value="'.$objp->qty.'">';
											print '<input id="qty_dispatched'.$suffix.'" type="hidden" value="'.(float) $products_dispatched[$objp->rowid].'">';
											print '</td>';

											print '<td>';
											print '<input type="text" class="serieBatch" required id="lot_number'.$suffix.'" name="lot_number'.$suffix.'" size="40" value="'.$ob->batch.'">';
											print '</td>';
											print '<td>';
											$dlcdatesuffix=dol_mktime(0, 0, 0, GETPOST('dlc'.$suffix.'month'), GETPOST('dlc'.$suffix.'day'), GETPOST('dlc'.$suffix.'year'));

											print '<input type="hidden" class="sufixEat"  value="'.$suffix.'">';
											$form->select_date(strtotime($ob->eatby),'dlc'.$suffix,'','',1,"");
											print '</td>';
											print '<td>';
											$dluodatesuffix=dol_mktime(0, 0, 0, GETPOST('dluo'.$suffix.'month'), GETPOST('dluo'.$suffix.'day'), GETPOST('dluo'.$suffix.'year'));
											print '<input type="hidden" class="sufixSell"  value="'.$suffix.'">';
											$form->select_date(strtotime($ob->sellby),'dluo'.$suffix,'','',1,"");
											print '</td>';
											print '<td colspan="2">&nbsp</td>';		// Qty ordered + qty already dispatached
											print '<td align="right">';
											//echo '"product'.$objp->ref.'"';
											$cantPro = ($ob->qty>0) ? $ob->qty : 0 ;
											print '<input type="hidden" class="sufixCant"  value="'.$suffix.'">';
											print '<input id="qty'.$suffix.'" name="qty'.$suffix.'" class="product'.$objp->ref.'" type="text" size="8"  value="'.$cantPro.'">';
											print '</td>';

											// Warehouse
											print '<input type="hidden" class="sufixWare"  value="'.$suffix.'">';
											
											print '<td align="right">';
											if (count($listwarehouses)>1)
											{	
												print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,1,0,$objp->fk_product);
											}
											elseif  (count($listwarehouses)==1)
											{
												print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,0,0,$objp->fk_product);
											}
											else
											{
												print $langs->trans("NoWarehouseDefined");
											}
											print "</td>\n";

											print "</tr>\n";
											$cant++;
										}
										$bandAux=$cant;
									}else{


										$sqlProd='
												SELECT
													a.fk_product,
													a.qty,
													a.fk_entrepot,
													a.batch,
													a.eatby,
													a.sellby,
													p.ref,
													p.label,
													p.tobatch
												FROM
													'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
												INNER JOIN '.MAIN_DB_PREFIX.'product AS p ON a.fk_product = p.rowid
												WHERE
													a.fk_commande ='.$id.'
													AND a.fk_order_reception ='.$idrec.'
													and a.fk_product='.$objp->fk_product.';';

											$resProd=$db->query($sqlProd);
											$numP=$db->num_rows($resProd);
										if($numP>0){
											$stringSum='SELECT	
												SUM(a.qty) as cant	
											FROM
												'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
											WHERE
												a.fk_commande ='.$id.'
													AND a.fk_order_reception ='.$idrec.'
													and a.fk_product='.$objp->fk_product.';';
											$resSum=$db->query($stringSum);
											$sum = $db->fetch_object($resSum);

											$type = 'batch';
											print '<td align="right">'.img_picto($langs->trans('AddDispatchBatchLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';	
											while ($ob = $db->fetch_object($resProd)) {
												$suffix='_0_'.($i+$cant);	

												
											//	$numRow=(int)$objp->qty - (int)$products_dispatched[$objp->rowid];
												//$s='select  SUM(qty) as cant from llx_commande_fournisseur_dispatch where fk_commande='.$id;
												$s='select  SUM(qty) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
												$r=$db->query($s);
												$d=$db->fetch_object($r);

												$numRow=(int)$objp->qty - $d->cant;
												
												// Dispatch column
												print '<td></td>';																													// Warehouse column
												print '</tr>';

												print '<tr '.$bc[$var].' name="'.$type.$suffix.'">';
												print '<td>';
												print '<input name="fk_commandefourndet'.$suffix.'" type="hidden" value="'.$objp->rowid.'">';
												print '<input name="product_batch'.$suffix.'" class="producID products" type="hidden" value="'.$objp->fk_product.'">';
												print '<input name="pu'.$suffix.'" type="hidden" value="'.$up_ht_disc.'"><!-- This is a up including discount -->';
												// hidden fields for js function
												print '<input id="qty_ordered'.$suffix.'" type="hidden" value="'.$objp->qty.'">';
												print '<input id="qty_dispatched'.$suffix.'" type="hidden" value="'.($sum->cant-1).'">';
												print '</td>';

												print '<td>';
												print '<input type="text" class="serieBatch" required id="lot_number'.$suffix.'" name="lot_number'.$suffix.'" size="40" value="'.$ob->batch.'">';
												print '</td>';
												print '<td>';
												$dlcdatesuffix=dol_mktime(0, 0, 0, GETPOST('dlc'.$suffix.'month'), GETPOST('dlc'.$suffix.'day'), GETPOST('dlc'.$suffix.'year'));
												print '<input type="hidden" class="sufixEat"  value="'.$suffix.'">';
												$form->select_date(strtotime($ob->eatby),'dlc'.$suffix,'','',1,"");
												print '</td>';
												print '<td>';
												$dluodatesuffix=dol_mktime(0, 0, 0, GETPOST('dluo'.$suffix.'month'), GETPOST('dluo'.$suffix.'day'), GETPOST('dluo'.$suffix.'year'));
												print '<input type="hidden" class="sufixSell"  value="'.$suffix.'">';
												$form->select_date(strtotime($ob->sellby),'dluo'.$suffix,'','',1,"");
												print '</td>';
												print '<td colspan="2">&nbsp</td>';		// Qty ordered + qty already dispatached
												print '<td align="right">';
												//echo '"product'.$objp->ref.'"';
												print '<input class=" cantid " type="hidden"   value="'.$ob->qty.'">';												
												$cantPro = ($ob->qty>0) ? $ob->qty : 0 ;
												print '<input type="hidden" class="sufixCant"  value="'.$suffix.'">';
												print '<input id="qty'.$suffix.'" name="qty'.$suffix.'" class="  product'.$objp->ref.'" type="text" type="text" size="8"  value="'.$cantPro.'">';
												print '</td>';

												// Warehouse
												print '<td align="right">';
												if (count($listwarehouses)>1)
												{
													print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,1,0,$objp->fk_product);
												}
												elseif  (count($listwarehouses)==1)
												{
													print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,0,0,$objp->fk_product);
												}
												else
												{
													print $langs->trans("NoWarehouseDefined");
												}
												print "</td>\n";

												print "</tr>\n";	
												$cant++;
											}
											$bandAux=$cant;
										}else{


												$type = 'batch';
												//$numRow=(int)$objp->qty - (int)$products_dispatched[$objp->rowid];
												//$s='select  SUM(qty) as cant from llx_commande_fournisseur_dispatch where fk_commande='.$id;
												$s='select  SUM(qty) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
												$r=$db->query($s);
												$d=$db->fetch_object($r);

												$numRow=(int)$objp->qty - $d->cant;
												
												print '<td align="right">'.img_picto($langs->trans('AddDispatchBatchLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';	// Dispatch column
												print '<td></td>';																													// Warehouse column
												print '</tr>';

												print '<tr '.$bc[$var].' name="'.$type.$suffix.'">';
												print '<td>';
												print '<input name="fk_commandefourndet'.$suffix.'" type="hidden" value="'.$objp->rowid.'">';
												print '<input name="product_batch'.$suffix.'"class="producID products" type="hidden" value="'.$objp->fk_product.'">';
												print '<input name="pu'.$suffix.'" type="hidden" value="'.$up_ht_disc.'"><!-- This is a up including discount -->';
												// hidden fields for js function
												print '<input id="qty_ordered'.$suffix.'" type="hidden" value="'.$objp->qty.'">';
												print '<input id="qty_dispatched'.$suffix.'" type="hidden" value="'.(float) $products_dispatched[$objp->rowid].'">';
												print '</td>';

												print '<td>';
												print '<input type="text" class="serieBatch" required id="lot_number'.$suffix.'" name="lot_number'.$suffix.'" size="40" value="'.GETPOST('lot_number'.$suffix).'">';
												print '</td>';
												print '<td>';
												$dlcdatesuffix=dol_mktime(0, 0, 0, GETPOST('dlc'.$suffix.'month'), GETPOST('dlc'.$suffix.'day'), GETPOST('dlc'.$suffix.'year'));
												print '<input type="hidden" class="sufixEat"  value="'.$suffix.'">';
												$form->select_date(strtotime($dlcdatesuffix),'dlc'.$suffix,'','',1,"");

												print '</td>';
												print '<td>';
												$dluodatesuffix=dol_mktime(0, 0, 0, GETPOST('dluo'.$suffix.'month'), GETPOST('dluo'.$suffix.'day'), GETPOST('dluo'.$suffix.'year'));
												print '<input type="hidden" class="sufixSell"  value="'.$suffix.'">';
												$form->select_date(strtotime($dluodatesuffix),'dluo'.$suffix,'','',1,"");
												print '</td>';
												print '<td colspan="2">&nbsp</td>';		// Qty ordered + qty already dispatached
												print '<td align="right">';
												//echo '"product'.$objp->ref.'"';
												
												print '<input class=" cantid " type="hidden"   value="'.(GETPOST('qty'.$suffix)!='' ? GETPOST('qty'.$suffix) : $remaintodispatch).'">';	
												print '<input type="hidden" class="sufixCant"  value="'.$suffix.'">';
												print '<input id="qty'.$suffix.'" name="qty'.$suffix.'" class="  product'.$objp->ref.'" type="text" type="text" size="8" value="0">';
												print '</td>';

												// Warehouse
												print '<td align="right">';
												if (count($listwarehouses)>1)
												{
													print $formproduct->selectWarehouses($ware->alm, "entrepot".$suffix, "entrepot".$suffix,1,0,$objp->fk_product);
												}
												elseif  (count($listwarehouses)==1)
												{
													print $formproduct->selectWarehouses($ware->alm,"entrepot".$suffix, "entrepot".$suffix,0,0,$objp->fk_product);
												}
												else
												{
													print $langs->trans("NoWarehouseDefined");
												}
												print "</td>\n";

												print "</tr>\n";
										}	
														
										
									}								
								}
								else
								{

									$sqlProd='
											SELECT
												a.fk_product,
												a.qty,
												a.fk_entrepot,
												a.batch,
												a.eatby,
												a.sellby,
												p.ref,
												p.label,
												p.tobatch
											FROM
												'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
											INNER JOIN '.MAIN_DB_PREFIX.'product AS p ON a.fk_product = p.rowid
											WHERE
												a.fk_commande ='.$id.'
												AND a.fk_order_reception ='.$idrec.'
												and a.fk_product='.$objp->fk_product.';';


										$resProd=$db->query($sqlProd);
										$numP=$db->num_rows($resProd);
									if($numP>0){
										$i=$cant;
										$type = 'dispatch';
										print '<td align="right">'.img_picto($langs->trans('AddDispatchBatchLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';	

										$stringSum='SELECT	
												 SUM(a.qty) as cant	
											FROM
												'.MAIN_DB_PREFIX.'recepcion_avanzada_dispatch AS a
											WHERE
												a.fk_commande ='.$id.'
													AND a.fk_order_reception ='.$idrec.'
													and a.fk_product='.$objp->fk_product.';';
											$resSum=$db->query($stringSum);
											$sum = $db->fetch_object($resSum);

										while ($ob = $db->fetch_object($resProd)) {
											$suffix='_0_'.($i);	
											//echo " su ".$suffix;
											//$numRow=(int)$objp->qty - (int)$products_dispatched[$objp->rowid];

											//$s='select  SUM(qty) as cant from llx_commande_fournisseur_dispatch where fk_commande='.$id;
											$s='select  SUM(qty) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
											$r=$db->query($s);
											$d=$db->fetch_object($r);

											$numRow=(int)$objp->qty - $d->cant;

											//print '<td align="right">'.img_picto($langs->trans('AddStockLocationLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';	// Dispatch column
											//print '<td align="right">'.img_picto($langs->trans('AddDispatchBatchLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';
											print '<td></td>';
											print '</tr>';
											print '<tr '.$bc[$var].' name="'.$type.$suffix.'">';
											print '<td colspan="6">';
											print '<input name="fk_commandefourndet'.$suffix.'" type="hidden" value="'.$objp->rowid.'">';
											print '<input name="product'.$suffix.'" type="hidden" class=" products" value="'.$objp->fk_product.'">';
											print '<input name="pu'.$suffix.'" type="hidden" value="'.$up_ht_disc.'"><!-- This is a up including discount -->';
											// hidden fields for js function
											print '<input id="qty_ordered'.$suffix.'" type="hidden" value="'.$objp->qty.'">';
											print '<input id="qty_dispatched'.$suffix.'" type="hidden" value="'.($sum->cant-1).'">';
											print '</td>';

											print '<td align="right">';
											print '<input class=" cantid " type="hidden"   value="'.$ob->qty.'">';	
											print '<input type="hidden" class="sufixCant"  value="'.$suffix.'">';
											print '<input id="qty'.$suffix.'" name="qty'.$suffix.'" class=" product'.$objp->ref.'" type="text"  type="text" size="8" value="'.$ob->qty.'">';
											print '</td>';

											// Warehouse
											print '<td align="right">';
											if (count($listwarehouses)>1)
											{
												print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,1,0,$objp->fk_product);
											}
											elseif  (count($listwarehouses)==1)
											{
												print $formproduct->selectWarehouses((!is_null($ob->fk_entrepot))?$ob->fk_entrepot:$ware->alm, "entrepot".$suffix, "entrepot".$suffix,0,0,$objp->fk_product);
											}
											else
											{
												print $langs->trans("NoWarehouseDefined");
											}
											print "</td>\n";

											print "</tr>\n";
											$cant++;
										}
										$bandAux=$cant;
									}
									else{
										$i=$i+$bandAux+$cant;
										$type = 'dispatch';
										//$numRow=(int)$objp->qty - (int)$products_dispatched[$objp->rowid];
										$suffix='_0_'.($i);	

										//$s='select  SUM(qty) as cant from llx_commande_fournisseur_dispatch where fk_commande='.$id;
										$s='select  ROUND(SUM(qty),2) as cant from '.MAIN_DB_PREFIX.'commande_fournisseur_dispatch where fk_commande='.$id.'  and fk_product='.$objp->fk_product;
										$r=$db->query($s);
										$d=$db->fetch_object($r);

										$numRow=(int)$objp->qty - $d->cant;


										//print '<td align="right">'.img_picto($langs->trans('AddStockLocationLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';	// Dispatch column
										print '<td align="right">'.img_picto($langs->trans('AddDispatchBatchLine'),'split.png','onClick="addDispatchLine('.$i.',\''.$type.'\')"').'</td>';
										print '<td></td>';
										print '</tr>';
										print '<tr '.$bc[$var].' name="'.$type.$suffix.'">';
										print '<td colspan="6">';										
										print '<input name="fk_commandefourndet'.$suffix.'" type="hidden" value="'.$objp->rowid.'">';
										print '<input name="product'.$suffix.'" type="hidden"  class=" products"  value="'.$objp->fk_product.'">';
										print '<input name="pu'.$suffix.'" type="hidden" value="'.$up_ht_disc.'"><!-- This is a up including discount -->';
										// hidden fields for js function
										print '<input id="qty_ordered'.$suffix.'" type="hidden" value="'.$objp->qty.'">';
										print '<input id="qty_dispatched'.$suffix.'" type="hidden" value="'.(float) $products_dispatched[$objp->rowid].'">';
										print '</td>';

										print '<td align="right">';
										print '<input class=" cantid " type="hidden"  value="'.(GETPOST('qty'.$suffix)!='' ? GETPOST('qty'.$suffix) : $remaintodispatch).'">';	
										print '<input type="hidden" class="sufixCant"  value="'.$suffix.'">';
										print '<input id="qty'.$suffix.'" name="qty'.$suffix.'" class="  product'.$objp->ref.'" type="text"  type="text" size="8" value="'.$numRow.'">';
										print '</td>';

										// Warehouse
										print '<td align="right">';
										if (count($listwarehouses)>1)
										{
											print $formproduct->selectWarehouses($ware->alm,"entrepot".$suffix, "entrepot".$suffix,1,0,$objp->fk_product);
										}
										elseif  (count($listwarehouses)==1)
										{
											print $formproduct->selectWarehouses($ware->alm,"entrepot".$suffix, "entrepot".$suffix,0,0,$objp->fk_product);
										}
										else
										{
											print $langs->trans("NoWarehouseDefined");
										}
										print "</td>\n";

										print "</tr>\n";
									}


									
								}
							}							
							// Dispatch
						}
					}
					$i++;
				}
				$db->free($resql);
			}
			else
			{
				dol_print_error($db);
			}

			print "</table>\n";
			print "<br/>\n";

			if ($nbproduct)
			{

				if($resStatus->STATUS==0){
					print $langs->trans("Comment").' : ';
					print '<input type="text" size="60" maxlength="128" id="comment" name="comment" value="';
					print $_POST["comment"]?GETPOST("comment"):$langs->trans("DispatchSupplierOrder",$commande->ref);
					// print ' / '.$commande->ref_supplier;	// Not yet available
					print '" class="flat"> &nbsp; ';

					//print '<div class="center">';
					if ($user->rights->recepcionavanzada->valid_recep ){
						print '<input type="button" id="validateProd" class="button" value="'.$langs->trans("DispatchVerb").'"';
						if (count($listwarehouses) <= 0) print ' disabled';
						print '>';
					}					
					
					
					print '</form></br>';
					print "<form align='center' action='recepcion.php?id=".$idp."&act=card&type=1&idrec=".$idrec."&action=deleteOrd' method='POST'>";
					//print '<form method="post" action="#" align="center">';
					$sql="SELECT a.status
					FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion a
					WHERE a.fk_order_supplier=".$idp." AND a.rowid=".$idrec." AND a.entity=".$conf->entity."  and rowid=".$idrec;
					$res=$db->query($sql);
					$data= $db->fetch_object($res);

					if ($user->rights->recepcionavanzada->delete_recep && ($data->status==0))
					{
						print '<input type="submit" class="button" value="'.$langs->trans("Eliminar orden de recepción").'"/>';
					}

				}
				
				
				//print '</div>';
			}
			if (! $nbproduct && $nbfreeproduct)
			{
				print $langs->trans("NoPredefinedProductToDispatch");
			}

			print '</form>';
		}

		dol_fiche_end();


		// List of lines already dispatched
		$sql = "SELECT p.ref, p.label,";
		$sql.= " e.rowid as warehouse_id, e.label as entrepot,";
		$sql.= " cfd.rowid as dispatchlineid, cfd.fk_product, cfd.qty, cfd.eatby, cfd.sellby, cfd.batch, cfd.comment, cfd.status";
		$sql.= " FROM ".MAIN_DB_PREFIX."product as p,";
		$sql.= " ".MAIN_DB_PREFIX."commande_fournisseur_dispatch as cfd";
		$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."entrepot as e ON cfd.fk_entrepot = e.rowid";
		$sql.= " WHERE cfd.fk_commande = ".$commande->id." AND cfd.fk_order_reception=".$idrec;
		$sql.= " AND cfd.fk_product = p.rowid";
		$sql.= " ORDER BY cfd.rowid ASC";

		$resql = $db->query($sql);
		if ($resql)
		{
			$num = $db->num_rows($resql);
			$i = 0;

			if ($num > 0)
			{
				print "<br/>\n";

				print load_fiche_titre($langs->trans("ReceivingForSameOrder"));

				print '<table class="noborder" width="100%">';

				print '<tr class="liste_titre">';
				print '<td>'.$langs->trans("Description").'</td>';
				if (! empty($conf->productbatch->enabled))
				{					
					print '<td>'.$langs->trans("Número Lote/Serie").'</td>';
					print '<td>'.$langs->trans("Fecha caducidad").'</td>';
					print '<td>'.$langs->trans("Fecha limite de venta").'</td>';
				}
				print '<td align="right">'.$langs->trans("QtyDispatched").'</td>';
				print '<td></td>';
				print '<td>'.$langs->trans("Warehouse").'</td>';
				print '<td>'.$langs->trans("Comment").'</td>';
				if (! empty($conf->global->SUPPLIER_ORDER_USE_DISPATCH_STATUS)) print '<td align="center" colspan="2">'.$langs->trans("Status").'</td>';
				print "</tr>\n";

				$var=false;
				

				while ($i < $num)
				{
					$objp = $db->fetch_object($resql);

					print "<tr ".$bc[$var].">";
					print '<td>';
					print '<a href="'.DOL_URL_ROOT.'/product/fournisseurs.php?id='.$objp->fk_product.'">'.img_object($langs->trans("ShowProduct"),'product').' '.$objp->ref.'</a>';
					print ' - '.$objp->label;
					print "</td>\n";

					if (! empty($conf->productbatch->enabled))
					{
						print '<td>'.$objp->batch.'</td>';
						print '<td>'.dol_print_date($db->jdate($objp->eatby),'day').'</td>';
						print '<td>'.dol_print_date($db->jdate($objp->sellby),'day').'</td>';
					}

					// Qty
					print '<td align="right">'.$objp->qty.'</td>';
					print '<td>&nbsp;</td>';

					// Warehouse
					print '<td>';
					$warehouse_static->id=$objp->warehouse_id;
					$warehouse_static->libelle=$objp->entrepot;
					print $warehouse_static->getNomUrl(1);
					print '</td>';

					// Comment
					print '<td>'.dol_trunc($objp->comment).'</td>';

					// Status
					if (! empty($conf->global->SUPPLIER_ORDER_USE_DISPATCH_STATUS))
					{
						print '<td align="right">';
						$supplierorderdispatch->status = (empty($objp->status)?0:$objp->status);
						//print $supplierorderdispatch->status;
						print $supplierorderdispatch->getLibStatut(5);
						print '</td>';

						// Add button to check/uncheck disaptching
						print '<td align="center">';
						if ((empty($conf->global->MAIN_USE_ADVANCED_PERMS) && empty($user->rights->fournisseur->commande->receptionner))
       					|| (! empty($conf->global->MAIN_USE_ADVANCED_PERMS) && empty($user->rights->fournisseur->commande_advance->check))
							)
						{
							if (empty($objp->status))
							{
								print '<a class="button buttonRefused" href="#">'.$langs->trans("Approve").'</a>';
								print '<a class="button buttonRefused" href="#">'.$langs->trans("Deny").'</a>';
							}
							else
							{
								print '<a class="button buttonRefused" href="#">'.$langs->trans("Disapprove").'</a>';
								print '<a class="button buttonRefused" href="#">'.$langs->trans("Deny").'</a>';
							}
						}
						else
						{
							$disabled='';
							if ($commande->statut == 5) $disabled=1;
							if (empty($objp->status))
							{
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=checkdispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Approve").'</a>';
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=denydispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Deny").'</a>';
							}
							if ($objp->status == 1)
							{
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=uncheckdispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Reinit").'</a>';
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=denydispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Deny").'</a>';
							}
							if ($objp->status == 2)
							{
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=uncheckdispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Reinit").'</a>';
								print '<a class="button'.($disabled?' buttonRefused':'').'" href="'.$_SERVER["PHP_SELF"]."?id=".$id."&action=checkdispatchline&lineid=".$objp->dispatchlineid.'">'.$langs->trans("Approve").'</a>';
							}
						}
						print '</td>';
					}

					print "</tr>\n";

					$i++;
					$var=!$var;
				}
				$db->free($resql);

				print "</table>\n";
			}
		}
		else
		{
			dol_print_error($db);
		}
	}
	else
	{
		// Commande	non	trouvee
		dol_print_error($db);
	}
}

$sql="SELECT status 
FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion 
WHERE rowid=".$idrec."";
		//echo $sql;
	$rq=$db->query($sql);		
	$rs=$db->fetch_object($rq);



if($rs->status!=0){

	print "</table><br><br>";
	
	$filedir=DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/";
	print load_fiche_titre($langs->trans("Archivos vinculados"));
	$titletoshow='Archivos vinculados';
	$modulepart="commande_fournisseur";
	@$conf->$modulepart->dir_output="reception";
	$file_list=dol_dir_list($filedir,'files',0,'','\.meta$','date',SORT_DESC);
	if (! empty($file_list) && ! $headershown)
	{
		$headershown=1;
		$out.= '<div class="liste_titre">';
		$out.= "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."'>";
		$out.= $titletoshow."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		if($rs->status!=2){
			$out.=  '<a class="button" href="PDFOrdRec.php?id='.$id.'&idrec='.$idrec.'" align="right">Generar PDF</a>';
		}		
		$out.= "</form>";
		$out.='</div>';
		$out.= '<table class="border" summary="listofdocumentstable" width="100%">';
		
	}else{
		$out.= '<div class="liste_titre">';
		$out.= "<form method='POST' action='recepcion.php?id=".$idp."&act=card&type=1&file=".$idrec."'>";
		$out.= $titletoshow."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		if($rs->status!=2){
			$out.=  '<a class="button" href="PDFOrdRec.php?id='.$id.'&idrec='.$idrec.'" align="right">Generar PDF</a>';	
		}		
		$out.= "</form>";
		$out.='</div>';

	}

	if($_REQUEST['deldoc']){
		//print $_REQUEST['deldoc']."<---";
		$dir_eliminar = DOL_DATA_ROOT.'/fournisseur/commande/reception/'.$idrec."/".$_REQUEST['deldoc'];
		unlink($dir_eliminar);
		$string='UPDATE '.MAIN_DB_PREFIX.'recepcionavanzada_recepcion SET date_mod=now(), fk_user_mod='.$user->id.' where fk_order_supplier='.$idp.' and entity='.$conf->entity . ' and rowid='.$idrec;
		$res=$db->query($string);
		print "<script>window.location.href='recepcion.php?id=".$idp."&act=card&type=1&idrec=".$idrec."';</script>";
	}
	// Loop on each file found
	if (is_array($file_list))
	{
		
		foreach($file_list as $file)
		{
			$var=!$var;
			// Define relative path for download link (depends on module)
			$relativepath="reception/".$idrec."/".$file["name"];								// Cas general	
			$out.= "<tr ".$bc[$var].">";
			// Show file name with link to download
			$out.= '<td nowrap="nowrap">';
			$out.= '<a href="'.DOL_URL_ROOT .'/document.php?modulepart='.$modulepart.'&amp;file='.urlencode($relativepath).'"';				
			$mime=dol_mimetype($relativepath,'',0);
			if (preg_match('/text/',$mime)) $out.= ' target="_blank"';
			$out.= '>';
			$out.= img_mime($file["name"],$langs->trans("File").': '.$file["name"]).' '.dol_trunc($file["name"],$maxfilenamelength);
			$out.= '</a>'."\n";
			$out.= '</td>';
			// Show file size
			$size=(! empty($file['size'])?$file['size']:dol_filesize($filedir."/".$file["name"]));
			$out.= '<td align="right" nowrap="nowrap">'.dol_print_size($size).'</td>';
			// Show file date
			$date=(! empty($file['date'])?$file['date']:dol_filemtime($filedir."/".$file["name"]));
			$out.= '<td align="right" nowrap="nowrap">'.dol_print_date($date, 'dayhour').'</td>';				
			//$out.= '<td><a href="recepcion.php?id='.$idp.'&act=card&type=1&file='.$idrec.'&deldoc='.$file["name"].'">'.img_delete().'</a></td>';
			if($rs->status!=2){
				$out.= '<td>';
				$out.= '<a href="recepcion.php?id='.$idp.'&act=card&type=1&fiche=1&idrec='.$idrec.'&deldoc='.$file["name"].'">'.img_delete().'</a>';	
				$out.= '</td>';
			}
		}
		$out.= '</tr>';		
	}
	$out.= '</table><br><br>';
	print $out;
}	

if($rs->status!=0){
	
/*	$sql="SELECT a.fk_product,a.qty,b.label,a.rowid
	FROM ".MAIN_DB_PREFIX."recepcionavanzada_recepcion_product a, ".MAIN_DB_PREFIX."product b
	WHERE a.fk_recepcion=".$idrec." AND a.fk_product=b.rowid";*/
	
	$sql="SELECT a.fk_product,a.qty,b.label,a.rowid, a.batch 
	FROM ".MAIN_DB_PREFIX."commande_fournisseur_dispatch a, ".MAIN_DB_PREFIX."product b
	WHERE a.fk_commande=".$idp." and a.fk_order_reception=".$idrec."  AND a.fk_product=b.rowid";
	
	
	$rq=$db->query($sql);
	print load_fiche_titre($langs->trans("Imprimir etiquetas"));
	print "<br><form name='formulario' action='".DOL_URL_ROOT."/imprimiretiqueta/imprimir.php' target='_blank'><table class='noborder' width='100%'>";	
	print "<tr class='liste_titre'>";
	print "<td></td>";
	print "<td>Producto</td>";
	print "<td>Cantidad</td>";
	print "<td>Cant. Etiquetas</td>";
	print "</tr>";
	$lotes = "";
	while($rs=$db->fetch_object($rq)){

		print "<tr>";
		print "<td>";
		$lotes .= $rs->batch."|";
		print '<input type="checkbox" name="p-'.$rs->fk_product.'-'.$rs->rowid.'" value="1" onclick="document.formulario.cant'.$rs->fk_product.$rs->rowid.'.disabled=!document.formulario.cant'.$rs->fk_product.$rs->rowid.'.disabled">';
		print "</td>";
		print "<td>".$rs->label."</td>";
		print "<td>".$rs->qty."</td>";
		print '<td><input type="text" name="cant'.$rs->fk_product.$rs->rowid.'" value="'.$rs->qty.'" disabled></td>';
		print "</tr>";
	}
	if($conf->global->MAIN_MODULE_IMPRIMIRETIQUETA){
	print "<tr>";
	print "<input type='hidden' name='lotes' value='".$lotes."'>";
	print "<td colspan='4'>&nbsp;&nbsp;&nbsp;&nbsp; Modo a imprimir <select name='modoimprimir'>";
		print "<option value='1'>1</option>";
		print "<option value='2'>2</option>";
		print "</select>";
	print '&nbsp;&nbsp;&nbsp;&nbsp;<input class="button" type="submit" value="Imprimir Etiquetas">';
	print "</td></tr>";
	}
	print "<input type='hidden' name='idOrderSupplier' value='".$idp."'>";
	print "<input type='hidden' name='pedimentoImportacion' value='".$pedimento."'>";
	print "<input type='hidden' name='idSocieteReprint' value='".$commande->socid."'>";
	print "</table></form>";
	
}

llxFooter();

}





