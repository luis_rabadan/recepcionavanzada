<?php
/* Copyright (C) 2007-2012  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2014       Juanjo Menent       <jmenent@2byte.es>
 * Copyright (C) 2015       Florian Henry       <florian.henry@open-concept.pro>
 * Copyright (C) 2015       Raphaël Doursenaud  <rdoursenaud@gpcsolutions.fr>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    recepcionavanzada/recepcionavanzadarecepcion.class.php
 * \ingroup recepcionavanzada
 * \brief   This file is an example for a CRUD class file (Create/Read/Update/Delete)
 *          Put some comments here
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class Recepcionavanzadarecepcion
 *
 * Put here description of your class
 * @see CommonObject
 */
class Recepcionavanzadarecepcion extends CommonObject
{
	/**
	 * @var string Id to identify managed objects
	 */
	public $element = 'recepcionavanzadarecepcion';
	/**
	 * @var string Name of table without prefix where object is stored
	 */
	public $table_element = 'recepcionavanzada_recepcion';

	/**
	 * @var RecepcionavanzadarecepcionLine[] Lines
	 */
	public $lines = array();

	/**
	 */
	
	public $fk_order_supplier;
	public $ref;
	public $date_orderreception = '';
	public $time_orderreception;
	public $fk_user_reception;
	public $fk_user_warehouse;
	public $exchange_rate;
	public $status_import_declaration;
	public $import_declaration;
	public $date_import_declaration = '';
	public $aduana;
	public $note_public;
	public $note_private;
	public $fk_user_creation;
	public $date_creation = '';
	public $date_validation = '';
	public $fk_user_validation;
	public $fk_user_close;
	public $date_close = '';
	public $status;
	public $attach_files;
	public $entity;

	/**
	 */
	

	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		$this->db = $db;
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, Id of created object if OK
	 */
	public function create(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		// Clean parameters
		
		if (isset($this->fk_order_supplier)) {
			 $this->fk_order_supplier = trim($this->fk_order_supplier);
		}
		if (isset($this->ref)) {
			 $this->ref = trim($this->ref);
		}
		if (isset($this->time_orderreception)) {
			 $this->time_orderreception = trim($this->time_orderreception);
		}
		if (isset($this->fk_user_reception)) {
			 $this->fk_user_reception = trim($this->fk_user_reception);
		}
		if (isset($this->fk_user_warehouse)) {
			 $this->fk_user_warehouse = trim($this->fk_user_warehouse);
		}
		if (isset($this->exchange_rate)) {
			 $this->exchange_rate = trim($this->exchange_rate);
		}
		if (isset($this->status_import_declaration)) {
			 $this->status_import_declaration = trim($this->status_import_declaration);
		}
		if (isset($this->import_declaration)) {
			 $this->import_declaration = trim($this->import_declaration);
		}
		if (isset($this->aduana)) {
			 $this->aduana = trim($this->aduana);
		}
		if (isset($this->note_public)) {
			 $this->note_public = trim($this->note_public);
		}
		if (isset($this->note_private)) {
			 $this->note_private = trim($this->note_private);
		}
		if (isset($this->fk_user_creation)) {
			 $this->fk_user_creation = trim($this->fk_user_creation);
		}
		if (isset($this->fk_user_validation)) {
			 $this->fk_user_validation = trim($this->fk_user_validation);
		}
		if (isset($this->fk_user_close)) {
			 $this->fk_user_close = trim($this->fk_user_close);
		}
		if (isset($this->status)) {
			 $this->status = trim($this->status);
		}
		if (isset($this->attach_files)) {
			 $this->attach_files = trim($this->attach_files);
		}
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}

		

		// Check parameters
		// Put here code to add control on parameters values

		// Insert request
		$sql = 'INSERT INTO ' . MAIN_DB_PREFIX . $this->table_element . '(';
		
		$sql.= 'fk_order_supplier,';
		$sql.= 'ref,';
		$sql.= 'date_orderreception,';
		$sql.= 'time_orderreception,';
		$sql.= 'fk_user_reception,';
		$sql.= 'fk_user_warehouse,';
		$sql.= 'exchange_rate,';
		$sql.= 'status_import_declaration,';
		$sql.= 'import_declaration,';
		$sql.= 'date_import_declaration,';
		$sql.= 'aduana,';
		$sql.= 'note_public,';
		$sql.= 'note_private,';
		$sql.= 'fk_user_creation,';
		$sql.= 'date_creation,';
		$sql.= 'date_validation,';
		$sql.= 'fk_user_validation,';
		$sql.= 'fk_user_close,';
		$sql.= 'date_close,';
		$sql.= 'status,';
		$sql.= 'attach_files';
		$sql.= 'entity';

		
		$sql .= ') VALUES (';
		
		$sql .= ' '.(! isset($this->fk_order_supplier)?'NULL':$this->fk_order_supplier).',';
		$sql .= ' '.(! isset($this->ref)?'NULL':"'".$this->db->escape($this->ref)."'").',';
		$sql .= ' '.(! isset($this->date_orderreception) || dol_strlen($this->date_orderreception)==0?'NULL':"'".$this->db->idate($this->date_orderreception)."'").',';
		$sql .= ' '.(! isset($this->time_orderreception)?'NULL':"'".$this->time_orderreception."'").',';
		$sql .= ' '.(! isset($this->fk_user_reception)?'NULL':$this->fk_user_reception).',';
		$sql .= ' '.(! isset($this->fk_user_warehouse)?'NULL':$this->fk_user_warehouse).',';
		$sql .= ' '.(! isset($this->exchange_rate)?'NULL':"'".$this->exchange_rate."'").',';
		$sql .= ' '.(! isset($this->status_import_declaration)?'NULL':$this->status_import_declaration).',';
		$sql .= ' '.(! isset($this->import_declaration)?'NULL':"'".$this->db->escape($this->import_declaration)."'").',';
		$sql .= ' '.(! isset($this->date_import_declaration) || dol_strlen($this->date_import_declaration)==0?'NULL':"'".$this->db->idate($this->date_import_declaration)."'").',';
		$sql .= ' '.(! isset($this->aduana)?'NULL':"'".$this->db->escape($this->aduana)."'").',';
		$sql .= ' '.(! isset($this->note_public)?'NULL':"'".$this->db->escape($this->note_public)."'").',';
		$sql .= ' '.(! isset($this->note_private)?'NULL':"'".$this->db->escape($this->note_private)."'").',';
		$sql .= ' '.(! isset($this->fk_user_creation)?'NULL':$this->fk_user_creation).',';
		$sql .= ' '.(! isset($this->date_creation) || dol_strlen($this->date_creation)==0?'NULL':"'".$this->db->idate($this->date_creation)."'").',';
		$sql .= ' '.(! isset($this->date_validation) || dol_strlen($this->date_validation)==0?'NULL':"'".$this->db->idate($this->date_validation)."'").',';
		$sql .= ' '.(! isset($this->fk_user_validation)?'NULL':$this->fk_user_validation).',';
		$sql .= ' '.(! isset($this->fk_user_close)?'NULL':$this->fk_user_close).',';
		$sql .= ' '.(! isset($this->date_close) || dol_strlen($this->date_close)==0?'NULL':"'".$this->db->idate($this->date_close)."'").',';
		$sql .= ' '.(! isset($this->status)?'NULL':$this->status).',';
		$sql .= ' '.(! isset($this->attach_files)?'NULL':"'".$this->db->escape($this->attach_files)."'").',';
		$sql .= ' '.(! isset($this->entity)?'NULL':$this->entity);

		
		$sql .= ')';

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error) {
			$this->id = $this->db->last_insert_id(MAIN_DB_PREFIX . $this->table_element);

			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action to call a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_CREATE',$user);
				//if ($result < 0) $error++;
				//// End call triggers
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return $this->id;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param int    $id  Id object
	 * @param string $ref Ref
	 *
	 * @return int <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetch($id, $ref = null)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.fk_order_supplier,";
		$sql .= " t.ref,";
		$sql .= " t.date_orderreception,";
		$sql .= " t.time_orderreception,";
		$sql .= " t.fk_user_reception,";
		$sql .= " t.fk_user_warehouse,";
		$sql .= " t.exchange_rate,";
		$sql .= " t.status_import_declaration,";
		$sql .= " t.import_declaration,";
		$sql .= " t.date_import_declaration,";
		$sql .= " t.aduana,";
		$sql .= " t.note_public,";
		$sql .= " t.note_private,";
		$sql .= " t.fk_user_creation,";
		$sql .= " t.date_creation,";
		$sql .= " t.date_validation,";
		$sql .= " t.fk_user_validation,";
		$sql .= " t.fk_user_close,";
		$sql .= " t.date_close,";
		$sql .= " t.status,";
		$sql .= " t.attach_files,";
		$sql .= " t.entity";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element . ' as t';
		if (null !== $ref) {
			$sql .= ' WHERE t.ref = ' . '\'' . $ref . '\'';
		} else {
			$sql .= ' WHERE t.rowid = ' . $id;
		}

		$resql = $this->db->query($sql);
		if ($resql) {
			$numrows = $this->db->num_rows($resql);
			if ($numrows) {
				$obj = $this->db->fetch_object($resql);

				$this->id = $obj->rowid;
				
				$this->fk_order_supplier = $obj->fk_order_supplier;
				$this->ref = $obj->ref;
				$this->date_orderreception = $this->db->jdate($obj->date_orderreception);
				$this->time_orderreception = $obj->time_orderreception;
				$this->fk_user_reception = $obj->fk_user_reception;
				$this->fk_user_warehouse = $obj->fk_user_warehouse;
				$this->exchange_rate = $obj->exchange_rate;
				$this->status_import_declaration = $obj->status_import_declaration;
				$this->import_declaration = $obj->import_declaration;
				$this->date_import_declaration = $this->db->jdate($obj->date_import_declaration);
				$this->aduana = $obj->aduana;
				$this->note_public = $obj->note_public;
				$this->note_private = $obj->note_private;
				$this->fk_user_creation = $obj->fk_user_creation;
				$this->date_creation = $this->db->jdate($obj->date_creation);
				$this->date_validation = $this->db->jdate($obj->date_validation);
				$this->fk_user_validation = $obj->fk_user_validation;
				$this->fk_user_close = $obj->fk_user_close;
				$this->date_close = $this->db->jdate($obj->date_close);
				$this->status = $obj->status;
				$this->attach_files = $obj->attach_files;
				$this->entity = $obj->entity;

				
			}
			$this->db->free($resql);

			if ($numrows) {
				return 1;
			} else {
				return 0;
			}
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param string $sortorder Sort Order
	 * @param string $sortfield Sort field
	 * @param int    $limit     offset limit
	 * @param int    $offset    offset limit
	 * @param array  $filter    filter array
	 * @param string $filtermode filter mode (AND or OR)
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function fetchAll($sortorder='', $sortfield='', $limit=0, $offset=0, array $filter = array(), $filtermode='AND')
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$sql = 'SELECT';
		$sql .= ' t.rowid,';
		
		$sql .= " t.fk_order_supplier,";
		$sql .= " t.ref,";
		$sql .= " t.date_orderreception,";
		$sql .= " t.time_orderreception,";
		$sql .= " t.fk_user_reception,";
		$sql .= " t.fk_user_warehouse,";
		$sql .= " t.exchange_rate,";
		$sql .= " t.status_import_declaration,";
		$sql .= " t.import_declaration,";
		$sql .= " t.date_import_declaration,";
		$sql .= " t.aduana,";
		$sql .= " t.note_public,";
		$sql .= " t.note_private,";
		$sql .= " t.fk_user_creation,";
		$sql .= " t.date_creation,";
		$sql .= " t.date_validation,";
		$sql .= " t.fk_user_validation,";
		$sql .= " t.fk_user_close,";
		$sql .= " t.date_close,";
		$sql .= " t.status,";
		$sql .= " t.attach_files,";
		$sql .= " t.entity";

		
		$sql .= ' FROM ' . MAIN_DB_PREFIX . $this->table_element. ' as t';

		// Manage filter
		$sqlwhere = array();
		if (count($filter) > 0) {
			foreach ($filter as $key => $value) {
				$sqlwhere [] = $key . ' LIKE \'%' . $this->db->escape($value) . '%\'';
			}
		}
		if (count($sqlwhere) > 0) {
			$sql .= ' WHERE ' . implode(' '.$filtermode.' ', $sqlwhere);
		}
		
		if (!empty($sortfield)) {
			$sql .= $this->db->order($sortfield,$sortorder);
		}
		if (!empty($limit)) {
		 $sql .=  ' ' . $this->db->plimit($limit + 1, $offset);
		}
		$this->lines = array();

		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);

			while ($obj = $this->db->fetch_object($resql)) {
				$line = new RecepcionavanzadarecepcionLine();

				$line->id = $obj->rowid;
				
				$line->fk_order_supplier = $obj->fk_order_supplier;
				$line->ref = $obj->ref;
				$line->date_orderreception = $this->db->jdate($obj->date_orderreception);
				$line->time_orderreception = $obj->time_orderreception;
				$line->fk_user_reception = $obj->fk_user_reception;
				$line->fk_user_warehouse = $obj->fk_user_warehouse;
				$line->exchange_rate = $obj->exchange_rate;
				$line->status_import_declaration = $obj->status_import_declaration;
				$line->import_declaration = $obj->import_declaration;
				$line->date_import_declaration = $this->db->jdate($obj->date_import_declaration);
				$line->aduana = $obj->aduana;
				$line->note_public = $obj->note_public;
				$line->note_private = $obj->note_private;
				$line->fk_user_creation = $obj->fk_user_creation;
				$line->date_creation = $this->db->jdate($obj->date_creation);
				$line->date_validation = $this->db->jdate($obj->date_validation);
				$line->fk_user_validation = $obj->fk_user_validation;
				$line->fk_user_close = $obj->fk_user_close;
				$line->date_close = $this->db->jdate($obj->date_close);
				$line->status = $obj->status;
				$line->attach_files = $obj->attach_files;
				$line->entity = $obj->entity;

				

				$this->lines[$line->id] = $line;
			}
			$this->db->free($resql);

			return $num;
		} else {
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);

			return - 1;
		}
	}

	/**
	 * Update object into database
	 *
	 * @param  User $user      User that modifies
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function update(User $user, $notrigger = false)
	{
		$error = 0;

		dol_syslog(__METHOD__, LOG_DEBUG);

		// Clean parameters
		
		if (isset($this->fk_order_supplier)) {
			 $this->fk_order_supplier = trim($this->fk_order_supplier);
		}
		if (isset($this->ref)) {
			 $this->ref = trim($this->ref);
		}
		if (isset($this->time_orderreception)) {
			 $this->time_orderreception = trim($this->time_orderreception);
		}
		if (isset($this->fk_user_reception)) {
			 $this->fk_user_reception = trim($this->fk_user_reception);
		}
		if (isset($this->fk_user_warehouse)) {
			 $this->fk_user_warehouse = trim($this->fk_user_warehouse);
		}
		if (isset($this->exchange_rate)) {
			 $this->exchange_rate = trim($this->exchange_rate);
		}
		if (isset($this->status_import_declaration)) {
			 $this->status_import_declaration = trim($this->status_import_declaration);
		}
		if (isset($this->import_declaration)) {
			 $this->import_declaration = trim($this->import_declaration);
		}
		if (isset($this->aduana)) {
			 $this->aduana = trim($this->aduana);
		}
		if (isset($this->note_public)) {
			 $this->note_public = trim($this->note_public);
		}
		if (isset($this->note_private)) {
			 $this->note_private = trim($this->note_private);
		}
		if (isset($this->fk_user_creation)) {
			 $this->fk_user_creation = trim($this->fk_user_creation);
		}
		if (isset($this->fk_user_validation)) {
			 $this->fk_user_validation = trim($this->fk_user_validation);
		}
		if (isset($this->fk_user_close)) {
			 $this->fk_user_close = trim($this->fk_user_close);
		}
		if (isset($this->status)) {
			 $this->status = trim($this->status);
		}
		if (isset($this->attach_files)) {
			 $this->attach_files = trim($this->attach_files);
		}
		if (isset($this->entity)) {
			 $this->entity = trim($this->entity);
		}

		

		// Check parameters
		// Put here code to add a control on parameters values

		// Update request
		$sql = 'UPDATE ' . MAIN_DB_PREFIX . $this->table_element . ' SET';
		
		$sql .= ' fk_order_supplier = '.(isset($this->fk_order_supplier)?$this->fk_order_supplier:"null").',';
		$sql .= ' ref = '.(isset($this->ref)?"'".$this->db->escape($this->ref)."'":"null").',';
		$sql .= ' date_orderreception = '.(! isset($this->date_orderreception) || dol_strlen($this->date_orderreception) != 0 ? "'".$this->db->idate($this->date_orderreception)."'" : 'null').',';
		$sql .= ' time_orderreception = '.(isset($this->time_orderreception)?$this->time_orderreception:"null").',';
		$sql .= ' fk_user_reception = '.(isset($this->fk_user_reception)?$this->fk_user_reception:"null").',';
		$sql .= ' fk_user_warehouse = '.(isset($this->fk_user_warehouse)?$this->fk_user_warehouse:"null").',';
		$sql .= ' exchange_rate = '.(isset($this->exchange_rate)?$this->exchange_rate:"null").',';
		$sql .= ' status_import_declaration = '.(isset($this->status_import_declaration)?$this->status_import_declaration:"null").',';
		$sql .= ' import_declaration = '.(isset($this->import_declaration)?"'".$this->db->escape($this->import_declaration)."'":"null").',';
		$sql .= ' date_import_declaration = '.(! isset($this->date_import_declaration) || dol_strlen($this->date_import_declaration) != 0 ? "'".$this->db->idate($this->date_import_declaration)."'" : 'null').',';
		$sql .= ' aduana = '.(isset($this->aduana)?"'".$this->db->escape($this->aduana)."'":"null").',';
		$sql .= ' note_public = '.(isset($this->note_public)?"'".$this->db->escape($this->note_public)."'":"null").',';
		$sql .= ' note_private = '.(isset($this->note_private)?"'".$this->db->escape($this->note_private)."'":"null").',';
		$sql .= ' fk_user_creation = '.(isset($this->fk_user_creation)?$this->fk_user_creation:"null").',';
		$sql .= ' date_creation = '.(! isset($this->date_creation) || dol_strlen($this->date_creation) != 0 ? "'".$this->db->idate($this->date_creation)."'" : 'null').',';
		$sql .= ' date_validation = '.(! isset($this->date_validation) || dol_strlen($this->date_validation) != 0 ? "'".$this->db->idate($this->date_validation)."'" : 'null').',';
		$sql .= ' fk_user_validation = '.(isset($this->fk_user_validation)?$this->fk_user_validation:"null").',';
		$sql .= ' fk_user_close = '.(isset($this->fk_user_close)?$this->fk_user_close:"null").',';
		$sql .= ' date_close = '.(! isset($this->date_close) || dol_strlen($this->date_close) != 0 ? "'".$this->db->idate($this->date_close)."'" : 'null').',';
		$sql .= ' status = '.(isset($this->status)?$this->status:"null").',';
		$sql .= ' attach_files = '.(isset($this->attach_files)?"'".$this->db->escape($this->attach_files)."'":"null").',';
		$sql .= ' entity = '.(isset($this->entity)?$this->entity:"null");

        
		$sql .= ' WHERE rowid=' . $this->id;

		$this->db->begin();

		$resql = $this->db->query($sql);
		if (!$resql) {
			$error ++;
			$this->errors[] = 'Error ' . $this->db->lasterror();
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		if (!$error && !$notrigger) {
			// Uncomment this and change MYOBJECT to your own tag if you
			// want this action calls a trigger.

			//// Call triggers
			//$result=$this->call_trigger('MYOBJECT_MODIFY',$user);
			//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
			//// End call triggers
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Delete object in database
	 *
	 * @param User $user      User that deletes
	 * @param bool $notrigger false=launch triggers after, true=disable triggers
	 *
	 * @return int <0 if KO, >0 if OK
	 */
	public function delete(User $user, $notrigger = false)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		$error = 0;

		$this->db->begin();

		if (!$error) {
			if (!$notrigger) {
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action calls a trigger.

				//// Call triggers
				//$result=$this->call_trigger('MYOBJECT_DELETE',$user);
				//if ($result < 0) { $error++; //Do also what you must do to rollback action if trigger fail}
				//// End call triggers
			}
		}

		if (!$error) {
			$sql = 'DELETE FROM ' . MAIN_DB_PREFIX . $this->table_element;
			$sql .= ' WHERE rowid=' . $this->id;

			$resql = $this->db->query($sql);
			if (!$resql) {
				$error ++;
				$this->errors[] = 'Error ' . $this->db->lasterror();
				dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
			}
		}

		// Commit or rollback
		if ($error) {
			$this->db->rollback();

			return - 1 * $error;
		} else {
			$this->db->commit();

			return 1;
		}
	}

	/**
	 * Load an object from its id and create a new one in database
	 *
	 * @param int $fromid Id of object to clone
	 *
	 * @return int New id of clone
	 */
	public function createFromClone($fromid)
	{
		dol_syslog(__METHOD__, LOG_DEBUG);

		global $user;
		$error = 0;
		$object = new Recepcionavanzadarecepcion($this->db);

		$this->db->begin();

		// Load source object
		$object->fetch($fromid);
		// Reset object
		$object->id = 0;

		// Clear fields
		// ...

		// Create clone
		$result = $object->create($user);

		// Other options
		if ($result < 0) {
			$error ++;
			$this->errors = $object->errors;
			dol_syslog(__METHOD__ . ' ' . join(',', $this->errors), LOG_ERR);
		}

		// End
		if (!$error) {
			$this->db->commit();

			return $object->id;
		} else {
			$this->db->rollback();

			return - 1;
		}
	}

	/**
	 *  Return a link to the user card (with optionaly the picto)
	 * 	Use this->id,this->lastname, this->firstname
	 *
	 *	@param	int		$withpicto			Include picto in link (0=No picto, 1=Include picto into link, 2=Only picto)
	 *	@param	string	$option				On what the link point to
     *  @param	integer	$notooltip			1=Disable tooltip
     *  @param	int		$maxlen				Max length of visible user name
     *  @param  string  $morecss            Add more css on link
	 *	@return	string						String with URL
	 */
	function getNomUrl($withpicto=0, $option='', $notooltip=0, $maxlen=24, $morecss='')
	{
		global $langs, $conf, $db;
        global $dolibarr_main_authentication, $dolibarr_main_demo;
        global $menumanager;


        $result = '';
        $companylink = '';

        $label = '<u>' . $langs->trans("Orden de recepci&oacute;n") . '</u>';
        $label.= '<div width="100%">';
        $label.= '<b>' . $langs->trans('Ref') . ':</b> ' . $this->ref;

        $link = '<a href="'.DOL_URL_ROOT.'/recepcionavanzada/recepcion.php?id='.$this->fk_order_supplier.'"';
        $link.= ($notooltip?'':' title="'.dol_escape_htmltag($label, 1).'" class="classfortooltip'.($morecss?' '.$morecss:'').'"');
        $link.= '>';
		$linkend='</a>';

        if ($withpicto)
        {
            $result.=($link.img_object(($notooltip?'':$label), 'label', ($notooltip?'':'class="classfortooltip"')).$linkend);
            if ($withpicto != 2) $result.=' ';
		}
		$result.= $link . $this->ref . $linkend;
		return $result;
	}
	
	/**
	 *  Retourne le libelle du status d'un user (actif, inactif)
	 *
	 *  @param	int		$mode          0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return	string 			       Label of status
	 */
	function getLibStatut($mode=0)
	{
		return $this->LibStatut($this->status,$mode);
	}

	/**
	 *  Renvoi le libelle d'un status donne
	 *
	 *  @param	int		$status        	Id status
	 *  @param  int		$mode          	0=libelle long, 1=libelle court, 2=Picto + Libelle court, 3=Picto, 4=Picto + Libelle long, 5=Libelle court + Picto
	 *  @return string 			       	Label of status
	 */
	function LibStatut($status,$mode=0)
	{
		global $langs;

		if ($mode == 0)
		{
			$prefix='';
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 1)
		{
			if ($status == 1) return $langs->trans('Enabled');
			if ($status == 0) return $langs->trans('Disabled');
		}
		if ($mode == 2)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 3)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5');
		}
		if ($mode == 4)
		{
			if ($status == 1) return img_picto($langs->trans('Enabled'),'statut4').' '.$langs->trans('Enabled');
			if ($status == 0) return img_picto($langs->trans('Disabled'),'statut5').' '.$langs->trans('Disabled');
		}
		if ($mode == 5)
		{
			if ($status == 1) return $langs->trans('Enabled').' '.img_picto($langs->trans('Enabled'),'statut4');
			if ($status == 0) return $langs->trans('Disabled').' '.img_picto($langs->trans('Disabled'),'statut5');
		}
	}
	
	
	/**
	 * Initialise object with example values
	 * Id must be 0 if object instance is a specimen
	 *
	 * @return void
	 */
	public function initAsSpecimen()
	{
		$this->id = 0;
		
		$this->fk_order_supplier = '';
		$this->ref = '';
		$this->date_orderreception = '';
		$this->time_orderreception = '';
		$this->fk_user_reception = '';
		$this->fk_user_warehouse = '';
		$this->exchange_rate = '';
		$this->status_import_declaration = '';
		$this->import_declaration = '';
		$this->date_import_declaration = '';
		$this->aduana = '';
		$this->note_public = '';
		$this->note_private = '';
		$this->fk_user_creation = '';
		$this->date_creation = '';
		$this->date_validation = '';
		$this->fk_user_validation = '';
		$this->fk_user_close = '';
		$this->date_close = '';
		$this->status = '';
		$this->attach_files = '';
		$this->entity = '';

		
	}

}

/**
 * Class RecepcionavanzadarecepcionLine
 */
class RecepcionavanzadarecepcionLine
{
	/**
	 * @var int ID
	 */
	public $id;
	/**
	 * @var mixed Sample line property 1
	 */
	
	public $fk_order_supplier;
	public $ref;
	public $date_orderreception = '';
	public $time_orderreception;
	public $fk_user_reception;
	public $fk_user_warehouse;
	public $exchange_rate;
	public $status_import_declaration;
	public $import_declaration;
	public $date_import_declaration = '';
	public $aduana;
	public $note_public;
	public $note_private;
	public $fk_user_creation;
	public $date_creation = '';
	public $date_validation = '';
	public $fk_user_validation;
	public $fk_user_close;
	public $date_close = '';
	public $status;
	public $attach_files;
	public $entity;

	/**
	 * @var mixed Sample line property 2
	 */
	
}
