<?php 
    
    function getNextNumRef($mode='next')    {
        global $conf, $db, $langs;
        $langs->load("bills");
        // Clean parameters (if not defined or using deprecated value)


        $mod_ref=(! empty($conf->global->SOCIETE_CODEREC_ADDON)?$conf->global->SOCIETE_CODEREC_ADDON:'mod_coderecepcion_monkey');
        $mybool=false;
        $file = $mod_ref.".php";
        $classname = $mod_ref;

        // Include file with class
        $dirmodels = array_merge(array('/'), (array) $conf->modules_parts['models']);




        foreach ($dirmodels as $reldir) {

            $dir = dol_buildpath($reldir."recepcionavanzada/core/modules/");

            // Load file with numbering class (if found)
            if (is_file($dir.$file) && is_readable($dir.$file))
            {
                $mybool |= include_once $dir . $file;
            }
        }
        $obj = new $classname();
        $numref = "";
        $numref = $obj->getNextValue($this);

        return $numref;
    }

    function formSearchProd($label = 'Productos/Servicios'){
        print '
        <span class="prod_entry_mode_predef">
        <label for="prod_entry_mode_predef"> 
            '.$label.'
        </label> 
        <input type="hidden" name="idprod" id="idprod" value="1">
        <!-- Javascript code for autocomplete of field idprod -->
        <script type="text/javascript">
        $(document).ready(function() {
            var autoselect = 0;
            var options = [];
            var get_parameter = "";

            /* Remove product id before select another product use keyup instead of change to avoid loosing the product id. This is needed only for select of predefined product */
            /* TODO Check if we can remove this */
            $("input#search_idprod").keydown(function() {
                $("#idprod").val("");
            });

            /* I disable this. A call to trigger is already done later into the select action of the autocomplete code
                $("input#search_idprod").change(function() {
                console.log("Call the change trigger on input idprod because of a change on search_idprod was triggered");
                $("#idprod").trigger("change");
            });*/

            // Check options for secondary actions when keyup
            $("input#search_idprod").keyup(function() {
                    if ($(this).val().length == 0)
                    {
                        $("#search_idprod").val("");
                        $("#idprod").val("").trigger("change");
                        if (options.option_disabled) {
                            $("#" + options.option_disabled).removeAttr("disabled");
                        }
                        if (options.disabled) {
                            $.each(options.disabled, function(key, value) {
                                $("#" + value).removeAttr("disabled");
                            });
                        }
                        if (options.update) {
                            $.each(options.update, function(key, value) {
                                $("#" + key).val("").trigger("change");
                            });
                        }
                        if (options.show) {
                            $.each(options.show, function(key, value) {
                                $("#" + value).hide().trigger("hide");
                            });
                        }
                        if (options.update_textarea) {
                            $.each(options.update_textarea, function(key, value) {
                                if (typeof CKEDITOR == "object" && typeof CKEDITOR.instances != "undefined" && CKEDITOR.instances[key] != "undefined") {
                                    CKEDITOR.instances[key].setData("");
                                } else {
                                    $("#" + key).html("");
                                }
                            });
                        }
                    }
            });
            $("input#search_idprod").autocomplete({
                source: function( request, response ) {
                    if( $("#enCompra").is(":checked") ){
                        get_parameter = "'.DOL_URL_ROOT.'/product/ajax/products.php?htmlname=idprod&outjson=1&price_level=0&type=&mode=2&status=1&finished=2";
                    } else {
                        get_parameter = "'.DOL_URL_ROOT.'/product/ajax/products.php?htmlname=idprod&outjson=1&price_level=0&type=&mode=1&status=1&finished=2";
                    }
                    $.get(get_parameter, { idprod: request.term }, function(data){
                        if (data != null)
                        {
                            response($.map( data, function(item) {
                                if (autoselect == 1 && data.length == 1) {
                                    $("#search_idprod").val(item.value);
                                    $("#idprod").val(item.key).trigger("change");
                                }
                                var label = item.label.toString();
                                var update = {};
                                if (options.update) {
                                    $.each(options.update, function(key, value) {
                                        update[key] = item[value];
                                    });
                                }
                                var textarea = {};
                                if (options.update_textarea) {
                                    $.each(options.update_textarea, function(key, value) {
                                        textarea[key] = item[value];
                                    });
                                }
                                return { label: label, value: item.value, id: item.key, update: update, textarea: textarea, disabled: item.disabled }
                            }));
                        }
                        else console.error("Error: Ajax url /dolibarr-4.0.4/htdocs/product/ajax/products.php?htmlname=idprod&outjson=1&price_level=0&type=&mode=1&status=1&finished=2 has returned an empty page. Should be an empty json array.");
                    }, "json");
                },
                dataType: "json",
                minLength: 1,
                select: function( event, ui ) {		// Function ran once new value has been selected into javascript combo
                    console.log("Call change on input idprod because of select definition of autocomplete select call on input#search_idprod");
                    console.log("Selected id = "+ui.item.id+" - If this value is null, it means you select a record with key that is null so selection is not effective");
                    $("#idprod").val(ui.item.id).trigger("change");	// Select new value
                    // Disable an element
                    if (options.option_disabled) {
                        if (ui.item.disabled) {
                            $("#" + options.option_disabled).prop("disabled", true);
                            if (options.error) {
                                $.jnotify(options.error, "error", true);		// Output with jnotify the error message
                            }
                            if (options.warning) {
                                $.jnotify(options.warning, "warning", false);		// Output with jnotify the warning message
                            }
                    } else {
                            $("#" + options.option_disabled).removeAttr("disabled");
                        }
                    }
                    if (options.disabled) {
                        $.each(options.disabled, function(key, value) {
                            $("#" + value).prop("disabled", true);
                        });
                    }
                    if (options.show) {
                        $.each(options.show, function(key, value) {
                            $("#" + value).show().trigger("show");
                        });
                    }
                    // Update an input
                    if (ui.item.update) {
                        // loop on each "update" fields
                        $.each(ui.item.update, function(key, value) {
                            $("#" + key).val(value).trigger("change");
                        });
                    }
                    if (ui.item.textarea) {
                        $.each(ui.item.textarea, function(key, value) {
                            if (typeof CKEDITOR == "object" && typeof CKEDITOR.instances != "undefined" && CKEDITOR.instances[key] != "undefined") {
                                CKEDITOR.instances[key].setData(value);
                                CKEDITOR.instances[key].focus();
                            } else {
                                $("#" + key).html(value);
                                $("#" + key).focus();
                            }
                        });
                    }
                    console.log("ajax_autocompleter new value selected, we trigger change on original component so field #search_idprod");
                    $("#search_idprod").trigger("change");	// We have changed value of the combo select, we must be sure to trigger all js hook binded on this event. This is required to trigger other javascript change method binded on original field by other code.
                }
                ,delay: 500
            }).data("ui-autocomplete")._renderItem = function( ul, item ) {
                return $("<li>")
                .data( "ui-autocomplete-item", item ) // jQuery UI > 1.10.0
                .append( "<a><span class=\'tag\'>" + item.label + "</span></a>" )
                .appendTo(ul);
            };

            //Alternar entre los tipos de busqueda
            $("#enVenta").click(function() {
                $("#enCompra").attr("checked", false);
            });
            $("#enCompra").click(function() {
                $("#enVenta").attr("checked", false);
            });

        });
        </script>
        <input type="text" size="20" name="search_idprod" id="search_idprod" value="" class="ui-autocomplete-input" autocomplete="off">
        [
            <input type="radio" id="enVenta"/> En Venta
            <input type="radio" id="enCompra" checked/> En Compra
        ]
        </span>
        ';
    }

?>