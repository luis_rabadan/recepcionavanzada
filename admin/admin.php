<?php
require '../../main.inc.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
dol_include_once('/recepcionavanzada/lib/recepcion.lib.php');
global $langs;
$langs->load("admin");
$langs->load('other');
$form=new Form($db);

llxHeader('',"Configuracion","");

$linkback='<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';

print load_fiche_titre("Configuracion Recepcion de Stock avanzado",$linkback,'title_setup');


$head=array(
		array($_SERVER["PHP_SELF"], "Configuracion", '0' ),
		array('recepAdvSetuppage.php', "Referencias", '1' )
);

dol_fiche_head($head,'0', "Configuracion", 0, 'service');

$dirsociete=array_merge(array('/recepcionstock/class/'));
// Module to manage customer/supplier code

print load_fiche_titre("Módulo de recepción",'','');

$action= $_GET['action'];
$val=$_GET['value'];
$num=$_GET['n'];

if ( isset($_POST['action']) && $_POST['action'] == 'set'){
	$sql = "SELECT * FROM llx_c_recepcionavanzada_categoria_gasto"; 
	$resql=$db->query($sql);
	if (! $resql){
		dol_print_error($db);
	}else{
		$n=$db->num_rows($resql);
	}

	while ($cat_gas = $db->fetch_object($resql)) {
		if($cat_gas->active){
			$sql2="UPDATE  llx_c_recepcionavanzada_categoria_gasto set take_into_account=".GETPOST("CAT_GAS".$cat_gas->rowid)." WHERE rowid=".$cat_gas->rowid.";";	
			$resql2=$db->query($sql2);
			if (! $resql2){
				dol_print_error($db);	
			}
		}
	}
}

if ( isset($_POST['action']) && $_POST['action'] == 'add_item'){
	$db->begin();
	$sql = "
	INSERT INTO llx_recepcionavanzada_rubros (fk_prod, active)
	VALUES (".GETPOST("prod").", 1);
	";
	$res=$db->query($sql);
	if (! $res > 0) $error++;
	if (! $error)
	{
		$db->commit();
		setEventMessage($langs->trans("SetupSaved"));
	}
}

if($action=='updateRubro'){	
	if($num>0){
		$sql="UPDATE llx_recepcionavanzada_rubros set active=".$val." WHERE rowid = ".$num.";";	
		$resql=$db->query($sql);
	}
	if (! $resql){
		dol_print_error($db);
	}
	else
	{
		$db->rollback();
		setEventMessage($langs->trans("Error"),"errors");
	}
}

if($action=='deleteRubro'){	
	if($num>0){
		$sql="DELETE FROM llx_recepcionavanzada_rubros WHERE rowid = ".$num.";";	
		$resql=$db->query($sql);
	}
	if (! $resql){
		dol_print_error($db);
	}
	else
	{
		$db->rollback();
		setEventMessage($langs->trans("Error"),"errors");
	}
}

if($action=='updateBand'){	
	if($num>0){
		$sql="UPDATE  ".MAIN_DB_PREFIX."recepcionavanzada_conf set bandConfNumSerie=".$val.";";	
		$resql=$db->query($sql);
	}else{
		$sql="insert into  ".MAIN_DB_PREFIX."recepcionavanzada_conf(entity, bandConfNumSerie) values(".$conf->entity.",".$val.");";
		$resql=$db->query($sql);
	}
	
	if (! $resql){
		dol_print_error($db);
	}
	else
	{
		$db->rollback();
		setEventMessage($langs->trans("Error"),"errors");
	}
}

if($action=='updatebandConfgCostos'){
	if($num>0){
		$sql="UPDATE  ".MAIN_DB_PREFIX."recepcionavanzada_conf set bandConfgCostos=".$val.";";
		$resql=$db->query($sql);
	}else{
		$sql="insert into  ".MAIN_DB_PREFIX."recepcionavanzada_conf(entity, bandConfgCostos) values(".$conf->entity.",".$val.");";
		$resql=$db->query($sql);
	}
	
	if (! $resql){
		dol_print_error($db);
	}
}

if($action=='updateBandCompra'){	
	if($num>0){
		$sql="UPDATE  ".MAIN_DB_PREFIX."recepcionavanzada_conf set bandConfOrdCompra=".$val.";";	
		$resql=$db->query($sql);
	}else{
		$sql="insert into  ".MAIN_DB_PREFIX."recepcionavanzada_conf(entity, bandConfOrdCompra) values(".$conf->entity.",".$val.");";
		$resql=$db->query($sql);
	}
	if (! $resql){
		dol_print_error($db);	
	}
}

if($action=='updatebandConfgCalcCst_Prc'){	
	if($num>0){
		$sql="UPDATE  ".MAIN_DB_PREFIX."recepcionavanzada_conf set bandConfgCalcCst_Prc=".$val.";";	
		$resql=$db->query($sql);
	}else{
		$sql="insert into  ".MAIN_DB_PREFIX."recepcionavanzada_conf(entity, bandConfgCalcCst_Prc) values(".$conf->entity.",".$val.");";
		$resql=$db->query($sql);
	}
	if (! $resql){
		dol_print_error($db);	
	}
}

$sql = "select bandConfNumSerie as band,  bandConfOrdCompra, bandConfgCostos, bandConfgCalcCst_Prc from  ".MAIN_DB_PREFIX."recepcionavanzada_conf "; 
$resql=$db->query($sql);
if (! $resql){
	dol_print_error($db);	
}else{
	$n=$db->num_rows($resql);
	$band = $db->fetch_object($resql);
} 

print '<table class="noborder" width="100%">'."\n";
	print '<tr class="liste_titre">'."\n";	
		print '  <td>'.$langs->trans("Description").'</td>';	
		print '  <td align="center" width="80">'.$langs->trans("Status").'</td>';	
	print "</tr>";	
	print '<tr>';
		print '<td>';
			print '¿Se pueden ingresar cantidades superiores a la orden de compra?';
		print '</td>';
		print '<td align="center">'."\n";				
			if($band->bandConfOrdCompra==0 || is_null($band->bandConfOrdCompra) ){
				print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateBandCompra&n='.$n.'&value=1">';
                	print img_picto($langs->trans("Disabled"),'switch_off');
                print '</a>';	
			}else{					
				print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateBandCompra&n='.$n.'&value=0">';
                	print img_picto($langs->trans("Activated"),'switch_on');
                print '</a>';					
			}
        print "</td>\n";
	print '</tr>';
print '</table>';


if(isset($conf->global->MAIN_MODULE_PRODUCTBATCH)){
	print '<table class="noborder" width="100%">'."\n";
		print '<tr class="liste_titre">'."\n";	
			print '  <td>'.$langs->trans("Description").'</td>';	
			print '  <td align="center" width="80">'.$langs->trans("Status").'</td>';	
		print "</tr>";	
		print '<tr>';
			print '<td>';
				print '¿El número de serie deben ser único por cada producto?';
			print '</td>';
			print '<td align="center">'."\n";				
				if($band->band==0 || is_null($band->band) ){
					print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateBand&n='.$n.'&value=1">';
                    	print img_picto($langs->trans("Disabled"),'switch_off');
                    print '</a>';	
				}else{					
					print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateBand&n='.$n.'&value=0">';
                    	print img_picto($langs->trans("Activated"),'switch_on');
                    print '</a>';					
				}
	        print "</td>\n";
		print '</tr>';
	print '</table>';	
}else{
	echo "El módulo de Lotes y Series se encuentrá desactivado";
}


print '<table class="noborder" width="100%">'."\n";
print '<tr class="liste_titre">'."\n";
print '  <td>'.$langs->trans("Description").'</td>';
print '  <td align="center" width="80">'.$langs->trans("Status").'</td>';
print "</tr>";
print '<tr>';
print '<td>';
print '¿Contara con costos de entrega?';
print '</td>';
print '<td align="center">'."\n";
if($band->bandConfgCostos==0 || is_null($band->bandConfgCostos) ){
	print '<a href="'.$_SERVER['PHP_SELF'].'?action=updatebandConfgCostos&n='.$n.'&value=1">';
	print img_picto($langs->trans("Disabled"),'switch_off');
	print '</a>';
}else{
	print '<a href="'.$_SERVER['PHP_SELF'].'?action=updatebandConfgCostos&n='.$n.'&value=0">';
	print img_picto($langs->trans("Activated"),'switch_on');
	print '</a>';
}
print "</td>\n";
print '</tr>';
print '</table>';

// Switch para activar el nuevo calculo del coto/precio de los productos
print '<table class="noborder" width="100%">'."\n";
	print '<tr class="liste_titre">'."\n";
		print '<td>'.$langs->trans("Description").'</td>';
		print '<td align="center" width="80">'.$langs->trans("Status").'</td>';
	print "</tr>";
	print '<tr>';
		print '<td>';
			print 'Realizar cálculo de costo/precio de entrada con nuevo método.';
		print '</td>';
		print '<td align="center">'."\n";
			if($band->bandConfgCalcCst_Prc==0 || is_null($band->bandConfgCalcCst_Prc) ){
			print '<a href="'.$_SERVER['PHP_SELF'].'?action=updatebandConfgCalcCst_Prc&n='.$n.'&value=1">';
			print img_picto($langs->trans("Disabled"),'switch_off');
			print '</a>';
			}else{
			print '<a href="'.$_SERVER['PHP_SELF'].'?action=updatebandConfgCalcCst_Prc&n='.$n.'&value=0">';
			print img_picto($langs->trans("Activated"),'switch_on');
			print '</a>';
			}
		print "</td>\n";
	print '</tr>';
print '</table>';

// Tabla para indicar que categorias de gasto entran y cuales en el nuevo calculo costo/precio
print_titre("¿Qué categorias de pago no se toman en cuenta?");

$sql = "SELECT * FROM llx_c_recepcionavanzada_categoria_gasto"; 
$resql=$db->query($sql);
if (! $resql){
	dol_print_error($db);	
}else{
	$n=$db->num_rows($resql);
}

print '<form action="'.$_SERVER["PHP_SELF"].'" method="post">';
print '<input type="hidden" name="action" value="set">';

print '<table class="noborder" width="100%">'."\n";
print '<tr class="liste_titre">'."\n";
	print '<td>'.$langs->trans("Description").'</td>';
	print '<td>'.$langs->trans("Status").'</td>';
print "</tr>";
if( $band->bandConfgCalcCst_Prc!=0 && !is_null($band->bandConfgCalcCst_Prc) ){
	while ($cat_gas = $db->fetch_object($resql)) {
		if($cat_gas->active){
			print '<tr>';
				print '<td>';
					print $cat_gas->label;
				print '</td>';
				print '<td>'."\n";
					print $form->selectyesno("CAT_GAS".$cat_gas->rowid, $cat_gas->take_into_account, 1);
				print "</td>\n";
			print '</tr>';
		}
	}
} else {
	print '<tr>';
		print '<td>';
			print "Esta opción sólo esta disponible para el nuevo cálculo costo/precio.";
		print '</td>';
	print '</tr>';
}
print '</table>';

print '<center><input type="submit" class="button" value="'.$langs->trans("Save").'"></center>'.'</br>';

print "</form>";

// Configuracion de rubros que no se toman en cuenta para el calculo costo/precio
function select_dol_products($selected='', $htmlname='prod', $show_empty=0, $exclude='', $disabled=0, $include='', $enableonly='', $force_entity=0, $maxlength=0, $showstatus=0, $morefilter='', $show_every=0, $enableonlytext='', $morecss='', $noactive=0)
{
    global $conf,$user,$langs,$db;
    // If no preselected user defined, we take current user
    if ((is_numeric($selected) && ($selected < -2 || empty($selected))) && empty($conf->global->SOCIETE_DISABLE_DEFAULT_SALESREPRESENTATIVE)) $selected=$user->id;
    $excludeUsers=null;
    $includeUsers=null;
    // Permettre l'exclusion d'utilisateurs
    if (is_array($exclude)) $excludeUsers = implode("','",$exclude);
    // Permettre l'inclusion d'utilisateurs
    if (is_array($include)) $includeUsers = implode("','",$include);
    else if ($include == 'hierarchy')
    {
        // Build list includeUsers to have only hierarchy
        $userid=$user->id;
        $include=array();
        if (empty($user->users) || ! is_array($user->users)) $user->get_full_tree();
        foreach($user->users as $key => $val)
        {
            if (preg_match('/_'.$userid.'/',$val['fullpath'])) $include[]=$val['id'];
        }
        $includeUsers = implode("','",$include);
        //var_dump($includeUsers);exit;
        //var_dump($user->users);exit;
    }
    $out='';
    // On recherche les utilisateurs
    $sql = "SELECT DISTINCT u.rowid, u.ref, u.label,u.price_ttc,u.tva_tx,u.duration,u.fk_product_type";
    /*if (! empty($conf->multicompany->enabled) && $conf->entity == 1 && $user->admin && ! $user->entity)
    {
        $sql.= ", e.label";
    }*/
    $sql.= " FROM ".MAIN_DB_PREFIX ."product as u";
    $sql.= " ORDER BY u.ref ASC";
    $resql=$db->query($sql);
    if ($resql)
    {
        $num = $db->num_rows($resql);
        $i = 0;
        if ($num)
        {
            // Enhance with select2
            $nodatarole='';
            if ($conf->use_javascript_ajax)
            {
                include_once DOL_DOCUMENT_ROOT . '/core/lib/ajax.lib.php';
                $comboenhancement = ajax_combobox($htmlname);
                $out.=$comboenhancement;
                $nodatarole=($comboenhancement?' data-role="none"':'');
            }
            $out.= '<select class="flat minwidth200'.($morecss?' '.$morecss:'').'" id="'.$htmlname.'" name="'.$htmlname.'"'.($disabled?' disabled':'').$nodatarole.'>';
            if ($show_empty) $out.= '<option value="-1"'.((empty($selected) || $selected==-1)?' selected':'').'>&nbsp;</option>'."\n";
            if ($show_every) $out.= '<option value="-2"'.(($selected==-2)?' selected':'').'>-- '.$langs->trans("Everybody").' --</option>'."\n";
            $userstatic=new User($db);
            while ($i < $num)
            {
                $obj = $db->fetch_object($resql);
                $disableline='';
                if (is_array($enableonly) && count($enableonly) && ! in_array($obj->rowid,$enableonly)) $disableline=($enableonlytext?$enableonlytext:'1');
                if ((is_object($selected) && $selected->id == $obj->rowid) || (! is_object($selected) && $selected == $obj->rowid))
                {
                    $out.= '<option value="'.$obj->rowid.'"';
                    if ($disableline) $out.= ' disabled';
                    $out.= ' selected>';
                }
                else
                {
                    $out.= '<option value="'.$obj->rowid.'"';
                    if ($disableline) $out.= ' disabled';
                    $out.= '>';
                }
                $out.= $obj->ref." - ".$obj->label." - $".price($obj->price_ttc,0,'',0,0,0);
                if (empty($obj->duration)) {
                    if ($obj->tva_tx>0) {
                        $out.=" ".$langs->trans("TTC");
                    }else{
                        $out.=" ".$langs->trans("HT");
                    }
                }else{
                    $our_value=substr($obj->duration,0,dol_strlen($obj->duration)-1);
                    $outdurationunit=substr($obj->duration,-1);
                    
                    $da=array("h"=>$langs->trans("Hour"),"d"=>$langs->trans("Day"),"w"=>$langs->trans("Week"),"m"=>$langs->trans("Month"),"y"=>$langs->trans("Year"));
                    if (isset($da[$outdurationunit]))
                    {
                        $out.= " - ".$our_value." ".$langs->trans($da[$outdurationunit]);
                    }
                }
                
                $out.= '</option>';
                $i++;
            }
        }
        else
        {
            $out.= '<select class="flat" id="'.$htmlname.'" name="'.$htmlname.'" disabled>';
            $out.= '<option value="">'.$langs->trans("None").'</option>';
        }
        $out.= '</select>';
    }
    else
    {
        dol_print_error($db);
    }
    return $out;
}

print '<form action="'.$_SERVER["PHP_SELF"].'" method="post">';
print '<input type="hidden" name="action" value="add_item">';

print '<table class="noborder" width="100%">'."\n";

print '<tr class="liste_titre">'."\n";
	print '<td colspan="2"> Configuración de rubros que no se toman en cuenta para el cálculo costo/precio </td>';
print "</tr>";

print '<tr>';
	print '<td>';
		print "Rubro a excluir: ".select_dol_products();
		print "<script type='text/javascript'> $(document).ready(function () { $('#s2id_prod').width('80%'); }); </script>";
	print '</td>';
	print '<td>'."\n";
		print '<input type="submit" class="button" style="border-radius:0px !important;" name="add" value="Asignar">';
	print "</td>";
print '</tr>';

print '</table>';

print "</form>";

//Listado de los rubros que actualmente se excluyen del calculo costo/precio
$sql = "
SELECT a.*, b.ref, b.label
FROM llx_recepcionavanzada_rubros a, llx_product b
WHERE a.fk_prod = b.rowid
";
$resql=$db->query($sql);
if (! $resql){
	dol_print_error($db);
}else{
	$n=$db->num_rows($resql);
}

print '<table class="noborder" width="100%">'."\n";
print '<tr class="liste_titre">'."\n";
	print '<td>'.$langs->trans("Description").'</td>';
	print '<td align="center">'.$langs->trans("Status").'</td>';
print "</tr>";

while ($rubro = $db->fetch_object($resql)) {
	
		print '<tr>';
			print '<td align="">';
				print $rubro->ref.' '.$rubro->label;
			print '</td>';
			print '<td align="center">';
				if($rubro->active){
					print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateRubro&n='.$rubro->rowid.'&value=0">';
						print img_picto($langs->trans("Activated"),'switch_on');
					print '</a>';
				} else {
					print '<a href="'.$_SERVER['PHP_SELF'].'?action=updateRubro&n='.$rubro->rowid.'&value=1">';
						print img_picto($langs->trans("Disabled"),'switch_off');
					print '</a>';
				}
				print '<a href="'.$_SERVER["PHP_SELF"].'?action=deleteRubro&n='.$rubro->rowid.'">'.img_delete().'</a>';
			print '</td>';
		print '</tr>';
}

print '</table>';

// require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.facture.class.php';

// $object=new FactureFournisseur($db);
// // Add free products/services
// $object->formAddObjectLine(1, $societe, $mysoc);
//$parameters = array();
//$reshook = $hookmanager->executeHooks('formAddObjectLine', $parameters, $object, $action); // Note that $action and $object may have been modified by hook

formSearchProd("Rubros a excluir: ");