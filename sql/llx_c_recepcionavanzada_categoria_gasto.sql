CREATE TABLE IF NOT EXISTS `llx_c_recepcionavanzada_categoria_gasto` (
 `rowid` int(11) NOT NULL AUTO_INCREMENT,
 `label` varchar(100) DEFAULT NULL,
 `active` int(11) NOT NULL,
 PRIMARY KEY (`rowid`)
);

ALTER TABLE `llx_c_recepcionavanzada_categoria_gasto` add `take_into_account` int(11) DEFAULT 1;
